<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix'=>'','namespace'=>'Front'],function(){
//    Barryvdh\Debugbar\Facade::disable();
    Route::get('/login', function () {
        return Redirect()->route('admin.login');
    });
    Route::get('/tim-kiem','HomeController@search');
    Route::get('/hoan-tat-don-hang','HomeController@payment');
    Route::get('/danh-sach-don-hang','HomeController@listOrder');
    Route::get('/chi-tiet-don-hang/{id}','HomeController@detailOrder')->where("id", '[0-9A-Za-z\-]+');

    Route::post('/don-hang/hoan-tat','HomeController@submitCart');
    Route::get('/don-hang/xac-nhan/{id}','HomeController@confirm')->name('payment_confirm');

    Route::get('/don-hang/thanh-cong/{id}','HomeController@paymentDone')->name('payment_done');

    // basic
    Route::group(['prefix'=>'chuyen-muc'],function(){ 
        Route::get('/{slug}','PageController@pageItem')->name('page.item');
        Route::get('ads/{slug}','PageController@pageBannerItem')->name('page.banner.item');
    });

    Route::group(['prefix'=>'san-pham'],function(){ 
        Route::get('/all','ProductController@allProducts')->name('product_all');
        Route::get('/{slug}/{id}','ProductController@detail');
        Route::get('/san-pham-hot','ProductController@hotProducts')->name('products.hot');
        Route::get('/san-pham-da-xem','ProductController@viewedProducts')->name('products.viewed');

    });
     Route::group(['prefix'=>'gio-hang'],function(){ 
        Route::get('/','CartController@index');
        Route::get('/add/{id}','CartController@add');
        Route::get('/remove/{id}','CartController@remove');
         Route::get('/get-cart','CartController@getCart')->name("cart.get");
         Route::post('/update-cart','CartController@updateCart')->name("cart.update");

    });
     Route::group(['prefix'=>'danh-muc-san-pham'],function(){ 
        Route::get('/','ProductController@listCategories')->name('front.category.all');
        Route::get('/{slug}/{id}','ProductController@getProductsOfCategories')->name('front.category.level1');
         Route::get('/{slug1}/{slug2}/{id}','ProductController@detail')->name('product.detail');
//         Route::get('/{slug1}/{slug2}/{id}','ProductController@getProductsOfCategories')->name('front.category.level2');
//         Route::get('/{slug1}/{slug2}/{slug3}/{id}','ProductController@getProductsOfCategoriesLevel3')->name('front.category.level3');

    });
    Route::get('/login','UsersController@logIn')->name('front.get.login');
    Route::get('/sign-in','UsersController@signIn')->name('front.get.signin');
    Route::get('/logout','UsersController@logOut')->name('front.get.logout');

    Route::post('/login','UsersController@postLogIn')->name('front.login');
    Route::post('/sign-in','UsersController@postSignIn')->name('front.signin');
    Route::group(['prefix'=>'api'], function () {
        Route::post('/products/list','ProductController@filter')->name('api.products.filter');
    });
    Route::get('/random','HomeController@randomToVietlot')->name('front.get.vietlot');

});

