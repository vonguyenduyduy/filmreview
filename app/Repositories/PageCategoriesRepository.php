<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\PageCategories;

/**
 * Interface ProductCategoriesRepository
 * @package namespace App\Repositories;
 */
class PageCategoriesRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageCategories::class;
    }
    public function selectNoField($fields){
        $arr =  [
            'id', 'name', 'slug', 'images', 'active', 'orderBy', 'created_at', 'updated_at', 'description', 'parent', 'is_menu'
        ];
        if(!is_null($fields)){
            return array_diff($arr,$fields);
        }
        return $arr;
    }
    public function rules(){
        return array(
            'name'    => 'required',
        );
    }
    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên sản phẩm',
        ];
    }



}
