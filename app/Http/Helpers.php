<?php
const ABC = 123;
function getBreadcrumb($breadcrumb)
{
    $html = '';

    foreach ($breadcrumb as $v) {
        $html .= '<a class="card btn" ><i class="mdi-action-input"></i></a>';
        $html .= '<a href="'.$v['url'].'" class="z-depth-5 waves-effect waves-light btn">'.$v['name'].'</a>';
    }
    return $html;
}
function getValueConfig($key){
    $config = \Modules\AdminLte\Entities\Options::where('optionID', $key)->get()->first();
    if(empty($config)){
        return '';
    }
    return $config->value;
}
function renderImage($link){
    if(empty($link)){
        return asset('ui/no-image.png');
    }
    return $link;
}


function stringToInterger($str)
{
    if ($str != null && $str != '') {
        return str_replace(".", "", $str);
    }
    return 0;
}

function countPriceSale($price, $sale)
{
    return ceil($price - ($price * $sale / 100));
}

class FormHelpers
{
    public static function input($title, $name, $bind, $col = 2, $type='text')
    {
        return '<div class="form-group">
                    <label for="' . $name . '" class="col-sm-'.$col.' control-label">' . $title . '</label>
                    <div class="col-sm-'.(12 - $col).'">
                         <input type="'.$type.'" name="' . $name . '" class="form-control" placeholder="' . $title . '" v-model="'.$bind.'">
                    </div>
                </div>';
    }
    public static function inputDefault($title, $name, $bind, $col = 2, $type='text')
    {
        return '<div class="form-group">
                    <label for="' . $name . '" class="col-sm-'.$col.' control-label">' . $title . '</label>
                    <div class="col-sm-'.(12 - $col).'">
                         <input type="'.$type.'" name="' . $name . '" class="form-control" placeholder="' . $title . '" value="'.$bind.'">
                    </div>
                </div>';
    }
    public static function inputCurrency($title, $name, $bind, $col = 2)
    {
        return '<div class="form-group">
                    <label for="' . $name . '" class="col-sm-'.$col.' control-label">' . $title . '</label>
                    <div class="col-sm-'.(12 - $col).'">
                         <input name="' . $name . '" type="text" class="form-control currency" placeholder="' . $title . '" v-model="'.$bind.'">
                    </div>
                </div>';
    }

    public static function inputHidden($name, $bind, $value = '')
    {
        return '<input name="' . $name . '" type="hidden" v-model="'.$bind.'">';
    }

    public static function inputHiddenDefault($name, $value = '')
    {
        return '<input name="' . $name . '" type="hidden" value="'.$value.'">';
    }

    public static function dropDownList($title, $name, $options)
    {
        $select  = '<select name="'.$name.'" class="form-control select2" data-placeholder="'.$title.'" style="width: 100%;">';
        if(count($options)){
            foreach ($options as $v){
                $select .=  '<option value="">'.$v->name.'</option>' ;

            }
        }
        $select .= '</select>';
        return $select;
    }

    public static function toggleChekbox($title, $name, $checked)
    {
        return '<div class="form-group">
                   <label for="' . $name . '" class="col-sm-2 control-label">' . $title . '</label>
                   <div class="col-sm-2">
                       <input name="' . $name . '" class="toggle-one toggle-check" '.$checked.' type="checkbox">
                   </div>
                </div>';
    }

    public static function textArea($title, $name, $value = '')
    {
        return '<div class="form-group">
                   <label for="' . $name . '" class="col-sm-2 control-label">' . $title . '</label>
                   <div class="col-sm-10">
                        <textarea class="description ckeditor-main" name="' . $name . '" v-model="'.$value.'"></textarea>
                   </div>
                </div>';
    }
    public static function textAreaDefault($title, $name, $value = '')
    {
        return '<div class="form-group">
                   <label for="' . $name . '" class="col-sm-2 control-label">' . $title . '</label>
                   <div class="col-sm-10">
                        <textarea class="description ckeditor-main" name="' . $name . '">'.$value.'</textarea>
                   </div>
                </div>';
    }
    public static function image($title, $name, $image = '')
    {
        return '<div class="form-group">
                   <label for="' . $name . '" class="col-sm-2 control-label">' . $title . '</label>
                   <div class="col-sm-9">
                   <div class="input-group">
                        <span class="input-group-btn">
                             <a id="lfm" data-input="main-image" data-preview="holder" class="btn btn-primary image1">
                                 <i class="fa fa-picture-o"></i> Choose
                             </a>
                        </span>
                        <input id="main-image" class="form-control" type="text" name="'.$name.'" v-model="'.$image.'">
                   </div>
                   <div class="col-sm-8 col-md-offset-2">
                        <img id="holder" width="100%" v-bind:src="'.$image.'">
                   </div>
                </div>';
    }
    public static function imageDefault($title, $name, $image = '')
    {
        return '<div class="form-group">
                   <label for="' . $name . '" class="col-sm-2 control-label">' . $title . '</label>
                   <div class="col-sm-9">
                       <div class="input-group">
                            <span class="input-group-btn">
                                 <a id="lfm" data-input="main-image" data-preview="holder" class="btn btn-primary image1">
                                     <i class="fa fa-picture-o"></i> Choose
                                 </a>
                            </span>
                            <input id="main-image" class="form-control" type="text" name="'.$name.'" value="'.$image.'">
                       </div>
                   </div>    
                   <div class="col-sm-9 col-md-offset-2">
                        <img id="holder" width="100%" src="'.$image.'">
                   </div>
                </div>';
    }
    public static function imageOther($title, $name, $image = '')
    {
        return '<div class="form-group row">
                   <label for="' . $name . '" class="col-sm-2 control-label">' . $title . '</label>
                   <div class="col-sm-10">
                       <div class="input-group col-sm-8">
                            <span class="input-group-btn">
                                 <a data-input="'.$name.'" data-preview="'.$name.'_holder" class="btn btn-primary lfm">
                                     <i class="fa fa-picture-o"></i> Choose
                                 </a>
                            </span>
                            <input id="'.$name.'" class="form-control" type="text" name="'.$name.'" value="'.$image.'">
                       </div>
                       <div class="col-sm-8">
                        <img id="'.$name.'_holder" src="'.$image.'" width="100%">
                   </div>
                   </div>
                   
                </div>';
    }
    public function loadImageChecked($url){
        if (file_exists($url)) {
            return asset('asset/ui/no-image.png');
        }
        return $url;
    }
}

?>