<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\Page_categories;
use Modules\AdminLte\Repositories\ProductsRepository;

class PageController extends BaseController
{
    protected $productsRepository;
    const PAGES = array(
        'lien-he' => 'Liên Hệ',
        'gioi-thieu' => 'Giới Thiệu',

    );
    function __construct(ProductsRepository $productsRepository){
    	parent::init();
        $this->productsRepository = $productsRepository;
        $this->data['featureProducts'] = $this->productsRepository->getProductsWithLimit(array(
            ['type' , 'LIKE', '%'.self::PRODUCT_FEATURE.'%'],
            'active' => self::ACTIVE

        ), 8);
    }
    public function pageItem(Request $request, $slug){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $this->data['page'] = Pages::where('slug',$slug)->get()->first();

    	$breadcrumb = array();
        $breadcrumb[] = array('name' => 'Chuyên mục' ,'url'=> '#' );
        $breadcrumb[] = array('name' => self::PAGES[$slug] ,'url'=> '');
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
		$this->data['subView'] = 'front.page.partials.content';
    	return view('front.page.index', $this->data);
    }
    public function pageByParentSlug(Request $request, $slug){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $this->data['pages'] = Pages::leftJoin('Page_categories','Page_categories.id','=','Pages.FKCategory')
				    	->where('Page_categories.slug',$slug)
				    	->get([
					        'Pages.*'
					        ,'Page_categories.name as catName'
				        ]);
		$this->data['subView'] = 'front.page.partials.content';
    	return view('front.page.index', $this->data);
    }
    public function section(Request $request, $slug){
        $this->data['viewed'] = $this->productsRepository->getViewd();

    	$page = Pages::where('slug',$slug)->get();
		$this->data['subView'] = 'front.page.partials.list';
    	return view('front.page.index', $this->data);
    }
    public function pageBannerItem(Request $request, $slug){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $this->data['pageBanner'] = Pages::where('slug',$slug)->get()->first();
        $this->data['banner'] = true;
        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Tin khuyến mãi' ,'url'=> '#' );
//        $breadcrumb[] = array('name' => !empty($this->data['page'])? $this->data['page']->name: "" ,'url'=> '#');
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.page.partials.content';
        return view('front.page.index', $this->data);
    }
}
