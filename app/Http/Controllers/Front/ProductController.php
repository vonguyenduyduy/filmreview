<?php

namespace App\Http\Controllers\Front;

use App\Repositories\ProductCategoriesRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\AdminLte\Repositories\ProductsRepository;
use Redirect,Cookie;
use Illuminate\Support\Facades\Cache;
use App\Models\Products;
use App\Models\Product_categories;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Session;

class ProductController extends BaseController
{
    protected $productsRepository;
    protected $productCategoriesRepository;
    const ACTIVE = 1;
    public function __construct(ProductsRepository $productsRepository, ProductCategoriesRepository $productCategoriesRepository){
        parent::init();
        $this->productsRepository = $productsRepository;
        $this->productCategoriesRepository = $productCategoriesRepository;
        $this->data['categories'] = $this->productCategoriesRepository->findWhere(array('active' => self::ACTIVE));
        $this->data['featureProducts'] = $this->productsRepository->getProductsWithLimit(array(
            ['type' , 'LIKE', '%'.self::PRODUCT_FEATURE.'%'],
            'active' => self::ACTIVE

        ), 8);
    }
    public function detail(Request $request,$slug1, $slug2,$id){
    	$detail = $this->productsRepository->find($id) ;
    	//Cache::forget('viewed');
    	$cache = [];
        if (session()->has('viewed')) {
            $cache = Session::get('viewed');
            if(!in_array($id, $cache)){
                $cache[] = $id;
                session()->put('viewed', $cache);
            }
        }else{
            $cache[] = $id;
            $request->session()->put('viewed', [$id]);
        }

        $this->data['viewed'] = $this->productsRepository->getViewd();
        if(count($detail) == 0){
            return Redirect::to('/');
        }
    	$this->data['relations'] = $this->productsRepository->scopeQuery(function ($q){
    	    return $q->limit(12);
        })->with('category')->findWhere(['FKCategory' => $detail->FKCategory, 'active' => $this::ACTIVE]);
        $this->data['detail'] = $detail;
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=>  route('front.category.all'));
        $breadcrumb[] =  array('name' => $detail->name ,'url'=> '' );

        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.detail';
    	return view('front.product.index',$this->data);
    }
    public function allProducts(){
        $this->data['products'] = $this->productsRepository->orderBy('orderBy', 'asc')->findWhere(array('active' => self::ACTIVE));
        $this->data['subView'] = 'front.product.partials.list-products';
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=>  route('front.category.all'));
        $breadcrumb[] =  array('name' => trans('front.all_products') ,'url'=> '' );

        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        return view('front.product.index',$this->data);
    }

    public function listCategories(){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=>  route('front.category.all'));
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.categories';
        return view('front.product.index',$this->data);
    }
    public function listSubCategories($slug,$id){
        $this->data['viewed'] = $this->productsRepository->getViewd();
        $parentCategory = $this->productCategoriesRepository->find($id);
        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=> route('front.category.all') );
        $breadcrumb[] =  array('name' => $parentCategory->name ,'url'=> '' );
        $this->data['parentSlug'] = $parentCategory->slug;
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.sub-categories';
        return view('front.product.index',$this->data);
    }
    public function getProductsOfCategories($slug1,$id){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $this->data['products'] = $this->productsRepository->with('category')->orderBy('orderBy', 'asc')->findWhere(['FKCategory' => $id, 'active' => self::ACTIVE]);
        $this->data['fkCategory'] = $id;
        $this->data['parentCat'] = $this->productCategoriesRepository->find($id);
        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=> route('front.category.all') );
        $breadcrumb[] = array('name' => $this->data['parentCat']->name ,'url'=> route('front.category.level1', [$this->data['parentCat']->slug, $id]) );
        $this->data['proTitle'] = $this->data['parentCat']->name;
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.list-products';
        return view('front.product.index',$this->data);
    }
    public function getProductsOfCategoriesLevel3(Request $request,$slug1,$slug2,$slug3, $id){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $this->data['products'] = $this->productsRepository->orderBy('orderBy', 'desc')->findWhere(['FKCategory' => $id, 'active' => $this::ACTIVE]);
        $this->data['fkCategory'] = $id;
        $cat = $this->productCategoriesRepository->find($id);
        $this->data['parentCat'] = $this->productCategoriesRepository->find($cat->parent);
        $parent = $this->productCategoriesRepository->find($this->data['parentCat']->parent);

        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=> 'danh-muc-san-pham' );
        $breadcrumb[] = array('name' => $this->data['parentCat']->name ,'url'=> 'danh-muc-san-pham/'.$parent->slug.'/'.$this->data['parentCat']->slug.'/'.$this->data['parentCat']->id );
        $breadcrumb[] = array('name' => $cat->name ,'url'=> '' );

        $this->data['proTitle'] = $this->data['parentCat']->name;
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.list-products';
        $this->data['routeApiFilter'] = route('api.products.filter');
        return view('front.product.index',$this->data);
    }
    public function hotProducts(){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=> 'danh-muc-san-pham' );
        $breadcrumb[] = array('name' => "Sản phẩm HOT" ,'url'=> '#' );
        $this->data['proTitle'] = 'Sản phẩm HOT';
        $this->data['fkCategory'] = 0;
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.list-products';
        $this->data['routeApiFilter'] = route('api.products.filter');
        return view('front.product.index',$this->data);
    }
    public function viewedProducts(){
        $this->data['viewed'] = $this->productsRepository->getViewd();

        $breadcrumb = array();
        $breadcrumb[] = array('name' => 'Danh mục sản phẩm' ,'url'=> 'danh-muc-san-pham' );
        $breadcrumb[] = array('name' => "Sản phẩm đã xem" ,'url'=> '#' );
        $this->data['proTitle'] = 'Sản phẩm đã xem';
        $this->data['fkCategory'] = 'viewed';
        $this->data['breadcrumb'] = getBreadcrumb($breadcrumb);
        $this->data['subView'] = 'front.product.partials.list-products';
        $this->data['routeApiFilter'] = route('api.products.filter');
        return view('front.product.index',$this->data);
    }
    public function filter(Request $request){
        if($request->get('fkCategory') == 'viewed'){
            $products = $this->productsRepository->getViewd();
            return response()->json($products);
        }
        if($request->get('fkCategory') == '0'){
            $products = $this->productsRepository->findWhere([['type','like','%PT03%'], ['fkCategory', '<>', 0], 'active' => $this::ACTIVE]);
            return response()->json($products);
        }
        if($request->get('type') == 'false'){
            $products = $this->productsRepository->orderBy('orderBy', 'desc')->findWhere(['FKCategory'=> $request->get('fkCategory')]);
        }else{
            $query = Products::where('FKCategory',$request->get('fkCategory'))->where('active', $this::ACTIVE);
            switch($request->get('type')){
                case 'name':
                    $query->orderBy('name', $request->get('sort'));
                    break;
                case 'new':
                    $query->orderByRaw("type = 'PT02' ".$request->get('sort'));
                    break;
                case 'bonus':
                    $query->orderByRaw("type = 'PT01' ".$request->get('sort'));

                    break;
                case 'priceAsc':
                    $query->orderBy('price', 'asc');

                    break;
                case 'priceDesc':
                    $query->orderBy('price', 'desc');
                    break;
            }
            $products = $query->get();
        }
        return response()->json($products);
    }
}
