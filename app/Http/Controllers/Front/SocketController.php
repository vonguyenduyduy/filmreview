<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use LRedis;
class SocketController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function index()
    {
        return view('front.chat.writemessage-anonys');
    }
    public function writemessage()
    {
        return view('front.chat.writemessage');
    }
    public function sendMessage(Request $request){
	$req = $request->all();
        $redis = LRedis::connection();
        $data = ['message' => $req['message'], 'user' => $req['user']];
        $redis->publish('message', json_encode($data));
        return redirect('writemessage');
    }
}

