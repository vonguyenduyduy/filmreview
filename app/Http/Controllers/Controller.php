<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB,Auth,Redirect,Cart;
use App\Models\Product_categories;
use App\Models\Page_categories;
use App\Models\Images;
use App\Models\Options;
use App\Models\Pages;
use App\Repositories\ProductCategoriesRepository;

class Controller extends BaseController
{
    public $data;
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $categoriesRepository;
    function __construct(){

    }
    public function init(){
		$this->data['currentUser'] = false;
		$cart = array(
    		'content' => Cart::content(), 
    		'total' => Cart::total(), 
    		'tax' => Cart::tax(), 
    		'subtotal' => Cart::subtotal(), 
    		'count' =>  Cart::count()
    		);
        $this->data['cart1'] = json_encode($cart);
        $this->data['floor'] = Product_categories::getAllFloor();
        $this->data['catlev1'] = Product_categories::where("parent",0)->get();

        $this->data['cart'] = $cart;
        $options = Options::where('active',1)->get();
        $opt = array();
        foreach ($options as $k => $v) {
            $opt[$v->optionID] = $v->value;
        }
        $this->data['options'] = $opt;
        $this->data['support'] = Pages::getPageByParentCategorySlug('ho-tro-khach-hang');
        $this->data['user'] = session()->get('user');
        $this->data['logo'] = Pages::where('slug','logo')->get()->first();

    }
}
