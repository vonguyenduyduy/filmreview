<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model {

    public static function newProduct() {
        $row = new self;
        $row->name = "";
        $row->FKCategory = "";
        $row->slug = "";
        $row->images = "";
        $row->thumbnail = "";
        $row->description = "";
        $row->content = "";
        $row->active = 1;
        $row->price = 0;
        $row->orderBy = 0;
        $row->type = '';
        $row->parentID = 0;
        $row->tags = '';
        $row->sales = 0;

        $row->created_at = "";
        $row->updated_at = "";
        return $row;
    }
    public function getParameter(){
        return $this->hasOne(Parameters::class,"paramID", "type");
    }

    public static function DeleteByID($listID) {
        if (!empty($listID)) {
            try {
                foreach ($listID as $v) {
                    Products::where('id', $v)->delete();
                }
                return array('success' => true, "message" => $listID);
            } catch (Exception $e) {
                return array('success' => false, 'message' => $e);
            }
        }
    }

}
