<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    public static function newCat(){
        $row = new self;
        $row->name = "";
        $row->FKCategory = "";
        $row->slug = "";
        $row->images = "";
        $row->thumbnail = "";
        $row->description = "";
        $row->content = "";
        $row->active = 1;
        $row->orderBy = 0;
        $row->created_at = "";
        $row->updated_at = "";
        return $row;
   }
   public static function DeleteByID($listID){
        if(!empty($listID)){
            try{
                foreach ($listID as $v) {
                    Pages::where('id', $v)->delete();
                }
                return array('success' => true,"message"=> $listID);
            }catch(Exception $e){
                return array('success' => false, 'message' => $e);
            }
            
        }
   }

    public function getPageCategory(){
        return $this->hasOne(Page_categories::class, "id", "FKCategory");
    }

    public static function getPageByParentCategorySlug($slug){
        return Pages::with('getPageCategory')->whereHas('getPageCategory', function ($query ) use ($slug) {
            return $query->where(["slug" => $slug]);
        })->where(["active" => 1])->orderBy('orderBy','asc')->get();
    }
}
