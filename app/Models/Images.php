<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File;

class Images extends Model
{
   public function parameters()
    {
        return $this->hasMany('App\Models\Parameters','paramID', 'type');
    }

    public static function getByType($paramID = ''){
      if($paramID != ''){
         $paras = Images::leftJoin('parameters','parameters.paramID','=','images.type')->where('type',$paramID)->get([
        'images.*'
        ,'parameters.value as paramName'
        ]);
       }else{
         $paras = Images::leftJoin('parameters','parameters.paramID','=','images.type')->get([
        'images.*'
        ,'parameters.value as paramName'
        ]);
       }
      return $paras;
    }
   // public static $rules = [
   //      'file' => 'required|mimes:png,gif,jpeg,jpg,bmp'
   //  ];

   //  public static $messages = [
   //      'file.mimes' => 'Uploaded file is not in image format',
   //      'file.required' => 'Image is required'
   //  ];
    public static function saveImage($param){
   		try {
	            $row = new self;
	            $row->url = $param['url'];
	            $row->type = $param['type'];
	            $row->active = 1;
	            $row->save();
            return array('success' => true,"message"=> $row);
        } catch (Exception $e) {
            return array('success' => false, 'message' => $e);
        }
   }
   public static function DeleteByID($listID){
      if(!empty($listID)){
        try{
          foreach ($listID as $v) {
            $checkExits = Images::where('id', $v)->get()->first();
            $base_url = base_path() . '/public/asset/images/'. $checkExits->url;
            if(File::exists($base_url)){
              File::delete($base_url);
              Images::where('id', $v)->delete();
            }else{
              return array('success' => false, 'message' => 'Hình ảnh không có trong hệ thống');
            }
            
          }
          return array('success' => true,"message"=> $listID);
        }catch(Exception $e){
              return array('success' => false, 'message' => $e);
        }
        
      }
   }
}
