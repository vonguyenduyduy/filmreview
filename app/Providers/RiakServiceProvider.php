<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\FormHelpers;

class RiakServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FormHelpers::class, function ($app) {
            return new FormHelpers();
        });
    }
}
