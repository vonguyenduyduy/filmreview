<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \App\Models\Users::delete();
        \App\Models\Users::create(array(
            'name'     => 'zet daica',
            'username' => 'admin',
            'password' => Hash::make('abc123'),
        ));
    }
}
