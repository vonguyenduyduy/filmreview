<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVietlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vietlots', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('number_1');
            $table->tinyInteger('number_2');
            $table->tinyInteger('number_3');
            $table->tinyInteger('number_4');
            $table->tinyInteger('number_5');
            $table->tinyInteger('number_6');
            $table->string('date_lot')->nullable();
            $table->string('temp_string')->nullable();
            $table->string('temp_int')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vietlots');
    }
}
