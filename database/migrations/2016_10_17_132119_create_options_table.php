<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('optionID');
            $table->string('name');
            $table->string('value');
            $table->integer('active');
            $table->timestamps();
        });
        $options = array(
          array('id' => '1','optionID' => 'hot-line','name' => 'Hot line','value' => '0909587557','active' => '1','created_at' => NULL,'updated_at' => '2016-10-17 14:13:50'),
          array('id' => '2','optionID' => 'phone','name' => 'Phone','value' => '08.6672.0909 - 08.3812.3813','active' => '1','created_at' => NULL,'updated_at' => NULL),
          array('id' => '3','optionID' => 'cong-ty','name' => 'Công ty','value' => 'Công ty TNHH MTV Thương Mại và Dịch Vụ','active' => '1','created_at' => NULL,'updated_at' => '2016-10-17 14:13:46'),
          array('id' => '4','optionID' => 'mst','name' => 'MST','value' => '0313023936 Cấp ngày: 22/11/2014','active' => '1','created_at' => NULL,'updated_at' => '2016-10-17 14:14:43'),
          array('id' => '5','optionID' => 'dia-chi','name' => 'Địa chỉ','value' => 'Phường 13, Quận Bình Thạnh, Tp. Hồ Chí Minh','active' => '1','created_at' => NULL,'updated_at' => '2016-10-17 14:13:43'),
          array('id' => '6','optionID' => 'dia-chi-trung-bay-va-ban-san-pham','name' => 'Địa chỉ trưng bày và bán sản phẩm','value' => 'abc','active' => '1','created_at' => NULL,'updated_at' => '2016-10-17 14:13:35')
        );
      \Illuminate\Support\Facades\DB::table('options')->insert($options);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
