<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paramID');
            $table->string('value');
            $table->string('paramCode');
            $table->integer('active');
            $table->timestamps();
        });
        $value =  array(
            array('PT01', 'Khuyến mãi', 'ProductType', 1),
            array('PT02', 'Nổi bật', 'ProductType', 1),
            array('IT01', 'Category', 'ImageType', 1),
            array('IT02', 'Product', 'ImageType', 1),
            array('IT03', 'Page', 'ImageType', 1)
        );
        $col = array(
            'paramID',
            'value',
            'paramCode',
            'active'
        );
        foreach ($value as $item){
            \Illuminate\Support\Facades\DB::table('parameters')->insert(
                [
                    $col[0] => $item[0],
                    $col[1] => $item[1],
                    $col[2] => $item[2],
                    $col[3] => $item[3],
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters');
    }
}
