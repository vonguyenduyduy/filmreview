<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('type');
            $table->boolean('active');
            $table->timestamps();
        });
        $images = array(
  array('id' => '1','url' => '032553_420_crop_xe3bconvoiweb.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '2','url' => '034523_xe_3_banh_family_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '3','url' => '034844_xe_3_banh_family_ghe_nga_cho_be3.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '4','url' => '035256_xe_3_banh_hinh_gau_truc_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '5','url' => '035429_xe_3_banh_hinh_cong_cho_be_hong.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '6','url' => '035549_xe_3_banh_hinh_con_bo_cho_be_xanh.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '7','url' => '035918_xe_dap_12_inch_ronin_chuot_cho_be3.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '8','url' => '040548_xe_dap_12_inch_con_meo.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '9','url' => '040659_xe_dap_12_inch_hotgirl.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '10','url' => '040835_xe_dap_12_inch_ronin_chuot_cho_be4.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '11','url' => '041123_xe_dap_tre_em_12_in_hello_kittin.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '12','url' => '041316_xe_dap_tre_em_14_inch_xanh_trang_race_do_den.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '13','url' => '041507_xe_day_qq3_1.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '14','url' => '041653_xe_day_tre_em_jollybaby_gc112.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '15','url' => '041837_xe_day_tre_em_the_first_years_y11100.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '16','url' => '042032_xe_day_tre_em_the_first_years_y11220.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '17','url' => '042210_xe_day_baohaohao_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '18','url' => '042511_xe_day_tre_em_gluck_c_8_mau_do.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '19','url' => '042743_xe_day_tre_em_635c.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '20','url' => '042927_xe_day_tre_em_gluck_b_60c.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '21','url' => '043132_xe_day_tap_di_cho_be_k2_kn_su_tu_babymartvn.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '22','url' => '043307_xe_day_tap_di_k1_m1463_bb7.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '23','url' => '043439_xe_tap_di_autoru_cao_cap.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '24','url' => '043609_xe_tap_di_bang_go_song_son_cho_be_new.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '25','url' => '043737_xe_tap_di_bang_go_duc_thanh_cho_be11.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '26','url' => '043949_bap_benh_ngua_go_xanh_duong.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '27','url' => '044113_bon_tam_bom_hoi_disney_pixar_cars_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '28','url' => '044327_do_choi_go_danh_golf_4_thu.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '29','url' => '044511_do_choi_go_tro_choi_bowling.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '30','url' => '044713_do_choi_go_vit_con_lat_dat_winwintoys.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '31','url' => '044847_bo_cau_sv_bien.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '32','url' => '045049_bo_do_choi_3_con_vit.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '33','url' => '045225_bo_do_choi_dung_cu_bac_si_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '34','url' => '045436_bo_dung_cu_cham_soc_cay.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '35','url' => '045807_bo_dung_cu_co_khi_tamir_seti_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '36','url' => '045923_bo_dung_cu_nau_an_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '37','url' => '050125_bo_lap_ghep_qua_bong_tombul_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '38','url' => '050304_bo_xe_container_va_12_xe_hoi_nho_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '39','url' => '050508_224.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '40','url' => '050644_untitled_1.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '41','url' => '050841_xep_hinh_226.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '42','url' => '050959_227.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '43','url' => '051116_bang_hoc_tu_tieng_anh_cho_be.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '44','url' => '102218_toy1.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '45','url' => '113255_310mrisvn5l.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '46','url' => '113406_310mrisvn5l.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '47','url' => '123750_toy1.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '48','url' => '123846_310mrisvn5l.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '49','url' => '135638_xe_lac_xe_hoi_sieu_nhan_cho_be_khong_nhac.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '50','url' => '140635_an_toan.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '51','url' => '140651_38965a106f0800c45ebfbdaf0824cf63.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '52','url' => '143025_cau_ca_winwintoys.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '53','url' => '155700_hl_branding.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '54','url' => '160352_2.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '55','url' => 'background-banner-morinaga.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '56','url' => 'banner1-tv.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '57','url' => 'dathongbao.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '58','url' => 'do-choi-tre-em-bang-go---xe-dua-f1.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '59','url' => 'logo.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '60','url' => 'mieng-lot-bambi-mio-ban-dem-size-m-babymartvn.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '61','url' => 'morinaga_title.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '62','url' => 'phone.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '63','url' => 'text-1-list-tv.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '64','url' => 'text-2-list-tv.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '65','url' => 'trung-thu-2-9-banner.jpg','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '66','url' => 'trung-thu-2-9-title.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '67','url' => 'xemchitiet.png','type' => 'IT02','active' => '1','created_at' => NULL,'updated_at' => NULL),
  array('id' => '68','url' => '062427_dochoivabe_full_27402015_124052.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:24:27','updated_at' => '2016-10-19 06:24:27'),
  array('id' => '69','url' => '062803_xediahinh_1.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:28:03','updated_at' => '2016-10-19 06:28:03'),
  array('id' => '70','url' => '062918_do_choi_go.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:29:18','updated_at' => '2016-10-19 06:29:18'),
  array('id' => '71','url' => '063008_medium_large_02_2012_8ecebf16c698d7c059a95269f24b90fagif.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:30:08','updated_at' => '2016-10-19 06:30:08'),
  array('id' => '72','url' => '063058_maxresdefault.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:30:58','updated_at' => '2016-10-19 06:30:58'),
  array('id' => '73','url' => '063729_hqdefault.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:37:29','updated_at' => '2016-10-19 06:37:29'),
  array('id' => '74','url' => '063848_choi_lego3.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:38:48','updated_at' => '2016-10-19 06:38:48'),
  array('id' => '75','url' => '064013_dfn1387957484.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:40:13','updated_at' => '2016-10-19 06:40:13'),
  array('id' => '76','url' => '064139_cm_b32809.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:41:39','updated_at' => '2016-10-19 06:41:39'),
  array('id' => '77','url' => '064222_ban_do_choi_bup_be_barbie_gia_re_bo_suu_tap_thoi_trang_nganh_nghe_430198j19722x300x300.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:42:22','updated_at' => '2016-10-19 06:42:22'),
  array('id' => '78','url' => '064307_maxresdefault_1.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:43:07','updated_at' => '2016-10-19 06:43:07'),
  array('id' => '79','url' => '064406_30pcs_kids_pretend_role_play_font_b_supermarket_b_font_font_b_set_b_font_basket.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:44:06','updated_at' => '2016-10-19 06:44:06'),
  array('id' => '80','url' => '064517_hoa_dep.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:45:17','updated_at' => '2016-10-19 06:45:17'),
  array('id' => '81','url' => '064606_248809759bo_nhac_cu16.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:46:06','updated_at' => '2016-10-19 06:46:06'),
  array('id' => '82','url' => '064749_bo_lap_rap_sang_tao_winwintoys_64302_xe.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:47:49','updated_at' => '2016-10-19 06:47:49'),
  array('id' => '83','url' => '064854_ngang21388152515.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:48:54','updated_at' => '2016-10-19 06:48:54'),
  array('id' => '84','url' => '065005_top_10_do_choi_an_toan_cho_be_so_sinh_2.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:50:05','updated_at' => '2016-10-19 06:50:05'),
  array('id' => '85','url' => '065110_lua_qua_16_cho_be_theo_do_tuoi_1.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:51:10','updated_at' => '2016-10-19 06:51:10'),
  array('id' => '86','url' => '065245_thu_go_mon_do_choi_go_an_toan_va_huu_ich_cho_be_ma_cac_ba_me_nen_quan_tam.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:52:45','updated_at' => '2016-10-19 06:52:45'),
  array('id' => '87','url' => '065343_758044ad10d8bb00990385c3b981a0a5.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:53:43','updated_at' => '2016-10-19 06:53:43'),
  array('id' => '88','url' => '065422_maxresdefault_2.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:54:22','updated_at' => '2016-10-19 06:54:22'),
  array('id' => '89','url' => '065529_17.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:55:29','updated_at' => '2016-10-19 06:55:29'),
  array('id' => '90','url' => '065621_hut_sua_bang_tay_lasinoh_1_copyright_yeucon247com_chuyen_binh_sua_do_choi_do_dung_thuc_pham_ngoai_nhap_cao_cap_danh_cho_me_va_be.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:56:21','updated_at' => '2016-10-19 06:56:21'),
  array('id' => '91','url' => '065724_20131112101052.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:57:24','updated_at' => '2016-10-19 06:57:24'),
  array('id' => '92','url' => '065817_xe_tap_di_bang_go.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:58:17','updated_at' => '2016-10-19 06:58:17'),
  array('id' => '93','url' => '065858_lcj1413455781.JPG','type' => 'IT01','active' => '1','created_at' => '2016-10-19 06:58:58','updated_at' => '2016-10-19 06:58:58'),
  array('id' => '94','url' => '070001_xedap.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 07:00:01','updated_at' => '2016-10-19 07:00:01'),
  array('id' => '95','url' => '070055_xe_o_to_dien_tre_em_police_max_1118_1.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 07:00:55','updated_at' => '2016-10-19 07:00:55'),
  array('id' => '96','url' => '072202_bo_lap_rap_sang_tao_winwintoys_64302_xe.jpg','type' => 'IT02','active' => '1','created_at' => '2016-10-19 07:22:02','updated_at' => '2016-10-19 07:22:02'),
  array('id' => '97','url' => '074117_0635042131711492386.jpg','type' => 'IT02','active' => '1','created_at' => '2016-10-19 07:41:17','updated_at' => '2016-10-19 07:41:17'),
  array('id' => '98','url' => '075051_gau_bong_tot_nghiep.png','type' => 'IT02','active' => '1','created_at' => '2016-10-19 07:50:51','updated_at' => '2016-10-19 07:50:51'),
  array('id' => '99','url' => '075346_offre_speciale_pikachu_jouets_en_peluche_tres_mignon_pokemon_jouets_en_peluche_pokemon_douce_peluche_jouets.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 07:53:46','updated_at' => '2016-10-19 07:53:46'),
  array('id' => '100','url' => '075556_e247221e03b74c1affd1e9545b1cd7c5.jpg','type' => 'IT02','active' => '1','created_at' => '2016-10-19 07:55:56','updated_at' => '2016-10-19 07:55:56'),
  array('id' => '101','url' => '075613_e247221e03b74c1affd1e9545b1cd7c5.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 07:56:13','updated_at' => '2016-10-19 07:56:13'),
  array('id' => '102','url' => '075827_extendimage_1658_d3.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 07:58:27','updated_at' => '2016-10-19 07:58:27'),
  array('id' => '103','url' => '080115_huong_dan_cach_chon_bim_ta_lot_tot_cho_tre_so_sinh_cho_cac_ba_me_4.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:01:15','updated_at' => '2016-10-19 08:01:15'),
  array('id' => '104','url' => '080239_bo_2_binh_sua_comotomo_250ml_1448426587.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:02:39','updated_at' => '2016-10-19 08:02:39'),
  array('id' => '105','url' => '080434_146_ghe_an_em_be_kora_co_nem.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:04:34','updated_at' => '2016-10-19 08:04:34'),
  array('id' => '106','url' => '080520_210_combo_cui_xuat_khao_70.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:05:20','updated_at' => '2016-10-19 08:05:20'),
  array('id' => '107','url' => '080616_xe_day_em_be.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:06:16','updated_at' => '2016-10-19 08:06:16'),
  array('id' => '108','url' => '080733_song_long_f1_3_logo.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:07:33','updated_at' => '2016-10-19 08:07:33'),
  array('id' => '109','url' => '080831_xe_dap_cho_be_gai_mau_hong_totem_1139_750x750.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:08:31','updated_at' => '2016-10-19 08:08:31'),
  array('id' => '110','url' => '081025_xe_choi_chan_o_to_grand_coupe_little_tikes_lt_445830091_cho_be.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:10:25','updated_at' => '2016-10-19 08:10:25'),
  array('id' => '111','url' => '081112_xe_may_dien_phan_khoi_lon.jpg','type' => 'IT01','active' => '1','created_at' => '2016-10-19 08:11:12','updated_at' => '2016-10-19 08:11:12'),
  array('id' => '112','url' => '081438_xe_con_gau_edugames_ga548_3.jpg','type' => 'IT02','active' => '1','created_at' => '2016-10-19 08:14:38','updated_at' => '2016-10-19 08:14:38')
);
            \Illuminate\Support\Facades\DB::table('images')->insert($images);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
