<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('FKCategory');
            $table->string('name');
            $table->string('slug');
            $table->string('images');
            $table->string('thumbnail');
            $table->string('type');
            $table->text('description');
            $table->longtext('content');
            $table->integer('price');
            $table->longtext('active');
            $table->longtext('orderBy');
            $table->integer('parentID');

            $table->timestamps();
        });
        $products = array(
  array('id' => '2','FKCategory' => '24','name' => 'Xe con gấu Edugames GA548','slug' => 'xe-con-gau-edugames-ga548','images' => '081438_xe_con_gau_edugames_ga548_3.jpg','thumbnail' => '','type' => '0','description' => '<ul>
    <li>K&iacute;ch thước: 10 x 15 x 10 cm</li>
    <li>D&agrave;nh cho b&eacute; tr&ecirc;n 2 tuổi</li>
    <li>Xuất xứ: Việt Nam</li>
</ul>
','content' => '','price' => '117000','active' => '1','orderBy' => '0','parentID' => '1','created_at' => '2016-10-19 08:16:01','updated_at' => '2016-10-19 08:16:01'),
  array('id' => '3','FKCategory' => '10','name' => 'xe 3 bánh Family cho bé','slug' => 'xe-3-banh-family-cho-be','images' => '034523_xe_3_banh_family_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/baby-bedside-bell.html">Family</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X075</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;90 x 50 x 65</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.9</p>

            <p>-&nbsp;<strong>&nbsp;Chất liệu khung:</strong>&nbsp;Khung bằng kim loại v&agrave; c&aacute;c bộ phận kh&aacute;c bằng nhựa.<br />
                - &nbsp;<strong>Trọng lượng tối đa:</strong>&nbsp;25 kg<br />
                - &nbsp;<strong>Nhạc:</strong>&nbsp;c&oacute;<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Trung Quốc</p>
                ','content' => '','price' => '720000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 03:50:25','updated_at' => '2016-10-06 03:50:25'),
  array('id' => '4','FKCategory' => '10','name' => 'xe 3 bánh Family ghế ngã cho bé','slug' => 'xe-3-banh-family-ghe-nga-cho-be','images' => '034844_xe_3_banh_family_ghe_nga_cho_be3.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/baby-bedside-bell.html">Family</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X071</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;90 x 50 x 65</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6.3</p>

            <p>-&nbsp;<strong>&nbsp;Chất liệu khung:</strong>&nbsp;Khung bằng kim loại v&agrave; c&aacute;c bộ phận kh&aacute;c bằng nhựa.<br />
                - &nbsp;<strong>Trọng lượng tối đa:</strong>&nbsp;25 kg<br />
                - &nbsp;<strong>Nhạc:</strong>&nbsp;c&oacute;<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Trung Quốc</p>
                ','content' => '','price' => '836000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 03:50:14','updated_at' => '2016-10-06 03:50:14'),
  array('id' => '5','FKCategory' => '10','name' => 'xe 3 bánh hình gấu trúc cho bé','slug' => 'xe-3-banh-hinh-gau-truc-cho-be','images' => '035256_xe_3_banh_hinh_gau_truc_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X087</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;95 x 55 x 75</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.8</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;khung xe bằng kim loại, c&aacute;c bộ phận kh&aacute;c bằng nhựa<br />
                - &nbsp;<strong>K&iacute;ch thước:&nbsp;</strong>940 x 480 x 800 mm&nbsp;<br />
                -&nbsp;&nbsp;<strong>Trọng lượng cho ph&eacute;p:</strong>&nbsp;20 kg<br />
                - &nbsp;<strong>M&agrave;u:</strong>&nbsp;xanh biển v&agrave; xanh l&aacute;<br />
                - &nbsp;<strong>Nhạc:</strong>&nbsp;C&oacute;<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam<br />
                - &nbsp;<strong>Ghi ch&uacute;:</strong>&nbsp;Bạn vui l&ograve;ng ghi m&agrave;u sắc v&agrave;o &ocirc;&nbsp;&quot;th&ocirc;ng tin th&ecirc;m&quot;&nbsp;trong qu&aacute; tr&igrave;nh đặt h&agrave;ng.</p>
                ','content' => '','price' => '498000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 03:53:46','updated_at' => NULL),
  array('id' => '6','FKCategory' => '10','name' => 'xe 3 bánh hình con công cho bé','slug' => 'xe-3-banh-hinh-con-cong-cho-be','images' => '035429_xe_3_banh_hinh_cong_cho_be_hong.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X030</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;95 x 55 x 90</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;khung xe bằng kim loại, c&aacute;c bộ phận kh&aacute;c bằng nhựa<br />
                -&nbsp;<strong>&nbsp;Trọng lượng cho ph&eacute;p:</strong>&nbsp;20 kg<br />
                - &nbsp;<strong>M&agrave;u:&nbsp;</strong>Xanh, Hồng<br />
                - &nbsp;<strong>Nhạc:</strong>&nbsp;C&oacute;<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;<br />
                - &nbsp;<strong>Ghi ch&uacute;:</strong>&nbsp;Qu&yacute; kh&aacute;ch vui l&ograve;ng ghi m&agrave;u sắc v&agrave;o &ocirc;&nbsp;<strong>&quot;th&ocirc;ng tin th&ecirc;m&quot;</strong>&nbsp;trong qu&aacute; tr&igrave;nh đặt h&agrave;ng.</p>
                ','content' => '','price' => '498000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 03:55:18','updated_at' => NULL),
  array('id' => '7','FKCategory' => '10','name' => 'xe 3 bánh hình con bò cho bé','slug' => 'xe-3-banh-hinh-con-bo-cho-be','images' => '035549_xe_3_banh_hinh_con_bo_cho_be_xanh.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X086</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;95 x 55 x 75</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6</p>

            <p>- &nbsp;<strong>Chất liệu:&nbsp;</strong>khung xe bằng kim loại, c&aacute;c bộ phận kh&aacute;c bằng nhựa<br />
                -&nbsp;&nbsp;<strong>Trọng lượng cho ph&eacute;p:</strong>&nbsp;20 kg<br />
                - &nbsp;<strong>M&agrave;u:&nbsp;</strong>Xanh,&nbsp;cam<br />
                - &nbsp;<strong>Nhạc:</strong>&nbsp;C&oacute;<br />
                - &nbsp;<strong>Nơi sản xuất:&nbsp;</strong>Việt Nam<br />
                - &nbsp;<strong>Ghi ch&uacute;:</strong>&nbsp;Bạn&nbsp;vui l&ograve;ng ghi m&agrave;u sắc v&agrave;o &ocirc;<strong>&nbsp;&quot;th&ocirc;ng tin th&ecirc;m&quot;</strong>&nbsp;trong qu&aacute; tr&igrave;nh đặt h&agrave;ng.</p>
                ','content' => '','price' => '498000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 03:57:18','updated_at' => NULL),
  array('id' => '8','FKCategory' => '11','name' => 'xe đạp 14 inch Chuột xanh lá','slug' => 'xe-dap-14-inch-chuot-xanh-la','images' => '035918_xe_dap_12_inch_ronin_chuot_cho_be3.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X001</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;95 x 60 x 75</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6.5</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại&nbsp;</p>

            <p>- &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ</p>

            <p>- &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:&nbsp;</strong>cho b&eacute; 4 - 6 tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
            ','content' => '','price' => '423000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:00:42','updated_at' => NULL),
  array('id' => '9','FKCategory' => '11','name' => 'xe đạp 12 inch chuột xanh lá','slug' => 'xe-dap-12-inch-chuot-xanh-la','images' => '035918_xe_dap_12_inch_ronin_chuot_cho_be3.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X081</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;90 x 52 x 70</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.8</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại&nbsp;</p>

            <p>- &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ</p>

            <p>- &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; 2 - 4 tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
            ','content' => '','price' => '382000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:04:19','updated_at' => NULL),
  array('id' => '10','FKCategory' => '11','name' => 'xe đạp 12 inch con mèo','slug' => 'xe-dap-12-inch-con-meo','images' => '040548_xe_dap_12_inch_con_meo.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X036</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;87 x 52 x 74</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại&nbsp;<br />
                - &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ<br />
                - &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; 2 - 4 tuổi<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
                ','content' => '','price' => '436000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:06:20','updated_at' => NULL),
  array('id' => '11','FKCategory' => '11','name' => 'xe đạp 12 inch Hotgirl','slug' => 'xe-dap-12-inch-hotgirl','images' => '040659_xe_dap_12_inch_hotgirl.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X003</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;85 x 55 x 75</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.2</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại&nbsp;<br />
                - &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ<br />
                - &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:&nbsp;</strong>cho b&eacute; 2 - 4 tuổi<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
                ','content' => '','price' => '380000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:07:38','updated_at' => NULL),
  array('id' => '12','FKCategory' => '11','name' => 'xe đạp 12 inch Ronin cho bé','slug' => 'xe-dap-12-inch-ronin-cho-be','images' => '040835_xe_dap_12_inch_ronin_chuot_cho_be4.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X018</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;90 x 52 x 70</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.8</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại&nbsp;</p>

            <p>- &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ</p>

            <p>- &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; 2 - 4 tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
            ','content' => '','price' => '382000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:09:14','updated_at' => NULL),
  array('id' => '13','FKCategory' => '11','name' => 'xe đạp 14 inch Hotgirl','slug' => 'xe-dap-14-inch-hotgirl','images' => '040659_xe_dap_12_inch_hotgirl.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X089</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;95 x 60 x 75</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.7</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại&nbsp;<br />
                - &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ<br />
                - &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:&nbsp;</strong>cho b&eacute; 4 - 6 tuổi<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
                ','content' => '','price' => '423000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:10:40','updated_at' => NULL),
  array('id' => '14','FKCategory' => '11','name' => 'xe đạp trẻ em 12 inch hello kittin','slug' => 'xe-dap-tre-em-12-inch-hello-kittin','images' => '041123_xe_dap_tre_em_12_in_hello_kittin.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X080</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;87 x 49 x 68</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.5</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại</p>

            <p>- &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ</p>

            <p>- &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c</p>

            <p>- &nbsp;<strong>Trọng lượng tối đa:</strong>&nbsp;20kg</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; 3 - 4 tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '412000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:12:06','updated_at' => NULL),
  array('id' => '15','FKCategory' => '11','name' => 'xe đạp trẻ em 12 inch xanh trắng ','slug' => 'xe-dap-tre-em-12-inch-xanh-trang','images' => '041316_xe_dap_tre_em_14_inch_xanh_trang_race_do_den.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X010</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;87 x 49 x 68</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;5.5</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Kim loại</p>

            <p>- &nbsp;<strong>Kiểu xe:</strong>&nbsp;2 b&aacute;nh ch&iacute;nh, 2 b&aacute;nh phụ</p>

            <p>- &nbsp;<strong>Kiểu v&agrave;nh:</strong>&nbsp;V&agrave;nh đ&uacute;c</p>

            <p>- &nbsp;<strong>Trọng lượng tối đa:</strong>&nbsp;20kg</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; 3 - 4 tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '412000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:13:54','updated_at' => NULL),
  array('id' => '16','FKCategory' => '12','name' => 'xe đẩy bé Seebaby QQ3','slug' => 'xe-day-be-seebaby-qq3','images' => '041507_xe_day_qq3_1.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/see-baby.html">See Baby</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X046</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;100 x 50 x 25</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;4.7</p>

            <p>- &nbsp;<strong>Trọng lượng tối đa đề nghị</strong><strong>:</strong>&nbsp;18 kg.<br />
                - &nbsp;<strong>M&agrave;u sắc:</strong>&nbsp;Xanh l&aacute;, T&iacute;m<br />
                - &nbsp;<strong>Nơi&nbsp;sản xuất</strong><strong>:</strong>&nbsp;Hong Kong</p>
                ','content' => '','price' => '690000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:16:05','updated_at' => NULL),
  array('id' => '17','FKCategory' => '12','name' => 'xe đẩy trẻ em JollyBaby GC11','slug' => 'xe-day-tre-em-jollybaby-gc11','images' => '041653_xe_day_tre_em_jollybaby_gc112.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/comfort.html">JollyBaby</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C553</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;111 x 31 x 35</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6.5</p>

            <p>-&nbsp;<strong>&nbsp;K&iacute;ch thước</strong><strong>:&nbsp;</strong>68 x 48 x 101 cm (d&agrave;i x rộng x cao)</p>

            <p>- &nbsp;<strong>Trọng lượng cho ph&eacute;p tối đa:</strong>&nbsp;25&nbsp;kg<br />
                - &nbsp;<strong>Chế độ:</strong>&nbsp;ngồi v&agrave; ngả.</p>

                <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Trung Quốc</p>
                ','content' => '','price' => '944000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:17:46','updated_at' => NULL),
  array('id' => '18','FKCategory' => '12','name' => 'xe đẩy trẻ em The First Years Y1110','slug' => 'xe-day-tre-em-the-first-years-y1110','images' => '041837_xe_day_tre_em_the_first_years_y11100.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/The-First-Years.html">The First Years</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;V232</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;110 x 30 x 30</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;7.1</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Vải, nhựa, kim loại</p>

            <p>- &nbsp;<strong>K&iacute;ch thước:</strong>&nbsp;76 x 48 x 100 cm (d x r x c)</p>

            <p>- &nbsp;<strong>Trọng lượng cho ph&eacute;p tối đa:&nbsp;</strong>23kg</p>

            <p>- &nbsp;<strong>Chế độ:&nbsp;</strong>ngồi v&agrave; ngả</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Trung Quốc (ti&ecirc;u chuẩn&nbsp;The First Years)</p>

            <p>- &nbsp;<strong>Phương thức giao h&agrave;ng:</strong>&nbsp;Sản phẩm n&agrave;y được giao tận nơi&nbsp;<strong>MIỄN PH&Iacute;</strong>&nbsp;trong nội th&agrave;nh TP.HCM.</p>
            ','content' => '','price' => '3587000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:19:35','updated_at' => NULL),
  array('id' => '19','FKCategory' => '12','name' => 'xe đẩy trẻ em The First Years Y111220','slug' => 'xe-day-tre-em-the-first-years-y111220','images' => '042032_xe_day_tre_em_the_first_years_y11220.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/The-First-Years.html">The First Years</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;V230</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;110 x 30 x 22</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;7.1</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Vải, nhựa, kim loại</p>

            <p>- &nbsp;<strong>K&iacute;ch thước:</strong>&nbsp;88 x 47 x 100 cm (d x r x c)</p>

            <p>- &nbsp;<strong>Trọng lượng cho ph&eacute;p tối đa:</strong>&nbsp;23kg</p>

            <p>- &nbsp;<strong>Chế độ:&nbsp;</strong>ngồi v&agrave; ngả</p>

            <p>-&nbsp;<strong>&nbsp;Nơi sản xuất:&nbsp;</strong>Trung Quốc (ti&ecirc;u chuẩn&nbsp;The First Years)</p>
            ','content' => '','price' => '3587000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:21:16','updated_at' => NULL),
  array('id' => '20','FKCategory' => '12','name' => 'xe đẩy Baohaohao cho bé','slug' => 'xe-day-baohaohao-cho-be','images' => '042210_xe_day_baohaohao_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/baohaohao.html">Baohaohao</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C296</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;110 x 45 x 30</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;9</p>

            <p>- &nbsp;K&iacute;ch thước:&nbsp;L90 x W60 x H110 cm<br />
                - &nbsp;Độ tuổi:&nbsp;cho b&eacute; từ 6 th&aacute;ng đến 3 tuổi.&nbsp;<br />
                - &nbsp;Trọng lượng tối đa đề nghị:&nbsp;20&nbsp;kg.<br />
                -&nbsp;&nbsp;Nơi&nbsp;sản xuất:&nbsp;Trung Quốc</p>
                ','content' => '','price' => '829000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:23:02','updated_at' => NULL),
  array('id' => '21','FKCategory' => '12','name' => 'xe đẩy trẻ em màu đỏ','slug' => 'xe-day-tre-em-mau-do','images' => '042511_xe_day_tre_em_gluck_c_8_mau_do.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/gluck.html">Gluck</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X066</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;43 x 31 x 81</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;7.7</p>

            <p>- &nbsp;<strong>K&iacute;ch thước</strong><strong>:</strong>&nbsp;L89 x W47 x H100 cm<br />
                - &nbsp;<strong>Trọng lượng tối đa đề nghị</strong><strong>:</strong>&nbsp;18 kg.<br />
                -&nbsp;<strong>&nbsp;</strong><strong>Nơi&nbsp;sản xuất</strong><strong>:</strong>&nbsp;Trung Quốc</p>
                ','content' => '','price' => '1560000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:25:51','updated_at' => NULL),
  array('id' => '22','FKCategory' => '12','name' => 'xe đẩy trẻ em 635C','slug' => 'xe-day-tre-em-635c','images' => '042743_xe_day_tre_em_635c.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/baohaohao.html">Baohaohao</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X053</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6.5</p>

            <p>- &nbsp;<strong>K&iacute;ch thước</strong><strong>:</strong>&nbsp;L80&nbsp;x W45&nbsp;x H110&nbsp;cm<br />
                - &nbsp;<strong>Trọng lượng tối đa đề nghị</strong><strong>:</strong>&nbsp;18 kg.<br />
                - &nbsp;<strong>M&agrave;u sắc</strong><strong>:</strong>&nbsp;cam, t&iacute;m<br />
                - &nbsp;<strong>Nơi sản xuất</strong><strong>:</strong>&nbsp;Hongkong<br />
                - &nbsp;<strong>Phương thức giao h&agrave;ng:</strong>&nbsp;Sản phẩm n&agrave;y được giao tận nơi&nbsp;<strong>MIỄN PH&Iacute;</strong>&nbsp;trong nội th&agrave;nh TP.HCM.</p>
                ','content' => '','price' => '870000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:28:29','updated_at' => NULL),
  array('id' => '23','FKCategory' => '12','name' => 'xe đẩy trẻ em Gluck B-60C','slug' => 'xe-day-tre-em-gluck-b-60c','images' => '042927_xe_day_tre_em_gluck_b_60c.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/gluck.html">Gluck</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X064</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;100 x 30 x 30</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;6.3</p>

            <p>- &nbsp;<strong>Độ tuổi</strong><strong>:</strong>&nbsp;cho b&eacute; từ 6 th&aacute;ng đến 3 tuổi.<br />
                - &nbsp;<strong>Nơi&nbsp;sản xuất</strong><strong>:</strong>&nbsp;Trung Quốc&nbsp;<br />
                -&nbsp;<strong>&nbsp;Phương thức giao h&agrave;ng:</strong>&nbsp;Sản phẩm n&agrave;y được giao tận nơi&nbsp;<strong>MIỄN PH&Iacute;</strong>&nbsp;trong nội th&agrave;nh TP.HCM.</p>
                ','content' => '','price' => '1210000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:30:15','updated_at' => NULL),
  array('id' => '24','FKCategory' => '14','name' => 'xe đẩy tập đi cho bé K2 KN sư tử','slug' => 'xe-day-tap-di-cho-be-k2-kn-su-tu','images' => '043132_xe_day_tap_di_cho_be_k2_kn_su_tu_babymartvn.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X115</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;48 x 30 x 34</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;1.8</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước sử dụng (d&agrave;i x rộng x cao, cm):</strong>&nbsp;49 x 44 x 38</p>

            <p><strong>- &nbsp;Chất liệu:</strong>&nbsp;Nhựa<br />
                <strong>- &nbsp;Loại:</strong>&nbsp;Kh&ocirc;ng c&oacute; nhạc</p>

                <p><strong>- &nbsp;Độ tuổi:</strong>&nbsp;từ 12 th&aacute;ng trở l&ecirc;n</p>

                <p><strong>- &nbsp;Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '196000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:32:23','updated_at' => NULL),
  array('id' => '25','FKCategory' => '14','name' => 'xe đẩy tập đi K1 M1463 BB7','slug' => 'xe-day-tap-di-k1-m1463-bb7','images' => '043307_xe_day_tap_di_k1_m1463_bb7.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X058</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;45 x 42 x 42</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;1.6</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Khung bằng kim loại v&agrave; c&aacute;c bộ phận kh&aacute;c bằng nhựa.<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; từ 12&nbsp;th&aacute;ng trở l&ecirc;n<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '174000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:33:51','updated_at' => NULL),
  array('id' => '26','FKCategory' => '14','name' => 'Xe tập đi Autoru cao cấp cho bé','slug' => 'xe-tap-di-autoru-cao-cap-cho-be','images' => '043439_xe_tap_di_autoru_cao_cap.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/autoru.html">Autoru</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X012</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;80 x 60 x 20</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;3.8</p>

            <p>- &nbsp;<strong>Chất liệu khung:</strong>&nbsp;Khung bằng kim loại v&agrave; c&aacute;c bộ phận kh&aacute;c bằng nhựa.&nbsp;<br />
                - &nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; từ&nbsp;6&nbsp;th&aacute;ng trở l&ecirc;n<br />
                - &nbsp;<strong>M&agrave;u:</strong>&nbsp;Xanh dương, Hồng, Xanh l&aacute;<br />
                - &nbsp;<strong>Nhạc:</strong>&nbsp;c&oacute;&nbsp;<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;<br />
                - &nbsp;<strong>Ghi ch&uacute;:</strong>&nbsp;Qu&yacute; kh&aacute;ch vui l&ograve;ng ghi m&agrave;u sắc v&agrave;o &ocirc;&nbsp;<strong>&quot;th&ocirc;ng tin th&ecirc;m&quot;</strong>&nbsp;trong qu&aacute; tr&igrave;nh đặt h&agrave;ng.</p>
                ','content' => '','price' => '370000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:35:28','updated_at' => NULL),
  array('id' => '27','FKCategory' => '14','name' => 'xe tập đi bằng gỗ Song Son cho bé','slug' => 'xe-tap-di-bang-go-song-son-cho-be','images' => '043609_xe_tap_di_bang_go_song_son_cho_be_new.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/song-son.html">Song Son</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;V536</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;48 x 30 x 15</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;2.1</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Gỗ&nbsp;tự nhi&ecirc;n<br />
                - &nbsp;<strong>K&iacute;ch thước:</strong>&nbsp;38 x 30 x 45 cm<br />
                - &nbsp;<strong>Độ tuổi sử dụng:</strong>&nbsp;tr&ecirc;n 1 tuổi.<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '144000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:36:56','updated_at' => NULL),
  array('id' => '28','FKCategory' => '14','name' => 'xe tập đi bằng gỗ Winwintoys','slug' => 'xe-tap-di-bang-go-winwintoys','images' => '043737_xe_tap_di_bang_go_duc_thanh_cho_be11.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/duc-thanh.html">Winwintoys</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;X062</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;30 x 17 x 39</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;2.5</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Gỗ - cao su<br />
                - &nbsp;<strong>K&iacute;ch thước sử dụng (d&agrave;i x rộng x cao, cm):</strong>&nbsp;38 x 29 x 44<br />
                - &nbsp;<strong>Độ tuổi sử dụng:</strong>&nbsp;tr&ecirc;n 1 tuổi.<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '340000','active' => '1','orderBy' => '0','parentID' => '6','created_at' => '2016-10-06 04:38:41','updated_at' => NULL),
  array('id' => '29','FKCategory' => '21','name' => 'Bập bênh ngựa gỗ','slug' => 'bap-benh-ngua-go','images' => '043949_bap_benh_ngua_go_xanh_duong.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/vietoys.html">Vietoys</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C026</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;90 x 28 x 65</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;7.9</p>

            <p>- &nbsp;<strong>Chất liệu:&nbsp;</strong>Gỗ.</p>

            <p>- &nbsp;<strong>K&iacute;ch thước:&nbsp;</strong>90 x 28 x 65 cm.</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;2 tuổi trở l&ecirc;n.</p>

            <p>- &nbsp;<strong>Nơi sản xuất:&nbsp;</strong>Việt Nam.<br />
                - &nbsp;<strong>Ghi ch&uacute;:&nbsp;</strong>Qu&yacute; kh&aacute;ch vui l&ograve;ng ghi m&agrave;u sắc v&agrave;o &ocirc;&nbsp;&quot;th&ocirc;ng tin th&ecirc;m&quot;&nbsp;trong qu&aacute; tr&igrave;nh đặt h&agrave;ng.</p>
                ','content' => '','price' => '675000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:40:35','updated_at' => NULL),
  array('id' => '30','FKCategory' => '21','name' => 'Bồn tắm bơm hơi Disney Pixar Cars cho bé','slug' => 'bon-tam-bom-hoi-disney-pixar-cars-cho-be','images' => '044113_bon_tam_bom_hoi_disney_pixar_cars_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/disney.html">Disney</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;V284</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước:</strong>&nbsp;77,4 x 52 x 17,8 cm</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;1 - 2 tuổi&nbsp;<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Trung Quốc&nbsp;</p>
                ','content' => '','price' => '420000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:41:58','updated_at' => NULL),
  array('id' => '31','FKCategory' => '21','name' => 'Đồ chơi gỗ - Đánh golf 4 thú','slug' => 'do-choi-go-danh-golf-4-thu','images' => '044327_do_choi_go_danh_golf_4_thu.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/duc-thanh.html">Winwintoys</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C277</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;54 x 14 x 6</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.35</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Gỗ</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 4&nbsp;tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '136000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:44:15','updated_at' => NULL),
  array('id' => '32','FKCategory' => '21','name' => 'Đồ chơi gỗ - Trò chơi Bowling','slug' => 'do-choi-go-tro-choi-bowling','images' => '044511_do_choi_go_tro_choi_bowling.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/duc-thanh.html">Winwintoys</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C365</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;13 x 8 x 18</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.25</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Gỗ</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n&nbsp;3&nbsp;tuổi</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '102000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:46:33','updated_at' => NULL),
  array('id' => '33','FKCategory' => '21','name' => 'Đồ chơi gỗ vịt con lật đật Winwintoys','slug' => 'do-choi-go-vit-con-lat-dat-winwintoys','images' => '044713_do_choi_go_vit_con_lat_dat_winwintoys.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/duc-thanh.html">Winwintoys</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C1400</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;5.8 x 5.8 x 7.5</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.1</p>

            <p><strong>- &nbsp;Chất liệu:</strong>&nbsp;Gỗ cao su</p>

            <p><strong>- &nbsp;Độ tuổi:</strong>&nbsp;D&agrave;nh cho trẻ tr&ecirc;n 2 tuổi</p>

            <p><strong>- &nbsp;Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '81000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:48:02','updated_at' => NULL),
  array('id' => '34','FKCategory' => '19','name' => 'Bộ câu sinh vật biển Winwintoys','slug' => 'bo-cau-sinh-vat-bien-winwintoys','images' => '044847_bo_cau_sv_bien.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/duc-thanh.html">Winwintoys</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C586</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;20 x 7 x 32</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.5</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Gỗ</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;3 tuổi trở l&ecirc;n</p>

            <p>- &nbsp;<strong>Bộ sản phẩm bao gồm:</strong>&nbsp;14 chi tiết rời</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '173000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:49:37','updated_at' => NULL),
  array('id' => '35','FKCategory' => '19','name' => 'Bộ đồ chơi 3 con vịt','slug' => 'bo-do-choi-3-con-vit','images' => '045049_bo_do_choi_3_con_vit.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/papa.html">PAPA</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C1478</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;16 x 11 x 18</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.1</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa an to&agrave;n, kh&ocirc;ng chứa BPA</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;0 tuổi trở l&ecirc;n.</p>

            <p>- &nbsp;<strong>Nơi sản xuất:&nbsp;</strong>Th&aacute;i Lan</p>
            ','content' => '','price' => '103000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:51:36','updated_at' => NULL),
  array('id' => '36','FKCategory' => '19','name' => 'Bộ đồ chơi dụng cụ bác sĩ cho bé','slug' => 'bo-do-choi-dung-cu-bac-si-cho-be','images' => '045225_bo_do_choi_dung_cu_bac_si_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/tam-anh.html">Tam Anh</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C276</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;26 x 7 x 17</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.2</p>

            <p>-<strong>&nbsp;Chất liệu:</strong>&nbsp;Nhựa</p>

            <p>-&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; 3 tuổi trở l&ecirc;n</p>

            <p>-&nbsp;<strong>Bộ sản phẩm gồm:</strong>&nbsp;Tai nghe, đo huyết &aacute;p, lọ thuốc, th&igrave;a,...</p>

            <p>-&nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam&nbsp;</p>
            ','content' => '','price' => '53000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:53:24','updated_at' => '2016-10-06 04:53:24'),
  array('id' => '37','FKCategory' => '19','name' => 'Bộ dụng cụ chăm sóc cây','slug' => 'bo-dung-cu-cham-soc-cay','images' => '045436_bo_dung_cu_cham_soc_cay.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/tam-anh.html">Tam Anh</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C246</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;14 x 17 x 26</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.4</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa.</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; từ 2 tuổi trở l&ecirc;n.</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam.</p>
            ','content' => '','price' => '0','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:55:08','updated_at' => NULL),
  array('id' => '38','FKCategory' => '19','name' => 'Bộ dụng cụ cơ khí Tamir Seti cho bé','slug' => 'bo-dung-cu-co-khi-tamir-seti-cho-be','images' => '045807_bo_dung_cu_co_khi_tamir_seti_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/furkan.html">Furkan</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C231</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;55 x 13 x 44.5</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;1.9</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi</p>

            <p>- &nbsp;<strong>Số lượng:</strong>&nbsp;40&nbsp;chi tiết</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Thổ Nhĩ Kỳ</p>
            ','content' => '','price' => '599000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 04:58:45','updated_at' => NULL),
  array('id' => '39','FKCategory' => '19','name' => 'bộ dụng cụ nấu ăn cho bé','slug' => 'bo-dung-cu-nau-an-cho-be','images' => '045923_bo_dung_cu_nau_an_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C1481</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;33 x 27 x 21</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;1.1</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa cao cấp<br />
                -&nbsp;<strong>&nbsp;Độ tuổi:&nbsp;</strong>cho b&eacute; tr&ecirc;n 3 tuổi<br />
                - &nbsp;<strong>Số lượng:&nbsp;</strong>42 chi tiết<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '167000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:00:13','updated_at' => NULL),
  array('id' => '40','FKCategory' => '20','name' => 'Bộ lắp ghép quả bóng Tombul cho bé','slug' => 'bo-lap-ghep-qua-bong-tombul-cho-be','images' => '050125_bo_lap_ghep_qua_bong_tombul_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/ucar.html">Ucaroyuncak</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C438</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;15 x 15 x 15</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.3</p>

            <p>-&nbsp;<strong>&nbsp;Th&agrave;nh phần:&nbsp;</strong>nhựa an to&agrave;n</p>

            <p>- &nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; từ&nbsp;1 - 3 tuổi</p>

            <p>-&nbsp;<strong>&nbsp;Số lượng:</strong>&nbsp;24 c&aacute;i</p>

            <p>- &nbsp;<strong>Nơi sản xuất:&nbsp;</strong>Thổ Nhĩ Kỳ</p>
            ','content' => '','price' => '96000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:02:18','updated_at' => NULL),
  array('id' => '41','FKCategory' => '20','name' => 'Bộ xe container và 12 xe hơi nhỏ cho bé','slug' => 'bo-xe-container-va-12-xe-hoi-nho-cho-be','images' => '050304_bo_xe_container_va_12_xe_hoi_nho_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C170</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;49 x 16 x 19</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;1.1</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa&nbsp;cao cấp<br />
                - &nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '202000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:04:33','updated_at' => NULL),
  array('id' => '42','FKCategory' => '20','name' => 'bộ xếp hình sáng tạo 224','slug' => 'bo-xep-hinh-sang-tao-224','images' => '050508_224.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C1128</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;36 x 18 x 22</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.8</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa cao cấp</p>

            <p>-&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi</p>

            <p>- &nbsp;<strong>Số lượng:</strong>&nbsp;53 chi tiết xếp được 5 h&igrave;nh</p>

            <p>- &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
            ','content' => '','price' => '119000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:05:48','updated_at' => NULL),
  array('id' => '43','FKCategory' => '20','name' => 'bộ xếp hình sáng tạo 225','slug' => 'bo-xep-hinh-sang-tao-225','images' => '050644_untitled_1.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C129</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;35 x 16 x 18</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.7</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa cao cấp<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi<br />
                - &nbsp;<strong>Số lượng:</strong>&nbsp;82&nbsp;chi tiết cho hơn 7 m&ocirc;&nbsp;h&igrave;nh kh&aacute;c nhau<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '119000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:07:49','updated_at' => NULL),
  array('id' => '44','FKCategory' => '20','name' => 'bộ xếp hình sáng tạo 226','slug' => 'bo-xep-hinh-sang-tao-226','images' => '050841_xep_hinh_226.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C184</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;39 x 20 x 18</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.6</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa cao cấp<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi<br />
                - &nbsp;<strong>Số lượng:</strong>&nbsp;71&nbsp;chi tiết cho 7 m&ocirc;&nbsp;h&igrave;nh kh&aacute;c nhau<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '119000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:09:18','updated_at' => NULL),
  array('id' => '45','FKCategory' => '20','name' => 'bộ xếp hình sáng tạo 227','slug' => 'bo-xep-hinh-sang-tao-227','images' => '050959_227.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C230</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;29 x 19 x 15</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.6</p>

            <p>- &nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa cao cấp<br />
                -&nbsp;&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi<br />
                - &nbsp;<strong>Số lượng:</strong>&nbsp;78 chi tiết cho hơn 7 m&ocirc;&nbsp;h&igrave;nh kh&aacute;c nhau<br />
                - &nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '112000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:10:30','updated_at' => NULL),
  array('id' => '46','FKCategory' => '23','name' => 'Bảng học từ tiếng Anh cho bé','slug' => 'bang-hoc-tu-tieng-anh-cho-be','images' => '051116_bang_hoc_tu_tieng_anh_cho_be.jpg','thumbnail' => '','type' => '0','description' => '<p>-&nbsp;&nbsp;&nbsp;<a href="http://babymart.vn/thuong-hieu/nhua-cho-lon.html">Nhựa Chợ Lớn</a>&nbsp;|&nbsp;<strong>M&atilde; SP:</strong>&nbsp;C294</p>

            <p>-&nbsp;&nbsp;<strong>K&iacute;ch thước bao b&igrave; (d&agrave;i x rộng x cao, cm):</strong>&nbsp;57 x 5 x 45</p>

            <p>-&nbsp;&nbsp;<strong>Khối lượng bao b&igrave; (kg):</strong>&nbsp;0.55</p>

            <p>-&nbsp;<strong>Chất liệu:</strong>&nbsp;Nhựa&nbsp;cao cấp<br />
                -&nbsp;<strong>Độ tuổi:</strong>&nbsp;cho b&eacute; tr&ecirc;n 3 tuổi<br />
                -&nbsp;<strong>Nơi sản xuất:</strong>&nbsp;Việt Nam</p>
                ','content' => '','price' => '100000','active' => '1','orderBy' => '0','parentID' => '9','created_at' => '2016-10-06 05:12:09','updated_at' => NULL),
  array('id' => '47','FKCategory' => '35','name' => 'Đồ chơi lắp ráp','slug' => 'do-choi-lap-rap','images' => '072202_bo_lap_rap_sang_tao_winwintoys_64302_xe.jpg','thumbnail' => '','type' => 'PT02','description' => '<p>Bộ lắp gh&eacute;p s&aacute;ng tạo gồm rất nhiều miếng gh&eacute;p nhựa cao cấp (miếng gh&eacute;p size to từ 3-8cm) B&eacute; c&oacute; thể s&aacute;ng tạo lắp gh&eacute;p th&agrave;nh c&aacute;c con vật ngộ nghĩnh Bộ đồ chơi gi&uacute;p b&eacute; ph&aacute;t triển khả năng vận động, tư duy; kỹ năng phối hợp tay v&agrave; mắt; k&iacute;ch th&iacute;ch sự tập trung s&aacute;ng tạo của n&atilde;o bộ H&agrave;ng tặng từ Abbott. K&iacute;ch thước hộp 23*37cm</p>
','content' => '','price' => '120000','active' => '1','orderBy' => '0','parentID' => '2','created_at' => '2016-10-19 07:28:02','updated_at' => NULL),
  array('id' => '48','FKCategory' => '24','name' => 'Bộ đồ chơi Domino gỗ thông minh cho bé từ 03 tuổi trở lên','slug' => 'bo-do-choi-domino-go-thong-minh-cho-be-tu-03-tuoi-tro-len','images' => '074117_0635042131711492386.jpg','thumbnail' => '','type' => '0','description' => '<ul>
    <li><em><strong>Bộ đồ chơi Domino gỗ</strong></em>&nbsp;th&ocirc;ng minh -&nbsp;Sản phẩm đồ chơi tr&iacute; tuệ gi&uacute;p ph&aacute;t triển khả năng s&aacute;ng tạo, tăng tư duy cho b&eacute;.</li>
    <li>Chất liệu: Sản phẩm được l&agrave;m bằng&nbsp;<strong>chất liệu gỗ</strong>&nbsp;th&ocirc;ng sấy an to&agrave;n kh&ocirc;ng c&oacute; chất độc hại</li>
    <li>
    <h2>M&agrave;u sắc: m&agrave;u sắc tươi s&aacute;ng, nhiều h&igrave;nh minh họa.</h2>
    </li>
    <li>Chơi Domino thường xuy&ecirc;n gi&uacute;p b&eacute; dễ d&agrave;ng&nbsp;<em><strong>nhận biết mặt chữ, c&aacute;c con vật</strong></em>, tăng khả năng&nbsp;<em><strong>tưởng tượng, ph&aacute;t triển n&atilde;o bộ</strong></em>&nbsp;cho trẻ trong những năm đầu đời.</li>
    <li>K&iacute;ch thước hộp:&nbsp;27 x 16cm.</li>
</ul>
','content' => '','price' => '165000','active' => '1','orderBy' => '0','parentID' => '1','created_at' => '2016-10-19 07:44:39','updated_at' => NULL)
);
       
            \Illuminate\Support\Facades\DB::table('products')->insert($products);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('products');
    }

}
