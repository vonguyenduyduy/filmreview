<script>
    $(function () {
        $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [[2, 25, 50, -1]]
        });
    });
    Vue.component('modal', {
        template: '#modal-template',
        props: {
            data: Array,
            columns: Array,
            filterKey: String,
            test: "abc"
        }
    });

    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            rows: JSON.parse('{!! $dataJson !!}'),
            editRow: [],
            editId: 0,
            showModal: false,
            add : false,
            selected: [],
            title: '',
            parent: '{{!empty($cat) ? $cat->id : 0}}'
        },
        methods:{
            addNew : function () {
                this.add = true;
                this.title = "Thêm mới";
                this.showModal = true;
                $('.bs-example-modal-lg').modal('show');

                this.resetForm();

            },
            saveForm: function () {
                var self = this;
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        self.parseData(data.result);
                        $('.bs-example-modal-lg').modal('hide');
                        if(!self.add){
                            $(".toggle-id-"+self.rows[self.editId].id)
                                    .prop("disabled", false)
                                    .prop("checked", self.rows[self.editId].active)
                                    .trigger("change")
                                    .prop("disabled", true)
                                    .trigger("change");
                        }

                    }else{
                           console.log(data.message);
                        $.notify({
                            // options
                            message: data.message.name
                        },{
                            // settings
                            type: 'danger',
                            z_index: 99999
                        });
                    }
                });
                return ;
            },
            deleteItem: function (id) {
                var self = this;
                this.ajaxRequest("POST", '{{$routeDelete}}', {id: id, parent: this.parent}, function (data){
                    if(data.success){
                        self.parseData(data.result);
                    }else{
                        alert(data.message);
                    }
                });
            },
            getUpdateItem: function (id, index) {
                this.showModal = true;
                this.add = false;
                this.editId = index;
                this.title = "Chỉnh sửa - " + this.rows[index].name;
                var self = this;
                $('.bs-example-modal-lg').modal('show');

                this.resetForm();
                this.ajaxRequest("GET", '{{$routeGetEdit}}', {id: id}, function (data){
                    if(data.success){
                        self.editRow = data.result;
                        $("#formSubmit .select-banner").val(data.result.banner.split(',')).trigger("change");
                        $("#formSubmit .toggle-check").prop("checked", data.result.active).trigger("change")

                    }else{
                        alert(data.message);
                    }
                });

            },
            parseData: function (data) {

                this.rows =  JSON.parse(data);

            },
            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success

                });
            },
            editSelected: function () {

                if(this.selected.length > 1){
                    alert("Pls select only 1 item to edit");
                }else if(this.selected.length == 1){
                    this.getUpdateItem(this.selected[0], $(".checklist[value='"+this.selected[0]+"']").attr('data-item'));
                }else{
                    alert("Pls select 1 item to edit");
                }
                return;

            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    confirm("Delete :" );
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, parent: this.parent}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                        }else{
                            alert(data.message);
                        }
                    });
                }else{

                }

            },
            resetForm: function () {
                $("#formSubmit .select-banner").find('option:selected').removeAttr('selected').trigger("change");
                $("#formSubmit")[0].reset();
                $("#holder").attr("src", "");
            }
           

        }

    });
</script>