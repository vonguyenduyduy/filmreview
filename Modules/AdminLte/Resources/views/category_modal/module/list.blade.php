<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">{{$title}}</a></li>
        <li class="active">Danh sách</li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="addNew">
                            <i class="fa fa-plus-square-o"></i> Thêm
                        </a>
                        <a class="btn btn-app" @click="editSelected">
                            <i class="fa fa-edit"></i> Sửa
                        </a>
                        <a class="btn btn-app" @click="deleteSelected">
                            <i class="fa fa-remove"></i> Xóa
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- use the modal component, pass in the prop -->
                    <modal v-if="showModal" @close="showModal = false" :data="editRow" :columns="rows"></modal>

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Ảnh đại diện</th>
                                <th>Tên chuyên mục</th>
                                <th>Trạng thái</th>
                                <th>Thứ tự</th>
                                <th>Banner</th>
                                <th>Ngày tạo</th>
                                <th>Action</th>

                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(v,index) in rows">
                                <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                                <td>@{{ v.id}}</td>
                                <td><img v-bind:src="v.images" height="80"></td>
                                <td><a v-bind:href="'/lte/category/child/'+v.id">@{{v.name}}</a></td>
                                <td>
                                    <div class="form-group">
                                        <input name="active" v-if="v.active == 1" v-bind:class="'toggle-one toggle-id-'+v.id" checked type="checkbox" disabled>
                                        <input name="active"  v-else v-bind:class="'toggle-one toggle-id-'+v.id" type="checkbox" disabled>
                                    </div>
                                </td>
                                <td> @{{v.orderBy}}</td>
                                <td>@{{v.banner}}</td>

                                <td> @{{v.created_at}}</td>
                                <td>
                                    {{--<button type="button" class="btn btn-info btn-xs"><i class="fa fa-list-alt"></i></button>--}}
                                    <button type="button" class="btn btn-info btn-sm" @click="getUpdateItem(v.id, index)"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Add modal -->
    @include('adminlte::category.module.modal')
</section>




