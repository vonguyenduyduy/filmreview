<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                        <button type="button" class="btn btn-default pull-right btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> {{trans('backend.cancel')}}</button>
                        <button type="button" class="btn btn-success pull-right btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}
                            {!! FormHelpers::inputHidden("status", 'editRow.status') !!}

                            {!! FormHelpers::input(trans('backend.name'), "name", 'editRow.name') !!}
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{trans('backend.status')}}</label>
                                <div class="col-sm-10">
                                    <button  v-if="editRow.status == 1"  type="button" class="btn btn-success btn-sm" @click="editRow.status = 0">Active</button>
                                    <button v-else type="button" class="btn btn-default btn-sm" @click="editRow.status = 1">Inactive</button>
                                </div>
                            </div>
                            {!! FormHelpers::input(trans('backend.order'), "order", 'editRow.order', 2, 'number') !!}
                            {!! FormHelpers::image(trans('backend.images'), "images", 'editRow.images') !!}

                            {!! FormHelpers::textArea(trans('backend.description'), "description", 'editRow.description') !!}
                            {!! FormHelpers::textArea(trans('backend.content'), "content", 'editRow.content') !!}

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> {{trans('backend.cancel')}}</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
