<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div v-bind:class="{'col-sm-6' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <div class="col-sm-3">
                            <a class="btn btn-app" href="{{route('backend.post.add', 0)}}?cat={{!empty($catId) ? $catId : 0}}"><i class="fa fa-plus-square-o"></i>
                                <span>{{trans('backend.add')}}</span>
                            </a>
                            <a class="btn btn-app" @click="deleteSelected">
                            <i class="fa fa-remove"></i>
                            <span>{{trans('backend.delete_multi')}}</span>
                            </a>
                            <a class="btn btn-app btn-danger" @click="actionForm(false)">
                            <i class="fa fa-undo"></i>
                            <span>{{trans('backend.cancel')}}</span>
                            </a>
                        </div>

                        <div class="col-sm-6" style="border: 1px solid rgb(221, 221, 221); padding: 10px">
                            <div class="col-sm-12" style="margin-bottom: 10px">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Type of Post</label>
                                    <div class="col-sm-5">
                                        <select id="typeOfPost" class="form-control select2 " style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            <option value="">All</option>
                                            @foreach(config('constants.POST_SELECT_BOX') as $k => $v)
                                                <option value="{{$k}}" @if($k == $type) selected @endif> {{$v}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <button class="btn btn-primary" @click="filter">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="margin-bottom: 10px">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Attribute</label>
                                    <div class="col-sm-5">
                                        <select id="attribute" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            <option value="">All</option>
                                            @foreach($attributeType as $k => $v)
                                                <option value="{{$v->paramID}}" @if($v->paramID == $attr) selected @endif> {{$v->value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="margin-bottom: 10px">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" id="postName" class="form-control" value="{{$postName}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                @if($type == 1)
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Show on home page</label>
                                        <div class="col-sm-5">
                                            <input type="checkbox" id="checkHomePage" @if($onHome == 1) checked @endif>
                                        </div>
                                    </div>
                                @endif
                                @if($type == 2)
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Post Categories</label>
                                        <div class="col-sm-5">
                                            <select id="postCategories" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                <option value="">All</option>
                                                @foreach($postCategories as $k => $v)
                                                    <option value="{{$v->id}}" @if($v->id == $typeCat) selected @endif> {{$v->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="pull-right">
                            <span v-if="lang == 'en'">
                                <a class="btn btn-primary" @click="changeLang('en')">
                                    <span>{{trans('backend.language_en')}}</span>
                                </a>
                                <a class="btn btn-default" @click="changeLang('vi')">
                                    <span>{{trans('backend.language_vi')}}</span>
                                </a>
                            </span>
                                <span v-else>
                                <a class="btn btn-default" @click="changeLang('en')">
                                    <span>{{trans('backend.language_en')}}</span>
                                    </a>
                                    <a class="btn btn-primary" @click="changeLang('vi')">
                                    <span>{{trans('backend.language_vi')}}</span>
                                    </a>
                            </span>

                            </div>
                        </div>



                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('backend.action')}}</th>
                                <th>{{trans('backend.name')}}</th>
                                <th>{{trans('backend.images')}}</th>
                                <th>{{trans('backend.status')}}</th>

                                <th>{{trans('backend.order')}}</th>
                                <th>{{trans('backend.created_at')}}</th>
                                <th>{{trans('backend.updated_at')}}</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(v,index) in rows">
                                <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                                <td>
                                    {{--<button type="button" class="btn btn-info btn-sm" @click="getUpdateItem(v.id, index, $event.target)"><i class="fa fa-edit"></i></button>--}}
                                    <a class="btn btn-info btn-sm" v-bind:href="'/admin/post/add/'+ v.id +'?cat='+ v.page_type"><i class="fa fa-edit"></i></a>

                                    <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                                </td>
                                <td><a title="Link to detail page" v-bind:href="'/admin/post/add/' + v.id + '?cat='+ v.page_type">@{{v.name}}</a></td>
                                <td><img v-bind:src="renderImage(v.images)" height="60" width="80"></td>

                                <td>
                                    <div class="form-group">
                                        <button  v-if="v.status == 1"  type="button" class="btn btn-success btn-sm disabled" >Active</button>
                                        <button v-else type="button" class="btn btn-default btn-sm disabled" >Inactive</button>
                                    </div>
                                </td>

                                <td> @{{v.order}}</td>
                                <td> @{{v.created_at}}</td>
                                <td> @{{v.updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            @include('adminlte::post.module.add')
        </div>
    </div>

</section>
@section('footer_script')
    @include('adminlte::post.module.script')
@endsection



