<script>

    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            editRow: {},
            editId: '',
            selected: [],
            title: '',
            btnSave : '',
            slug : 'abc',
            status: '{{!empty($detail->status)? $detail->status : 0}}',
            is_home: '{{!empty($detail->is_home)? $detail->is_home : 0}}',
            pageType: null,
            postType : '',
            catId : '{{!empty($catId) ? $catId : ''}}'

        },
        computed:{

        },
        created:function () {
            this.init();
        },

        methods:{
            init: function () {
                @if($create)
                   this.status = 1;
                @endif
            },
            saveForm: function () {
                var self = this;
                if($('select[name="page_type"]').val() == null){
                    self.notify('{{trans('backend.message.page_type_required')}}','danger');
                    return;
                }
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        if(self.catId != ''){
                            location.href = '/admin/post/get-by-page/'+ self.catId;
                        }else{
                            location.href = '{{$routeIndex}}';
                        }
                    }else{
                        self.notify(data.message,'danger');

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },

            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success
                });
            },

            pageTypeChange: function (pageType) {
                var self = this;

                this.ajaxRequest("GET", '{{route('backend.post.get_post_by_page_type')}}', {page_type: pageType}, function (data){
                    if(data.success){
                        self.pageType = data.result;
                        $('select[name="type"]').val(null).change();
                    }
                });
            },

            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            }
        }

    });
    function pageTypeChange(e) {
        tableContent.pageTypeChange($(e).val());
    }
</script>