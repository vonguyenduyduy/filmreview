<script>

    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            dataJson: {},
            editRow: {},
            editId: '',
            showModal: false,
            selected: [],
            title: '',
            catId: '{{ !empty($catId)? $catId : '' }}',
            btnSave : '',
            action: false,
            detail: false,
            slug : 'abc',
            lang: 'en',
            type: '{{$type}}',
            postCategories: $("#postCategories").val()

        },
        computed:{
          rows: function () {
              return this.dataJson.rows;
          }
        },
        created:function () {

            this.init(this.lang);
            var self = this;
            setTimeout(function(){
                $('#dataTable').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "lengthMenu": [[10, 25, 50, -1],[10, 25, 50, "All"]]

                });
                if(self.detail){
                    self.editRow = self.filterBySlug(self.slug);
                }

            }, 0);

        },

        methods:{
            init: function (lang) {
                var self = this;
//                var typeOfPost = $("#typeOfPost").val();
//                var postCategories = $("#postCategories").val();
                this.ajaxRequest("GET", '{{$routeInit}}', {
                    cat_id: this.catId,
                    lang_code: lang,
                    type : this.type,
                    typeCat : this.postCategories,
                    onHome:  $("#checkHomePage").is(":checked")? 1 : 0,
                    attr : $("#attribute").val(),
                    postName: $("#postName").val()
                }, function (data){
                    self.dataJson =  JSON.parse(data);
                    self.lang = lang;
                });
            },
            changeLang: function (lang) {
                this.init(lang)
            },
            addNew : function () {
                this.actionForm(true);
                this.title = "Thêm mới";
                this.btnSave = "Lưu";

            },
            saveForm: function () {
                var self = this;
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        self.parseData(data.result);
                        if(!self.detail){
                            self.actionForm(false);
                        }
                        self.notify('Cập nhật thành công !', 'success');
                        $('html, body').animate({scrollTop: '0px'}, 300);

                    }else{
                        self.notify(data.message,'danger');

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },

            getUpdateItem: function (id, index, e) {
                $("#dataTable tr").removeClass('selected');
                $(e).parents('tr').addClass('selected');
                this.editId = index;
                this.title = "Chỉnh sửa ";
                this.btnSave = "Cập nhật";
                this.actionForm(true);
                this.bindDataEdit(index);

            },
            bindDataEdit: function (index) {
                $("#formSubmit .type-display").find('option:selected').removeAttr('selected').trigger("change");
                this.editRow = this.rows[index];
                CKEDITOR.instances['description'].setData(this.editRow.description);

            },
            parseData: function (data) {

                this.dataJson =  JSON.parse(data);
                $('.toggle-one').bootstrapToggle();

            },
            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success
                });
            },
            editSelected: function () {

                if(this.selected.length > 1){
                    this.notify('Pls select only 1 item to edit', 'warning');

                }else if(this.selected.length == 1){
                    this.getUpdateItem(this.selected[0], $(".checklist[value='"+this.selected[0]+"']").attr('data-item'));
                }else{
                    this.notify('Pls select 1 item to edit', 'warning');
                }

            },
            deleteItem: function (id) {
                var self = this;
                this.ajaxRequest("POST", '{{$routeDelete}}', {id: id, catId: this.catId}, function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.notify('Xóa thành công 1 chuyên mục !', 'success');
                    }else{
                        self.notify(data.message,'danger');
                    }
                });
            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, catId: this.catId}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Xóa thành công '+self.selected.length+' item !', 'success');

                        }else{
                            self.notify(data.message,'danger');
                        }
                    });
                }else{

                }

            },
            resetForm: function () {
                this.initRowEdit();
                if($("#formSubmit").length > 0){
                    $("#formSubmit")[0].reset();
                    $("#holder").attr("src", "");
                }
            },
            actionForm: function (type) {
                this.resetForm();
                this.action = type;
                if(type){
                    $("#blockForm").show(200);

                }else{
                    $("#blockForm").hide(200);
                    $("#dataTable tr").removeClass('selected');

                }
            },
            initRowEdit: function () {
                this.editRow = {
                "id" : 0,
                "status": 1,
                "name" : "",
                "order" : 1,
                "images" :"",
                "description" : "",
                "content" : ""
                };
            },
            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            },
            filterBySlug: function (slug) {
                var ret = [];
                this.rows.map(function (value, index) {
                    if(value.slug == slug){
                        ret =  value;
                        return false;
                    }
                }, []);
                return ret;
            },
            renderImage: function (link) {
                if(link == '' || !link){
                    return '{{renderImage('')}}'

                }
                return link;
            },
            filter: function () {
//                this.init(this.lang);return;
                var typeOfPost = $("#typeOfPost").val();
                this.postCategories = $("#postCategories").val();
                var checkHomePage = $("#checkHomePage").is(":checked");
                var attribute = $("#attribute").val();
                var postName = $("#postName").val();

                var url = '?type='+ typeOfPost + '&attr=' + attribute;

                if(typeOfPost == 2){
                    url += '&typeCat=' + this.postCategories;
                }
                if(typeOfPost == 1){
                    var check = checkHomePage? 1 : 0;
                    url += '&onHome=' + check;
                }
                if(postName != ''){
                    url += '&postName=' + postName;
                }
                location.href = url;
            }

        }

    });
</script>