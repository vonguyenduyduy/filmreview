<div class="row" id="blockForm" style="display: none;">
    <style>
        .area-galleries .gitem-item .g-no {
            display: inline-block;
            padding: 5px;
            color: #fff;
            background: #333;
            width: 20px;
            text-align: center;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
        }
    </style>
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                        <button type="button" class="btn btn-default pull-right btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-right btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                    <div class="box-body">

                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}
                            {!! FormHelpers::inputHidden("parentID", 'editRow.parentID') !!}
                            {!! FormHelpers::inputHidden("thumbnail", 'editRow.thumbnail') !!}
                            {!! FormHelpers::inputHidden("active", 'editRow.active') !!}

                            {!! FormHelpers::input("Tên", "name", 'editRow.name') !!}
{{--                            {!! FormHelpers::input("Tên (EN)", "name_en", 'editRow.name_en') !!}--}}

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Danh mục</label>
                                <div class="col-sm-10">
                                    <select name="FKCategory" class="form-control select2 " style="width: 100%;">
                                        @foreach($categories as $v)
                                            @if($v->id == $cat->id)
                                                <option value="{{$v->id}}" selected>{{$v->name}}</option>
                                            @else
                                                <option value="{{$v->id}}">{{$v->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Loại hiển thị</label>
                                <div class="col-sm-10">
                                    <select name="type[]" class="form-control select2 type-display" style="width: 100%;" tabindex="-1" aria-hidden="true" multiple>
                                        {{--<option value="" selected>-- Choose one --</option>--}}
                                        @foreach($listProductType as $v)
                                            <option value="{{$v->paramID}}">{{$v->value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if(empty($deleted))
                            {{--<div class="form-group">--}}
                                {{--<label class="col-sm-2 control-label">Danh mục hiển thị</label>--}}
                                {{--<div class="col-sm-10">--}}
                                    {{--<select name="others[]" class="form-control select2 category-display" style="width: 100%;" tabindex="-1" aria-hidden="true" multiple>--}}
                                        {{--@foreach($categories as $v)--}}
                                            {{--<option value="{{$v2->id}}">{{$v2->name}}</option>--}}
                                            {{--<optgroup label="{{$v->name}}">--}}
                                                {{--@foreach($v->getCatLevel2 as $v2)--}}
                                                    {{--@if($v2->id != $cat->id)--}}
                                                    {{--<option value="{{$v2->id}}">{{$v2->name}}</option>--}}
                                                    {{--@endif--}}
                                                {{--@endforeach--}}
                                            {{--</optgroup>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            @endif
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Trạng thái</label>
                                <div class="col-sm-10">
                                    <button  v-if="editRow.active == 1"  type="button" class="btn btn-success btn-sm" @click="editRow.active = 0">Active</button>
                                    <button v-else type="button" class="btn btn-default btn-sm" @click="editRow.active = 1">Inactive</button>
                                </div>
                            </div>
                            {!! FormHelpers::inputCurrency("Giá", "price", 'numberToString(editRow.price)') !!}
                            {!! FormHelpers::input("Khuyến mãi", "sales", 'editRow.sales') !!}
                            {!! FormHelpers::input("Thứ tự", "orderBy", 'editRow.orderBy') !!}
                            {!! FormHelpers::input("Tags", "tags", 'editRow.tags') !!}
                            {!! FormHelpers::image("Hình ảnh", "images", 'editRow.images') !!}

                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    <span @click="addImage" class="btn btn-primary btn-sm"><i class="fa fa-plus-square-o"></i></span> ảnh
                                </label>
                                <div class="col-sm-10 file-upload g-file-upload">
                                    <div v-if="editRow.thumbnail != ''" id="frame-image">
                                        <div v-for="(v,index) in stringParseJson(editRow.thumbnail)" class="one-item-image">
                                            <div v-if="v != ''">
                                                <input style="width: 60%" class="mt-default" v-bind:id="'id_galleries_'+index" name="galleries[]" type="text" v-bind:value="v">
                                                <a class="lfm btn btn-default" v-bind:data-input="'id_galleries_'+index" v-bind:data-preview="'galleries_holder_'+index">Choose</a>
                                                <img v-bind:id="'galleries_holder_'+index" v-bind:src="v" width="50">
                                                <span class="btn btn-danger btn-sm" @click="deleteImage($event)"><i class="fa fa-remove"></i></span>
                                            </div>

                                        </div>
                                    </div>
                                    <div v-else id="frame-image">
                                        <div v-for="index in 5" class="one-item-image">
                                            <input style="width: 60%" class="mt-default" v-bind:id="'id_galleries_'+index" name="galleries[]" type="text">
                                            <a class="lfm btn btn-default" v-bind:data-input="'id_galleries_'+index" v-bind:data-preview="'galleries_holder_'+index">Choose</a>
                                            <img v-bind:id="'galleries_holder_'+index" src="" width="50">
                                            <span class="btn btn-danger btn-sm" @click="deleteImage($event)"><i class="fa fa-remove"></i></span>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            {!! FormHelpers::textArea("Mô tả", "description", 'editRow.description') !!}
                            {!! FormHelpers::textArea("Nội dung", "content", 'editRow.content') !!}

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
<script>
    $(document).ready(function () {
        $('.lfm').filemanager('image');
    })
</script>
