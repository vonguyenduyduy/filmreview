<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div v-bind:class="{'col-sm-7' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="addNew">
                            <i class="fa fa-plus-square-o"></i> Thêm
                        </a>
                        <a class="btn btn-app" @click="editSelected">
                            <i class="fa fa-edit"></i> Sửa
                        </a>
                        <a class="btn btn-app" @click="deleteSelected">
                            <i class="fa fa-remove"></i> Xóa
                        </a>
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                        <i class="fa fa-undo"></i> Hủy
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Action</th>
                                <th>Tên</th>
                                <th>Hình ảnh</th>
                                <th>Giá</th>
                                <th>Khuyến mãi</th>
                                <th>Trạng thái</th>
                                <th>Thứ tự</th>
                                <th>Ngày tạo</th>
                                <th>Ngày cập nhật</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(v,index) in rows">
                                <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm" @click="getUpdateItem(v.id, index, $event.target)"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                                </td>
                                <td>@{{v.name}}</td>
                                <td><img v-bind:src="v.images" height="80"></td>

                                <td>@{{numberToString(v.price)}}</td>
                                <td>@{{v.sales}} %</td>
                                <td>
                                    <div class="form-group">
                                        <button  v-if="v.active == 1"  type="button" class="btn btn-success btn-sm disabled" >Active</button>
                                        <button v-else type="button" class="btn btn-default btn-sm disabled" >Inactive</button>
                                    </div>
                                </td>
                                <td> @{{v.orderBy}}</td>
                                <td> @{{v.created_at}}</td>
                                <td> @{{v.updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-5">
            @include('adminlte::product.module.add')
        </div>
    </div>

</section>





