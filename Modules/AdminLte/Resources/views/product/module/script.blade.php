<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" type="application/javascript"></script>
<script>
 $('.image1').filemanager('image');
    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            dataJson: {},
            editRow: {},
            editId: '',
            showModal: false,
            selected: [],
            title: '',
            parent: '{{!empty($cat) ? $cat->id : 0}}',
            btnSave : '',
            action: false,
            img: 100

        },
        computed:{
          rows: function () {
              return this.dataJson.rows;
          }
        },
        created:function () {
            this.init();
            setTimeout(function(){
                $('#dataTable').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "lengthMenu": [[5,10, 25, 50, -1],[5,10, 25, 50, "All"]]

                });
                $('.lfm').filemanager('image');

            }, 0);

        },

        methods:{
            init: function () {
                var self = this;
                this.ajaxRequest("GET", '{{$routeInit}}', {id: this.parent}, function (data){
                    self.dataJson =  JSON.parse(data);
                });
            },
            addNew : function () {
                this.actionForm(true);
                this.title = "Thêm mới";
                this.btnSave = "Lưu";

            },
            saveForm: function () {
                var self = this;
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.actionForm(false);
                        self.notify('Cập nhật thành công !', 'success');
                        $('html, body').animate({scrollTop: '0px'}, 300);
//                        window.location.reload();
                    }else{
                        self.notify(data.message,'danger');

//                        for(var o in data.message) {
//                            self.notify(data.message,'danger');
//                        }

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },

            getUpdateItem: function (id, index, e) {
                $("#dataTable tr").removeClass('selected');
                $(e).parents('tr').addClass('selected');
                this.editId = index;
                this.title = "Chỉnh sửa ";
                this.btnSave = "Cập nhật";
                this.actionForm(true);
                this.bindDataEdit(index);
                setTimeout(function(){
                    $('.lfm').filemanager('image');
                }, 200);
            },
            bindDataEdit: function (index) {
                $("#formSubmit .type-display").find('option:selected').removeAttr('selected').trigger("change");
                this.editRow = this.rows[index];
                CKEDITOR.instances['description'].setData(this.editRow.description);
                CKEDITOR.instances['content'].setData(this.editRow.content);
                $("#formSubmit .type-display").val(this.editRow.type.split(',')).trigger("change");
                $("#formSubmit .category-display").val(this.splitOthers(this.editRow.others)).trigger("change");

            },
            parseData: function (data) {
                this.dataJson =  JSON.parse(data);
                $('.toggle-one').bootstrapToggle();

            },
            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success
                });
            },
            editSelected: function () {

                if(this.selected.length > 1){
                    this.notify('Pls select only 1 item to edit', 'warning');

                }else if(this.selected.length == 1){
                    this.getUpdateItem(this.selected[0], $(".checklist[value='"+this.selected[0]+"']").attr('data-item'));
                }else{
                    this.notify('Pls select 1 item to edit', 'warning');
                }

            },
            deleteItem: function (id) {
                var self = this;
                this.ajaxRequest("POST", '{{$routeDelete}}', {id: id, parent: this.parent}, function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.notify('Xóa thành công 1 sản phẩm!', 'success');
                    }
                });
            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, parent: this.parent}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Xóa thành công !', 'success');

                        }
                    });
                }else{

                }

            },
            resetForm: function () {
                this.initRowEdit();
                if($("#formSubmit").length > 0){
                    $("#formSubmit")[0].reset();
                    $("#holder").attr("src", "");
                    $('.currency').maskMoney({
                        thousands:".",
                        decimal:",",
                    //  suffix:" đ",
                        precision:0
                    });
                }
            },
            actionForm: function (type) {
                this.resetForm();
                this.action = type;
                if(type){
                    $("#blockForm").show(200);

                }else{
                    $("#blockForm").hide(200);
                    $("#dataTable tr").removeClass('selected');

                }
            },
            initRowEdit: function () {
                this.editRow = {
                "id" : 0,
                "parentID" : '',
                "thumbnail" : "",
                "name" : "",
                "name_en" : "",
                "FKCategory": this.parent,
                "type" : "",
                "price" : 0,
                "sales" : 0,
                "orderBy" : 1,
                "tags" : "",
                "images" :"",
                "description" : "",
                "content": ""
                };
            },
            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            },
            stringParseJson: function (images) {
                if(images){
                    return JSON.parse(images);
                }
                return [];
            },
            deleteImage: function (e) {
                $(e.target).parents('.one-item-image').remove();
            },
            addImage: function(){
                var div = '';
                this.img ++;
                div += '<div v-for="index in 5" class="one-item-image">';
                div +=        '<input style="width: 60%" class="mt-default" id="id_galleries_'+this.img+'" name="galleries[]" type="text">';
                div +=     '<a class="lfm btn btn-default" data-input="id_galleries_'+this.img+'" data-preview="galleries_holder_'+this.img+'">Choose</a>';
                div +=      '<img id="galleries_holder_'+this.img+'" src="" width="50">';
                div +=     '<span class="btn btn-danger btn-sm" v-on:click="deleteImage($event)"><i class="fa fa-remove"></i></span></div>';
                $('#frame-image').append(div);
                $('.lfm').filemanager('image');
            },
            splitOthers: function (oth) {
                var others = [];
                if(oth){
                    others = this.editRow.others.split('-').join('');
                    others = others.split(',');
                }
                return others;
            }


        }

    });
</script>