<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>

<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="row" id="blockForm">
            <div class="col-md-8 col-md-offset-2">
                <div class="box">
                    <div class="box-body">
                        <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                            {{csrf_field()}}
                            <div class="box-header with-border">
                                <h3 class="box-title">@{{ title }}</h3>
                            </div>
                            <div class="box-body">
                                <div class="col-sm-12">
                                    {!! FormHelpers::inputHiddenDefault("id", $detail->id) !!}

                                    {!! FormHelpers::inputDefault(trans('backend.config_key'), "optionID", $detail->optionID, 3) !!}

                                    {!! FormHelpers::inputDefault(trans('backend.value'), "value", $detail->value, 3) !!}

                                    {!! FormHelpers::inputDefault(trans('backend.name'), "name", $detail->name, 3) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{trans('backend.type')}}</label>
                                        <div class="col-sm-3">
                                            <select name="type" class="form-control select2 select2-hidden-accessible" style="width: 100%">
                                                @foreach($configType as $v)
                                                    <option value="{{$v->paramID}}" @if($v->paramID == $detail->type) selected @endif>{{$v->value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                                <button type="button" class="btn btn-success pull-left btn-sm" @click="saveFormDetail"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>




