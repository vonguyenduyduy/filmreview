@extends('adminlte::main.index')

@section('content')
    @include($viewContent)
@endsection

@section('footer_script')

    @include('adminlte::option.module.script')

@endsection
