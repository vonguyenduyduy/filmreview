<script>

    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            editRow: {},
            editId: '',
            selected: [],
            title: '',
            btnSave : '',
            slug : 'abc',
            status: '{{!empty($detail)? $detail->status : 0}}',
            pageType: null,
            postType : ''

        },
        computed:{

        },
        created:function () {
            this.init();

        },

        methods:{
            init: function () {


            },
            saveForm: function () {
                var self = this;

                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        location.href = '{{$routeEdit}}';
                    }else{
                        self.notify(data.message,'danger');

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },

            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success
                });
            },

            pageTypeChange: function (pageType) {
                var self = this;

                this.ajaxRequest("GET", '{{route('backend.post.get_post_by_page_type')}}', {page_type: pageType}, function (data){
                    if(data.success){
                        self.pageType = data.result;
                        $('select[name="type"]').val(null).change();
                    }
                });
            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, catId: this.catId}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Xóa thành công '+self.selected.length+' item !', 'success');

                        }else{
                            for(var o in data.message) {
                                self.notify(data.message[o][0],'danger');
                            }
                        }
                    });
                }else{

                }

            },
            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            }
        }

    });
    function pageTypeChange(e) {
        tableContent.pageTypeChange($(e).val());
    }
    $('.lfm').filemanager('image');

</script>