<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<style>
.status{
    color: #ad0d0d;
}
.status-submit{
    background-color: #af9870;
}
.status-confirm{
    background-color: yellow;
}
.status-notyet{
    background-color: red;
    color: #fff;
}
.status-finish{
    background-color: #008000;
    color: #fff;
}
</style>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div v-bind:class="{'col-sm-8' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        {{--<a class="btn btn-app" @click="addNew">--}}
                            {{--<i class="fa fa-plus-square-o"></i> Thêm--}}
                        {{--</a>--}}
                        <a class="btn btn-app" @click="editSelected">
                            <i class="fa fa-edit"></i> Sửa
                        </a>
                        <a class="btn btn-app" @click="deleteSelected">
                            <i class="fa fa-remove"></i> Xóa
                        </a>
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                            <i class="fa fa-undo"></i> Hủy
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Action</th>
                                <th>Họ & tên</th>
                                <th>username</th>
                                <th>Loại </th>
                                <th>email</th>
                                <th>Phone</th>
                                <th>Địa chỉ</th>
                                <th>Ngày tạo</th>
                                <th>Ngày cập nhật</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(v,index) in rows">
                                <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm" @click="getUpdateItem(v.id, index, $event.target)"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                                </td>
                                <td>@{{v.name}}</td>
                                <td>@{{v.username}}</td>
                                <td><button v-bind:class="classObj(v.type)" type="button" >@{{v.customer_type? v.customer_type.value : 'None_'+v.type}}</button></td>
                                <td>@{{v.email}}</td>
                                <td>@{{v.phone}}</td>
                                <td>@{{v.address}}</td>
                                <td> @{{v.created_at}}</td>
                                <td> @{{v.updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            @include('adminlte::usersfront.module.add')
        </div>
    </div>

</section>




