<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}

                            {!! FormHelpers::input("username", "username", 'editRow.username') !!}
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Loại KH</label>
                                <div class="col-sm-10">
                                    <select name="type" class="form-control select2 type-display" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    @foreach($listCustomerType as $v)
                                            <option value="{{$v->paramID}}">{{$v->value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {!! FormHelpers::input("Họ & tên", "name", 'editRow.name') !!}

                            {!! FormHelpers::input("Điện thoại", "phone", 'editRow.phone') !!}

                            {!! FormHelpers::input("Email", "email", 'editRow.email') !!}

                            {!! FormHelpers::input("Địa chỉ", "address", 'editRow.address') !!}


                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>

                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
