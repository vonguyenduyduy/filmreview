<script>
    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            dataJson: {},
            editRow: {},
            editId: '',
            showModal: false,
            selected: [],
            title: '',
            btnSave : '',
            action: false

        },
        computed:{
          rows: function () {
              return this.dataJson.rows;
          }
        },
        created:function () {
            this.init();
            setTimeout(function(){
                $('#dataTable').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "lengthMenu": [[5,10, 25, 50, -1],[5,10, 25, 50, "All"]]

                });

            }, 0);

        },
        methods:{
            init: function () {
                var self = this;
                this.ajaxRequest("GET", '{{$routeInit}}', {}, function (data){
                    self.dataJson =  JSON.parse(data);
                });
            },
            addNew : function () {
                this.actionForm(true);
                this.title = "Thêm mới";
                this.btnSave = "Lưu";
            },
            saveForm: function () {
                var self = this;
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.actionForm(false);
                        self.notify('Cập nhật thành công !', 'success');

                    }else{
                        for(var o in data.message) {
                            self.notify(data.message[o][0],'danger');
                        }

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },
            deleteItem: function (id) {
                var self = this;
                this.ajaxRequest("POST", '{{$routeDelete}}', {id: id}, function (data){
                    if(data.success){
                        self.parseData(data.result);
                    }else{
                        for(var o in data.message) {
                            self.notify(data.message[o][0],'danger');
                        }
                    }
                });
            },
            getUpdateItem: function (id, index, e) {
                $("#dataTable tr").removeClass('selected');
                $(e).parents('tr').addClass('selected');
                this.actionForm(true);
                this.editId = index;
                this.title = "Chỉnh sửa ";
                this.btnSave = "Cập nhật";
                this.editRow = this.rows[index];

            },
            parseData: function (data) {

                this.dataJson =  JSON.parse(data);

            },
            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success

                });
            },
            editSelected: function () {

                if(this.selected.length > 1){
                    this.notify('Pls select only 1 item to edit', 'warning');

                }else if(this.selected.length == 1){
                    this.getUpdateItem(this.selected[0], $(".checklist[value='"+this.selected[0]+"']").attr('data-item'));
                }else{
                    this.notify('Pls select 1 item to edit', 'warning');
                }

            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, parent: this.parent}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Xóa thành công !', 'success');

                        }else{
                            for(var o in data.message) {
                                self.notify(data.message[o][0],'danger');
                            }
                        }
                    });
                }else{

                }

            },
            resetForm: function () {
                this.initRowEdit();
                if($("#formSubmit").length > 0){
                    $("#formSubmit")[0].reset();
                }
            },
            actionForm: function (type) {
                this.resetForm();
                this.action = type;
                if(type){
                    $("#blockForm").show(200);

                }else{
                    $("#blockForm").hide(200);
                    $("#dataTable tr").removeClass('selected');

                }
            },
            initRowEdit: function () {
                this.editRow = {
                    id: 0,
                    paramID: '',
                    paramCode: '',
                    value: ''
                };
            },
            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            },
            classObj: function (obj) {
                var cls = 'btn status-';
                switch (obj){
                    case 1:
                        cls += 'submit';
                        break;
                    case  2:
                        cls += 'confirm';
                        break;
                    case  3:
                        cls += 'notyet';
                        break;
                    case  4:
                        cls += 'finish';
                        break;
                }
                return cls;
            }
        }

    });
</script>