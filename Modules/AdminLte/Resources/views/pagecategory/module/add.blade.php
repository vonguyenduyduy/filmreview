<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                        <button type="button" class="btn btn-default pull-right btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> {{trans('backend.cancel')}}</button>
                        <button type="button" class="btn btn-success pull-right btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}
                            {!! FormHelpers::inputHidden("parent", 'editRow.parent') !!}

                            {!! FormHelpers::input(trans('backend.name_vi'), "name", 'editRow.name') !!}
                            {!! FormHelpers::input(trans('backend.name_en'), "name_en", 'editRow.name_en') !!}
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{trans('backend.is_menu')}}</label>
                                <div class="col-sm-10">
                                    {!! FormHelpers::inputHidden("is_menu", 'editRow.is_menu') !!}
                                    <button  v-if="editRow.is_menu == 1"  type="button" class="btn btn-success btn-sm" @click="editRow.is_menu = 0">On</button>
                                    <button v-else type="button" class="btn btn-default btn-sm" @click="editRow.is_menu = 1">Off</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{trans('backend.status')}}</label>
                                <div class="col-sm-10">
                                    {!! FormHelpers::inputHidden("active", 'editRow.active') !!}
                                    <button  v-if="editRow.active == 1"  type="button" class="btn btn-success btn-sm" @click="editRow.active = 0">Active</button>
                                    <button v-else type="button" class="btn btn-default btn-sm" @click="editRow.active = 1">Inactive</button>
                                </div>
                            </div>
                            {!! FormHelpers::input(trans('backend.order'), "orderBy", 'editRow.orderBy') !!}
{{--                            {!! FormHelpers::image("Hình ảnh", "images", 'editRow.images') !!}--}}
{{--                            {!! FormHelpers::textArea("Mô tả", "description", 'editRow.description') !!}--}}

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> {{trans('backend.cancel')}}</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
