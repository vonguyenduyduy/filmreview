<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" type="application/javascript"></script>
<script>

    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            dataJson: {},
            editRow: {},
            editId: '',
            showModal: false,
            selected: [],
            title: '',
            parent: 0,
            btnSave : '',
            action: false,

        },
        computed:{
          rows: function () {
              return this.dataJson.rows;
          }
        },
        created:function () {
            this.init();
            setTimeout(function(){
                $('#dataTable').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "lengthMenu": [[5,10, 25, 50, -1],[5,10, 25, 50, "All"]]

                });

            }, 0);

        },

        methods:{
            init: function () {
                var self = this;
                this.ajaxRequest("GET", '{{$routeInit}}', {id: '0'}, function (data){
                    self.dataJson =  JSON.parse(data);
                });
            },
            addNew : function () {
                this.actionForm(true);
                this.title = '{{trans('backend.add')}}';
                this.btnSave = '{{trans('backend.save')}}';

            },
            saveForm: function () {
                var self = this;
                if($('input[name="name"]').val() == ''){
                    self.notify('{{trans('backend.message.title_vi_required')}}','danger');
                    return;
                }
                if($('input[name="name_en"]').val() == ''){
                    self.notify('{{trans('backend.message.title_en_required')}}','danger');
                    return;
                }
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.actionForm(false);
                        self.notify('Cập nhật thành công !', 'success');
                        $('html, body').animate({scrollTop: '0px'}, 300);

                    }else{
                        self.notify(data.message,'danger');
                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },

            getUpdateItem: function (id, index, e) {
                $("#dataTable tr").removeClass('selected');
                $(e.target).parents('tr').addClass('selected');
                this.editId = index;
                this.title = '{{trans('backend.edit')}}';
                this.btnSave = '{{trans('backend.save')}}';
                this.actionForm(true);
                this.bindDataEdit(index);

            },
            bindDataEdit: function (index) {
                $("#formSubmit .type-display").find('option:selected').removeAttr('selected').trigger("change");
                this.editRow = this.rows[index];
                CKEDITOR.instances['description'].setData(this.editRow.description);

            },
            parseData: function (data) {

                this.dataJson =  JSON.parse(data);
                $('.toggle-one').bootstrapToggle();

            },
            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success
                });
            },
            editSelected: function () {

                if(this.selected.length > 1){
                    this.notify('Pls select only 1 item to edit', 'warning');

                }else if(this.selected.length == 1){
                    this.getUpdateItem(this.selected[0], $(".checklist[value='"+this.selected[0]+"']").attr('data-item'));
                }else{
                    this.notify('Pls select 1 item to edit', 'warning');
                }

            },
            deleteItem: function (id) {
                var self = this;
                this.ajaxRequest("POST", '{{$routeDelete}}', {id: id, parent: this.parent}, function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.notify('Deleting success', 'success');
                    }else{
                        self.notify(data.message,'danger');
                    }
                });
            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, parent: this.parent}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Delete success '+self.selected.length+'!', 'success');

                        }else{
                            self.notify(data.message,'danger');
                        }
                    });
                }else{

                }

            },
            resetForm: function () {
                this.initRowEdit();
                if($("#formSubmit").length > 0){
                    $("#formSubmit")[0].reset();
                    $("#holder").attr("src", "");
                    $('.currency').maskMoney({
                        thousands:".",
                        decimal:",",
                    //  suffix:" đ",
                        precision:0
                    });
                }
            },
            actionForm: function (type) {
                this.resetForm();
                this.action = type;
                if(type){
                    $("#blockForm").show(200);

                }else{
                    $("#blockForm").hide(200);
                    $("#dataTable tr").removeClass('selected');

                }
            },
            initRowEdit: function () {
                this.editRow = {
                "id" : 0,
                "active": 0,
                "is_menu": 0,
                "parent" : 0,
                "name" : "",
                "name_en" : "",
                "orderBy" : 1,
                "images" :"",
                "description" : ""
                };
            },
            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            }

        }

    });
</script>