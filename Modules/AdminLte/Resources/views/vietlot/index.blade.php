@extends('adminlte::main.index')


@section('content')
    <?php
    $colorCode = [
    '888888','555555','FF0000','AA0000','660000','FFFF00','CCFFFF','CCFFCC','FFCC00','66CCFF',
    '33CC99','FFFFCC','CCFF66','99FFFF','66FFCC','99FF00','00FFFF','00FF33','FFCCFF','669933',
    '009900','009966', 'FFCC99','006699','CC6633','FF6633','003366','FF0066','660033','000066',
    '663333','003300','999966', 'CC33FF','CC3333','33CCFF','FFCCFF','66FFFF','FF6699','00CC33',
    'CCCCFF','99FF99','0099CC','CC66FF','333300','003366','FF0033','6600FF','0000CC','CC6600',
    ];
    ?>
    <style>
        .small-box{
            text-align: center;
        }
        .num-lot{
            font-size: 2em;
            background-color: #0f9d58;
            padding: 5px 10px;
            border-radius: 3px;
            color: #fff;
        }
        @foreach($colorCode as $k => $v)
            .nu-{{$k}}{
                background-color: {{'#'.$v}};
            }
        @endforeach

    </style>
    @include($viewContent)
@endsection

@section('footer_script')

    @include('adminlte::vietlot.module.script')

@endsection
