<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>

<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-lg-8 col-sm-8 col-lg-10">
            <div v-for="(number,index) in random" class="col-lg-2 col-xs-3">
                <!-- small box -->
                <div style="color: #c5b7b7" v-bind:class="'small-box nu-'+number">
                    <div class="inner">
                        <h3><span >@{{number}}</span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4">
            <button style="padding: 20px; font-size: 2em" type="button" class="btn btn-danger btn-large" @click="getRandom"><i class="fa fa-spinner" aria-hidden="true"></i> GUEST</button>
        </div>
    </div>
    <div class="row">

        <div v-bind:class="{'col-sm-8' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="addNew">
                        <i class="fa fa-plus-square-o"></i> Thêm
                        </a>
                        <a class="btn btn-app" @click="editSelected">
                        <i class="fa fa-edit"></i> Sửa
                        </a>
                        <a class="btn btn-app" @click="deleteSelected">
                        <i class="fa fa-remove"></i> Xóa
                        </a>
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                        <i class="fa fa-undo"></i> Hủy
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Action</th>
                                <th>Thứ tự</th>
                                <th>Number 1</th>
                                <th>Number 2</th>
                                <th>Number 3</th>
                                <th>Number 4</th>
                                <th>Number 5</th>
                                <th>Number 6</th>
                                <th>Ngày xổ</th>
                                <th>Ngày tạo</th>
                                <th>Ngày cập nhật</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(v,index) in rows">
                                <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm" @click="getUpdateItem(v.id, index, $event.target)"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                                </td>
                                <td>@{{ v.id}}</td>
                                <td><span v-bind:class="'num-lot nu-'+v.number_1">@{{v.number_1}}</span></td>
                                <td><span v-bind:class="'num-lot nu-'+v.number_2">@{{v.number_2}}</span></td>
                                <td><span v-bind:class="'num-lot nu-'+v.number_3">@{{v.number_3}}</span></td>
                                <td><span v-bind:class="'num-lot nu-'+v.number_4">@{{v.number_4}}</span></td>
                                <td><span v-bind:class="'num-lot nu-'+v.number_5">@{{v.number_5}}</span></td>
                                <td><span v-bind:class="'num-lot nu-'+v.number_6">@{{v.number_6}}</span></td>
                                <td>@{{v.date_lot}}</td>
                                <td> @{{v.created_at}}</td>
                                <td> @{{v.updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            @include('adminlte::vietlot.module.add')
        </div>
    </div>

</section>




