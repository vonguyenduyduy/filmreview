<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}

                            {!! FormHelpers::input("Number list", "number_1", 'editRow.number_1', 5) !!}
                            {{--{!! FormHelpers::input("Number 2", "number_2", 'editRow.number_2', 5) !!}--}}
                            {{--{!! FormHelpers::input("Number 3", "number_3", 'editRow.number_3', 5) !!}--}}
                            {{--{!! FormHelpers::input("Number 4", "number_4", 'editRow.number_4', 5) !!}--}}
                            {{--{!! FormHelpers::input("Number 5", "number_5", 'editRow.number_5', 5) !!}--}}
{{--                            {!! FormHelpers::input("Number 6", "number_6", 'editRow.number_6', 5) !!}--}}
                            {!! FormHelpers::input("Lot date", "date_lot", 'editRow.date_lot', 5) !!}



                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>

                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
