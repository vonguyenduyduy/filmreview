<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\SystemFeaturesRepository;
use Validator,Response;

class SystemFeatureController extends BaseController
{
    protected $systemFeaturesRepository;

    function __construct(SystemFeaturesRepository $systemFeaturesRepository){
        parent::lteInit();
        $this->systemFeaturesRepository = $systemFeaturesRepository;
        $this->data['routeSave'] = route('backend.system_feature.save');
        $this->data['routeIndex'] = route('backend.system_feature.index');
        $this->data['routeAdd'] = route('backend.system_feature.add');
        $this->data["title"] = "System Features";
    }

    public function index(){

        $this->data["rows"] =$this->systemFeaturesRepository->all();
        $this->data['viewContent'] = 'adminlte::system_feature.module.list';
        return view('adminlte::system_feature.index', $this->data);
    }

    public function add(){
        $this->data['viewContent'] = 'adminlte::system_feature.module.add';
        return view('adminlte::system_feature.index', $this->data);
    }

    public function edit($id){
        $this->data["detail"] =$this->systemFeaturesRepository->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::system_feature.module.edit';
        return view('adminlte::system_feature.index', $this->data);
    }

    public function oneFeature($key){
        $this->data["detail"] =$this->systemFeaturesRepository->findWhere(array('config_key' => $key))->first();
        switch ($key){
            case 'ICON':
            case 'LOGO':
                $this->data['viewContent'] = 'adminlte::system_feature.module.logo';
                break;
            case 'SERVICE1':
            case 'SERVICE2':
            case 'SERVICE3':
            case 'SERVICE4':
                $this->data['viewContent'] = 'adminlte::system_feature.module.service';
                break;
        }
        return view('adminlte::system_feature.index', $this->data);
    }
    public function delete($id){
        $this->systemFeaturesRepository->delete($id);
        return redirect()->route('backend.system_feature.index');
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->systemFeaturesRepository->rules(), $this->systemFeaturesRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->systemFeaturesRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => !empty($param['images'])? $param['images'] : '',
            );
            if(!empty($param['config_key'])){
                $data['config_key'] = $param['config_key'];
            }
            if(!empty($param['description'])){
                $data['description'] = $param['description'];
            }
            if(!empty($param['content'])){
                $data['content'] = $param['content'];
            }
            if(!empty($param['status'])){
                $data['status'] = $param['status'];
            }
            if(!empty($param['order'])){
                $data['order'] = $param['order'];
            }
            if (empty($exits)) {
                $this->systemFeaturesRepository->create($data);
            } else {
                $this->systemFeaturesRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
