<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\AlbumsRepository;
use Modules\AdminLte\Repositories\ProjectsRepository;
use Modules\AdminLte\Repositories\SubImagesRepository;
use Validator,Response, DB;

class AlbumController extends BaseController
{
    protected $albumsRepository;
    protected $projectsRepository;
    protected $subImagesRepository;

    function __construct(AlbumsRepository $albumsRepository, ProjectsRepository $projectsRepository, SubImagesRepository $subImagesRepository){
        parent::lteInit();
        $this->albumsRepository = $albumsRepository;
        $this->projectsRepository = $projectsRepository;
        $this->subImagesRepository = $subImagesRepository;

        $this->data['routeSave'] = route('backend.album.save');
        $this->data["title"] = "album";

    }

    public function album($projectId){
        $this->data["projectId"] = $projectId;

        $this->data["rows"] = $this->albumsRepository->findWhere(array('project_id' => $projectId));
        $this->data['viewContent'] = 'adminlte::album.module.list';
        return view('adminlte::album.index', $this->data);
    }

    public function add($projectId){
        $this->data["projectId"] = $projectId;
        $this->data["projects"] = $this->projectsRepository->findWhere(array('status' => self::ACTIVE));
        $this->data['viewContent'] = 'adminlte::album.module.add';
        return view('adminlte::album.index', $this->data);
    }

    public function edit($projectId, $id){
        $this->data["projectId"] = $projectId;
        $this->data["projects"] = $this->projectsRepository->findWhere(array('status' => self::ACTIVE));
        $this->data["detail"] =$this->albumsRepository->findWhere(array('id' => $id))->first();
        $this->data["subs"] =$this->subImagesRepository->findWhere(array('album_id' => $id));

        $this->data['viewContent'] = 'adminlte::album.module.edit';
        return view('adminlte::album.index', $this->data);
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->albumsRepository->rules(), $this->albumsRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->albumsRepository->find($param['id']): null;
        $subs = [];
        if(!empty($param['sub'])){
            $subs = array_filter($param['sub']);
        }
        DB::beginTransaction();
        try {
            $imgSub = [];

            $data = array(
                'name' => $param['name'],
                'project_id' => $param['project_id'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'order' => !empty($param['order'])? $param['order'] : 0,
            );
            if (empty($exits)) {
                $album = $this->albumsRepository->create($data);
                if(!empty($subs)){
                    foreach ($subs as $s){
                        $imgSub[] = array(
                            'description' => $param['name'],
                            'name' => '',
                            'album_id' => $album->id,
                            'url' => $s
                        );
                    }
                    $this->subImagesRepository->insertMultiRows($imgSub);
                }
            } else {
                $this->splitSubImages($param['id'], $subs);
                $this->albumsRepository->update($data, $param['id']);

            }

        } catch (\Exception $e) {
            DB::rollBack();
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
        DB::commit();
        return Response::json(array('success' => true));
    }
    public function delete($id){
        DB::beginTransaction();
        $album = $this->albumsRepository->find($id);
        try {
            $this->albumsRepository->delete($id);
            $this->subImagesRepository->deleteWhere(array(
                'album_id' => $id
            ));

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();

        }
        DB::commit();

        return redirect()->route('backend.album.index', $album->project_id);
    }
    protected function splitSubImages($albumId, $subs){
        if(empty($subs)){
            return $this->subImagesRepository->deleteWhere(array('album_id' => $albumId));
        }
        $originalSubs = $this->subImagesRepository->findWhere(array('album_id' => $albumId));
        $exist = $except = array();

        foreach ($originalSubs as $v){
            if(in_array($v->url, $subs)){
                $exist[] = $v->url;
            }else{
                $except[] = $v->id;
            }
        }
        if(!empty($except)){
            foreach ($except as $id){
                $this->subImagesRepository->deleteWhere(array('id' => $id));
            }
        }

        foreach ($subs as $url){
            if(!in_array($url, $exist)){
                $data = array(
                    'album_id' => $albumId,
                    'url' => $url
                );

                $this->subImagesRepository->create($data);
            }
        }

    }


}
