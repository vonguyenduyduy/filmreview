<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\ParametersRepository;
use Validator,Response;
use Modules\AdminLte\Repositories\OptionsRepository;
use Modules\AdminLte\Entities\Options;

class OptionController extends BaseController
{
    protected $optionRepo;
    protected $parametersRepository;
    function __construct(OptionsRepository $optionRepo, ParametersRepository $parametersRepository){
        parent::lteInit();

        $this->optionRepo = $optionRepo;
        $this->parametersRepository = $parametersRepository;

        $this->data['routeSave'] = route('backend.option.save');
        $this->data['routeDelete'] = route('backend.option.delete');
        $this->data['routeInit'] = route('backend.option.init');
        $this->data['current'] = 'option';

    }
    public function index(){
        $this->data['title'] = trans('backend.config');
        $this->data['list'] = true;
        $this->data['configType'] = $this->parametersRepository->findWhere(array("paramCode" => self::CONFIG_TYPE));

        $this->data['viewContent'] = 'adminlte::option.module.list';
        return view('adminlte::option.index', $this->data);
    }
    public function detail($id){
        $this->data['title'] = 'Config Detail';
        $this->data['configType'] = $this->parametersRepository->findWhere(array("paramCode" => self::CONFIG_TYPE));

        $this->data['detail'] = $this->optionRepo->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::option.module.detail';
        return view('adminlte::option.index', $this->data);

    }

    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->optionRepo->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Options::whereIn('id', $req['listId'])->delete();

        }else{
            $this->optionRepo->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }

    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->optionRepo->rules(), $this->optionRepo->messages());

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->optionRepo->find($param['id']): null;

        try {

            $data = array(
                'optionID' => $param['optionID'],
                'value' => $param['value'],
                'name' => $param['name'],
                'active' => 1,
                'type' => $param['type'],
            );
            if (empty($exits)) {
                $this->optionRepo->create($data);
            } else {
                $this->optionRepo->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(){
        return $this->getNewUpdate([]);
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
                'rows' => $this->optionRepo
                    ->orderBy('id','desc')
                    ->findWhere($cond)
                    ->toArray(),
            )
        );
    }
}
