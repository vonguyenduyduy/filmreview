<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\MembersRepository;
use Validator,Response;

class MemberController extends BaseController
{
    protected $membersRepository;

    function __construct(MembersRepository $membersRepository){
        parent::lteInit();
        $this->membersRepository = $membersRepository;
        $this->data['routeSave'] = route('backend.member.save');
        $this->data['routeIndex'] = route('backend.member.index');
        $this->data['routeAdd'] = route('backend.member.add');
        $this->data["title"] = "Member";
    }

    public function index(){

        $this->data["rows"] =$this->membersRepository->all();
        $this->data['viewContent'] = 'adminlte::member.module.list';
        return view('adminlte::member.index', $this->data);
    }

    public function add(){
        $this->data['viewContent'] = 'adminlte::member.module.add';

        return view('adminlte::member.index', $this->data);
    }

    public function edit($id){
        $this->data["detail"] =$this->membersRepository->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::member.module.edit';
        return view('adminlte::member.index', $this->data);
    }
    public function delete($id){
        $this->membersRepository->delete($id);
        return redirect()->route('backend.member.index');
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->membersRepository->rules(), $this->membersRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->membersRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'order' => !empty($param['order'])? $param['order'] : 0,
            );
            if (empty($exits)) {
                $this->membersRepository->create($data);
            } else {
                $this->membersRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
