<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Validator,Response;
use Modules\AdminLte\Repositories\ParametersRepository;
use Modules\AdminLte\Entities\Parameters;

class ParameterController extends BaseController
{
    protected $parametersRepository;
    function __construct(ParametersRepository $parametersRepository){
        parent::lteInit();

        $this->parametersRepository = $parametersRepository;
        $this->data['routeSave'] = route('backend.parameter.save');
        $this->data['routeDelete'] = route('backend.parameter.delete');
        $this->data['routeInit'] = route('backend.parameter.init');

        $this->data['current'] = '';
    }
    public function index(){
        $this->data['title'] = 'Parameter';
        $this->data['dataJson'] = $this->getNewUpdate([]);
		$this->data['viewContent'] = 'adminlte::parameter.module.list';
    	return view('adminlte::parameter.index', $this->data);
    }

    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->parametersRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Parameters::whereIn('id', $req['listId'])->delete();

        }else{
            $this->parametersRepository->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }

    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->parametersRepository->rules(), $this->parametersRepository->messages());

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->parametersRepository->find($param['id']): null;

        try {

            $data = array(
                'paramID' => $param['paramID'], 
                'value' => $param['value'], 
                'paramCode' => $param['paramCode'], 
                'active' => 1,
            );
            if (empty($exits)) {
                $this->parametersRepository->create($data);
            } else {
                $this->parametersRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(){
        return $this->getNewUpdate([]);
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
            'rows' => $this->parametersRepository
                ->orderBy('id','desc')
                ->findWhere($cond)
                ->toArray(),
            'rules' => $this->parametersRepository->rules()
            )
        );
    }
}
