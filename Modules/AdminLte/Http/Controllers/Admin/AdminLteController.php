<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\VietlotsRepository;
use Illuminate\Support\Facades\Response;

class AdminLteController extends BaseController
{
    protected $vietlotsRepository;
    const SAME = 4;
    const MAX = 45;
    const MIN = 1;
    function __construct(VietlotsRepository $vietlotsRepository){
        parent::lteNonAuth();

        $this->data['routeSave'] = route('backend.vietlot.save');
        $this->data['routeDelete'] = route('backend.vietlot.delete');
        $this->data['routeInit'] = route('backend.vietlot.init');
        $this->vietlotsRepository = $vietlotsRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->data['dataJson'] = $this->getNewUpdate([]);
        $this->data['viewContent'] = 'adminlte::vietlot.module.list';
        return view('adminlte::vietlot.index', $this->data);
    }
    public function randomToVietlot(){
        $arr = [];
        for ($i = $this::MIN ; $i <= $this::MAX; $i ++){
            $arr[$i] = $i;
        }
        $rand = array_rand($arr, 6);
        if(!$this->startRandom($rand)){
            $this->randomToVietlot();
        }
        return response()->json(array('success' => true, 'random' => $rand));

    }
    protected function startRandom($rand){
        $lots = $this->vietlotsRepository->all();
        foreach ($lots as $v){
            $temp = [
                $v->number_1,
                $v->number_2,
                $v->number_3,
                $v->number_4,
                $v->number_5,
                $v->number_6,
            ];
            $count = 0;
            foreach ($rand as $number){
                if(in_array($number, $temp)){
                    $count ++;
                }
            }

            if($count == $this::SAME){
                return false;
                continue;
                break;
            }
        }

        return true;
    }
    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->vietlotsRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Orders::whereIn('id', $req['listId'])->delete();

        }else{
            $this->vietlotsRepository->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }
    public function update(Request $request){
        $param = $request->all();
        $this->vietlotsRepository->update(array("active" => $param['active'], 'member' => $param['member']), $param['id']);
        return redirect()->back();
    }
    public function create(Request $request) {
        $param = $request->all();

        // if the validator fails, redirect back to the form

        $exits = !empty($param['id'])? $this->vietlotsRepository->find($param['id']): null;

        try {
            $numbers = explode(' ', $param['number_1']);
            $data = array(
                'number_1' => $numbers[0],
                'number_2' => $numbers[1],
                'number_3' => $numbers[2],
                'number_4' => $numbers[3],
                'number_5' => $numbers[4],
                'number_6' => $numbers[5],
                'date_lot' => $param['date_lot'],

            );
            if (empty($exits)) {
                $this->vietlotsRepository->create($data);
            } else {
                $this->vietlotsRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }

    public function getInit(){
        return $this->getNewUpdate([]);
    }
    protected function getNewUpdate($cond){

        return  json_encode(array(
                'rows' => $this->vietlotsRepository
                    ->orderBy('created_at','desc')
                    ->findWhere($cond)
                    ->toArray()
            )
        );
    }
}
