<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\AdminLte\Entities\PageCategories;
use Modules\AdminLte\Repositories\PagesRepository;
use Modules\AdminLte\Repositories\PageCategoriesRepository;
use Modules\AdminLte\Entities\Pages;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\ParametersRepository;
use Modules\AdminLte\Repositories\PostLanguagesRepository;
use Modules\AdminLte\Repositories\PostsRepository;
use Validator,Response;

class PostController extends BaseController
{
    protected $postsRepository;
    protected $postLanguagesRepository;
    protected $parametersRepository;
    protected $pageCategoriesRepository;
    const ALL = 3;
    const CATS = 1;
    const POSTS = 2;
    function __construct(PostsRepository $postsRepository , PostLanguagesRepository $postLanguagesRepository,
                         ParametersRepository $parametersRepository, PageCategoriesRepository $pageCategoriesRepository){
        parent::lteInit();
        $this->postsRepository = $postsRepository;
        $this->postLanguagesRepository = $postLanguagesRepository;
        $this->parametersRepository = $parametersRepository;
        $this->pageCategoriesRepository = $pageCategoriesRepository;
        $this->data['routeSave'] = route('backend.post.save');
        $this->data['routeDelete'] = route('backend.post.delete');
        $this->data['routeInit'] = route('backend.post.init');
        $this->data['routeIndex'] = route('backend.post.index');

        $this->data['current'] = 'post';

    }
    public function index(Request $request){
        $this->data['type'] =  !empty($request->get('type'))? $request->get('type') : self::CATS;
        $this->data['typeCat'] =  !empty($request->get('typeCat'))? $request->get('typeCat') : 0;
        $this->data['onHome'] =  $request->get('onHome');
        $this->data['attr'] =  $request->get('attr');
        $this->data['postName'] =  $request->get('postName');
        $this->data['title'] = trans('backend.posts');
        $this->data['attributeType'] = $this->parametersRepository->findWhere(array("paramCode" => self::ATTRIBUTE_TYPE));

        $this->data['viewContent'] = 'adminlte::post.module.list';
        return view('adminlte::post.index', $this->data);
    }

    public function postByPage(Request $request, $pageCatId){
        $this->data['type'] =  !empty($request->get('type'))? $request->get('type') : self::CATS;
        $this->data['typeCat'] =  !empty($request->get('typeCat'))? $request->get('typeCat') : 0;
        $this->data['onHome'] =  $request->get('onHome');
        $this->data['attr'] =  $request->get('attr');
        $this->data['postName'] =  $request->get('postName');

        $this->data['current'] = 'page_category';
        $this->data['catId'] = $pageCatId;
        $category = $this->pageCategoriesRepository->find($pageCatId);
        $this->data['postCategories'] = $this->postsRepository->getPostCategories(array(
            'type' => self::PARENT_POST,
            'status' => self::ACTIVE,
            'page_type' => $pageCatId,
        ));
        $this->data['attributeType'] = $this->parametersRepository->findWhere(array("paramCode" => self::ATTRIBUTE_TYPE));

        $this->data['title'] = trans('backend.posts'). ' - '. $category->name_en;
        $this->data['viewContent'] = 'adminlte::post.module.list';
        return view('adminlte::post.index', $this->data);
    }
    public function add(Request $request, $id = 0){
        $this->data['title'] = "Post Detail";
        if(!empty($request->get('cat'))){
            $this->data['catId'] = $request->get('cat');
            $this->data['postType'] = $this->postsRepository->getPostCategories(array(
                'type' => self::PARENT_POST,
                'status' => self::ACTIVE,
                'page_type' => $request->get('cat'),
            ));
        }else{
            $this->data['postType'] = $this->postsRepository->getPostCategories(array(
                'type' => self::PARENT_POST,
                'status' => self::ACTIVE
            ));
        }
        $this->data['detail'] = $this->postsRepository->firstOrNew(array('id' => $id));
        $this->data['pageType'] = $this->pageCategoriesRepository->findWhere(array('active' => self::ACTIVE), ['id', 'name_en as name']);
        $this->data['attributeType'] = $this->parametersRepository->findWhere(array("paramCode" => self::ATTRIBUTE_TYPE));
        $this->data['create'] = $id == 0 ? true : false;
        $this->data['postLangs'] = $this->reduceLangs($id);
        $this->data['viewContent'] = 'adminlte::post.module.detail';
        return view('adminlte::post.index', $this->data);
    }
    protected function reduceLangs($id){
        $postLangs = $this->postLanguagesRepository->findWhere(array('post_id' => $id));
        $arr = array();

        foreach ($postLangs as $k => $v){
            $arr[$v->lang_code] = array(
                'name' => $v->name,
                'content' => $v->content,
                'description' => $v->description,
                'sub_title' => $v->sub_title,

            );
        }
        foreach (config('constants.languages') as $code => $name){
            if(!array_key_exists($code, $arr)){
                $arr[$code] = array(
                    'name' => '',
                    'content' => '',
                    'description' => '',
                    'sub_title' => '',
                );
            }
        }

        return $arr;
    }
    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->postsRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            $this->postsRepository->deleteMultiRows($req['listId']);
            $this->postLanguagesRepository->deleteMultiPosts($req['listId']);
        }else{
            $this->postLanguagesRepository->deleteWhere(array("post_id" => $req['id']));
            $this->postsRepository->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }
    public function deleteOne($id){
        DB::beginTransaction();
        try{
            $this->postLanguagesRepository->deleteWhere(array("post_id" => $id));
            $this->postsRepository->delete($id);

        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors("error", $e->getMessage());
        }
        DB::commit();
        return redirect()->route('backend.post.index');

    }
    public function create(Request $request) {
        $param = $request->all();
        $attribute = '';
        $rules = $messages = array();
        if(!empty($request->get('attribute'))){
            $attribute = implode(',', $request->get('attribute'));
        }
        $data = array(
            'status' => $request->get('status'),
            'order' => !empty($request->get('order'))? $request->get('order') : 0,
            'images' => $request->get('images'),
            'type' => $request->get('type'),
            'page_type' => $request->get('page_type'),
            'is_home' => $request->get('is_home'),
            'attribute' => $attribute
        );
        foreach (config('constants.languages') as $k => $v){
            if($k == 'en' && empty($request->get('name')[$k])){
                return $this->responseResultFail("Name is required");
            }
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->responseResultFail("Validation fail !");
        }
        DB::beginTransaction();
        try {

            if(!empty($request->get('id'))){
                $exits = $this->postsRepository->findWhere(array('id' => $request->get('id')))->first();

                if($exits){
                    $this->postsRepository->update($data,$request->get('id'));
                    $this->postLanguagesRepository->createOrganizationLanguage($this->makeLanguageArray($param, $exits->id), $exits->id);
                }
            }else{
                $org = $this->postsRepository->create($data);
                $this->postLanguagesRepository->createOrganizationLanguage($this->makeLanguageArray($param, $org->id), $org->id);
            }

        }catch (\Exception $e) {
            DB::rollBack();
            return $this->responseResultFail($e->getMessage());
        }
        DB::commit();
        return $this->responseBasicResult(array('success' => true));

    }
    protected function makeLanguageArray($request, $postId){
        $arrLang = array();

        foreach (config('constants.languages') as $k => $v){
            $arrLang[$v]['lang_code'] = $k;
            $arrLang[$v]['name'] = $request['name'][$k];
            $arrLang[$v]['slug'] = str_slug($request['name'][$k]);
            $arrLang[$v]['content'] = $request['content'][$k];
            $arrLang[$v]['description'] = $request['description'][$k];
            $arrLang[$v]['sub_title'] = $request['sub_title'][$k];
            $arrLang[$v]['post_id'] = $postId;
        }
        return $arrLang;
    }
    public function getInit(Request $request){
        $cond = array();

        if(!empty($request->get('type'))){
            $type = $request->get('type');
            switch ($type){
                case 1:
                    $cond[] = ['posts.type', '=',  self::PARENT_POST];
                    if(!empty($request->get('onHome'))){
                        $cond[] = ['posts.is_home', '=',  $request->get('onHome')? 1 : 0];
                    }
                    break;
                case  2:
                    $cond[] = ['posts.type', '<>',  self::PARENT_POST];

                    if(!empty($request->get('typeCat'))){
                        $cond[] = ['posts.type', '=',  $request->get('typeCat')];
                    }
                    break;
            }
        }
        if(!empty($request->get('postName'))){
            $cond[] = ['post_languages.name', 'like', "%".$request->get('postName')."%"];
        }
        if(!empty($request->get('attr'))){
            $cond[] = ['posts.attribute', 'like', "%".$request->get('attr')."%"];
        }
        if(!empty($request->get('cat_id'))){
            $cond[] = ['posts.page_type', '=',$request->get('cat_id')];
        }

        return $this->getNewUpdate($cond, $request->get('lang_code'));
    }
    public function getPostByPageType(Request $request){
        if(!empty($request->get('page_type'))){
           return $this->responseBasicResult(array(
               'success' => true,
               'result' => $this->postsRepository->getPostCategories(array(
                   'type' => self::PARENT_POST,
                   'status' => self::ACTIVE,
                   'page_type' => $request->get('page_type'),
               ))
           ));
        }else{
            return $this->responseBasicResult(array('success' => false));
        }
    }

    protected function getNewUpdate($cond, $lang = 'en'){

        return  json_encode(array(
                'rows' => $this->postsRepository->getAllByLang($cond, $lang)
            )
        );
    }
}
