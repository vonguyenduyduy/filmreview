<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\BannersRepository;
use Modules\AdminLte\Repositories\NewsCategoriesRepository;
use Modules\AdminLte\Repositories\NewsRepository;
use Modules\AdminLte\Repositories\ParametersRepository;
use Modules\AdminLte\Repositories\ProjectsRepository;
use Validator,Response;

class NewsController extends BaseController
{
    protected $newsRepository;
    protected $newsCategoriesRepository;
    protected $parametersRepository;

    function __construct(NewsRepository $newsRepository, NewsCategoriesRepository $newsCategoriesRepository,
                        ParametersRepository $parametersRepository){
        parent::lteInit();
        $this->newsRepository = $newsRepository;
        $this->newsCategoriesRepository = $newsCategoriesRepository;
        $this->parametersRepository = $parametersRepository;

        $this->data['routeSave'] = route('backend.news.save');
        $this->data["title"] = "project";
    }

    public function index($newsId){
        $this->data["newsId"] = $newsId;
        $this->data["rows"] = $this->newsRepository->findWhere(array('category_id' => $newsId));
        $this->data['viewContent'] = 'adminlte::news.module.list';

        return view('adminlte::news.index', $this->data);
    }

    public function add($catId){
        $this->data["newsId"] = $catId;
        $this->data["categories"] = $this->newsCategoriesRepository->findWhere(array('status' => self::ACTIVE));
        $this->data['viewContent'] = 'adminlte::news.module.add';
        $this->data['attributeType'] = $this->parametersRepository->findWhere(array("paramCode" => self::ATTRIBUTE_TYPE));

        return view('adminlte::news.index', $this->data);
    }

    public function edit($catId, $id){
        $this->data["newsId"] = $catId;
        $this->data["categories"] = $this->newsCategoriesRepository->findWhere(array('status' => self::ACTIVE));
        $this->data["detail"] =$this->newsRepository->findWhere(array('id' => $id))->first();
        $this->data['attributeType'] = $this->parametersRepository->findWhere(array("paramCode" => self::ATTRIBUTE_TYPE));

        $this->data['viewContent'] = 'adminlte::news.module.edit';
        return view('adminlte::news.index', $this->data);
    }
    public function delete($catId, $id){
        $this->newsRepository->delete($id);
        return redirect()->route('backend.news.index', $catId);
    }
    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->newsRepository->rules(), $this->newsRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $attribute = '';
        if(!empty($request->get('attribute'))){
            $attribute = implode(',', $request->get('attribute'));
        }
        $exits = !empty($param['id'])? $this->newsRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'category_id' => $param['category_id'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'is_home' => $param['is_home'],
                'order' => !empty($param['order'])? $param['order'] : 0,
                'attribute' => $attribute
            );
            if (empty($exits)) {
                $this->newsRepository->create($data);
            } else {
                $this->newsRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
