<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Validator,Redirect;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    public function showLogin()
    {
        $this->data['viewContent'] = 'adminlte::user.module.login';
        return view('adminlte::user.index', $this->data);
    }

    public function doLogin(Request $request)
    {
        $rules = array(
            'username'    => 'required', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make($request->all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect()->route('backend.admin.login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'username'     => Input::get('username'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {
                return Redirect()->route('backend.project.index');

            } else {

                return Redirect()->route('backend.admin.login');
            }

        }
    }
    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect()->route('backend.admin.login');
    }
}
