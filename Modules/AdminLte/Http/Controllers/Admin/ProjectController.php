<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\AlbumsRepository;
use Modules\AdminLte\Repositories\BannersRepository;
use Modules\AdminLte\Repositories\ProjectsRepository;
use Validator,Response;

class ProjectController extends BaseController
{
    protected $projectsRepository;
    protected $albumsRepository;

    function __construct(ProjectsRepository $projectsRepository, AlbumsRepository $albumsRepository){
        parent::lteInit();
        $this->projectsRepository = $projectsRepository;
        $this->albumsRepository = $albumsRepository;

        $this->data['routeSave'] = route('backend.project.save');
        $this->data['routeIndex'] = route('backend.project.index');
        $this->data['routeAdd'] = route('backend.project.add');
        $this->data["title"] = "project";
    }

    public function index(){
        $this->data["rows"] =$this->projectsRepository->all();
        $this->data['viewContent'] = 'adminlte::project.module.list';
        return view('adminlte::project.index', $this->data);
    }

    public function add(){
        $this->data['viewContent'] = 'adminlte::project.module.add';
        return view('adminlte::project.index', $this->data);
    }

    public function edit($id){
        $this->data["detail"] =$this->projectsRepository->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::project.module.edit';
        return view('adminlte::project.index', $this->data);
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->projectsRepository->rules(), $this->projectsRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->projectsRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'order' => !empty($param['order'])? $param['order'] : 0,
            );
            if (empty($exits)) {
                $this->projectsRepository->create($data);
            } else {
                $this->projectsRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }

    public function delete($id){

        $this->projectsRepository->delete($id);
        $this->albumsRepository->deleteWhere(array(
            'project_id' => $id
        ));

        return redirect()->route('backend.project.index');
    }


}
