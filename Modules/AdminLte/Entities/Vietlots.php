<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Vietlots extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'id', 'number_1','number_2', 'number_3', 'number_4', 'number_5', 'number_6', 'date_lot'
    ];
    protected $table = "vietlots";

}
