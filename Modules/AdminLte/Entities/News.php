<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class News extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'news';

    protected $fillable = [
        'id', 'slug','category_id', 'name', 'images','description', 'content', 'order', 'status', 'is_home', 'attribute'
    ];

    public function category(){
        return $this->hasOne(NewsCategories::class, 'id', 'category_id');
    }

}
