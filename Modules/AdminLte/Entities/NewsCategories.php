<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class NewsCategories extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'news_categories';

    protected $fillable = [
        'id', 'slug',  'name', 'images','description', 'content', 'order', 'status'
    ];

}
