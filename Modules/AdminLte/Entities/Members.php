<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Members extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'members';

    protected $fillable = [
        'id', 'slug', 'name', 'images','description', 'content', 'order', 'status'
    ];

}
