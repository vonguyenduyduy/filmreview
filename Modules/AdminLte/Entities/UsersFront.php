<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersFront extends Authenticatable
{

    protected $table = 'users_front';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password','type'
    ];
    public function customer_type(){
        return $this->hasOne(Parameters::class,  'paramID', 'type');
    }
}
