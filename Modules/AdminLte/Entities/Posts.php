<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Modules\AdminLte\Entities\PageCategories;

class Posts extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'posts';

    protected $fillable = [
        'id',
        'type',
        'name',
        'slug',
        'images',
        'description',
        'content',
        'order',
        'status',
        'attribute',
        'is_home',
        'page_type',

    ];
    public function getName(){
        return $this->hasOne(PostLanguages::class, "post_id", "id");
    }

    public function category(){
        return $this->hasOne(PageCategories::class, "id", "page_type");
    }

    public function posts_child(){
        return $this->hasOne(PageCategories::class, "id", "page_type");
    }
}
