<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Products extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'products';
    public $fillable = [
        'id',
        'FKCategory',
        'name',
        'slug',
        'images','image1','image2','image3','image4',
        'thumbnail',
        'type',
        'description',
        'content',
        'price',
        'active',
        'orderBy',
        'parentID',
        'created_at',
        'updated_at',
        'tags',
        'sales',
        'others'
    ];
    public function getParameter(){
        return $this->hasOne(Parameters::class, 'paramID', 'type');
    }
    public function getProductsByOthers(){
        return $this->hasMany(Products::class, 'FKCategory' , 'others');
    }
    public function category(){
        return $this->hasOne(ProductCategories::class , 'id', 'FKCategory');
    }
}
