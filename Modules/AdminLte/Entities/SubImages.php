<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class SubImages extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'sub_images';

    protected $fillable = [
        'id', 'album_id', 'name', 'url','description'
    ];

}
