<?php

namespace Modules\AdminLte\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Modules\AdminLte\Entities\Orders;

class OrdersRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Orders::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}