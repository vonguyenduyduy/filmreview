<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\SystemFeatures;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class SystemFeaturesRepository extends BaseRepository
{
    public function model()
    {
        return SystemFeatures::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
