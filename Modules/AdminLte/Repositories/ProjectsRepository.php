<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Projects;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class ProjectsRepository extends BaseRepository
{
    public function model()
    {
        return Projects::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
