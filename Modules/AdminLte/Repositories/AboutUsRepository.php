<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\AboutUs;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class AboutUsRepository extends BaseRepository
{
    public function model()
    {
        return AboutUs::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
