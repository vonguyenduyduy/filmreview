<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Vietlots;
use Prettus\Repository\Eloquent\BaseRepository;

class VietlotsRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Vietlots::class;
    }


}