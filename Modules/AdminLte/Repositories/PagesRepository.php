<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Pages;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class PagesRepository extends BaseRepository
{
    public function model()
    {
        return Pages::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên sản phẩm',
        ];
    }

}
