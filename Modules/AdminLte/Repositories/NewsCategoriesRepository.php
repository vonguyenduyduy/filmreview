<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\NewsCategories;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class NewsCategoriesRepository extends BaseRepository
{
    public function model()
    {
        return NewsCategories::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
