<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Posts;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class PostsRepository extends BaseRepository
{
    const SCOLIOSIS_TREATMENT = 2;
    const ABOUT_US = 4;
    const CONTACTS = 5;
    const NEWS = 6;
    const STATUS_ACTIVE = 1;

    public function model()
    {
        return Posts::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }
    public function messages()
    {
        return [
            'name.required' => 'Name'.trans('backend.require'),
        ];
    }

    public function deleteMultiRows(array $list)
    {
        return $this->model->whereIn('id', $list)->delete();
    }

    public function getAllByLang($cond, $lang){
        return $this->model
            ->select('posts.*', 'post_languages.name as name')
            ->leftJoin('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
//            ->leftJoin('parameters', 'parameters.paramID', '=', 'posts.type')
            ->where('post_languages.lang_code', $lang)
            ->where($cond)
            ->orderBy('id','desc')
            ->get();
    }

    public function getPostCategories($cond, $lang = 'en'){
        return $this->model
            ->select('posts.*', 'post_languages.name as name', 'post_languages.description', 'post_languages.sub_title')
            ->leftJoin('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->where('post_languages.lang_code', $lang)
            ->where($cond)
            ->orderBy('id','desc')
            ->get();
    }

    public function getPostByConditions($cond, $limit = null) {
        $query = $this->model
            ->select('posts.*', 'post_languages.name as name', 'post_languages.sub_title as subtitle', 'post_languages.description as description',
                'post_languages.content as content', 'post_languages.slug as slug','page_categories.slug as category_slug')
            ->join('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->join('page_categories', function($join)
            {
                $join->on('posts.page_type', '=', 'page_categories.id');
            })
            ->where('posts.status', config('constants.STATUS_ACTIVE'))
            ->where($cond)
            ->orderBy('posts.order','asc');

            if(!empty($limit)){
                $query->limit($limit);
            }

            return $query->get();
    }
    public function getPostWithLang($cond, $lang){
        return $this->model
            ->select('posts.*', 'post_languages.name as name', 'post_languages.sub_title', 'post_languages.description',
                'post_languages.content as content', 'post_languages.slug')
            ->join('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->where($cond)
            ->where('post_languages.lang_code', $lang)
            ->get();
    }

    public function getPostDetailsBySlug($slug, $lang) {
        return $this->model
            ->select('posts.*', 'post_languages.name as name', 'post_languages.sub_title as subtitle', 'post_languages.description as description',
                'post_languages.content as content', 'post_languages.slug as slug','page_categories.id as category_id','page_categories.name as category_name',
                'page_categories.name_en as category_name_en','page_categories.slug as category_slug')
            ->join('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->join('page_categories', function($join)
            {
                $join->on('posts.page_type', '=', 'page_categories.id');
            })
            ->where('post_languages.lang_code', $lang)
            ->where('post_languages.slug', $slug)
            ->where('posts.status', config('constants.STATUS_ACTIVE'))
            ->orderBy('posts.order','asc')
            ->get()->first();
    }

    public function getPostDetailsById($id, $lang) {
        return $this->model
            ->select('posts.*', 'post_languages.name as name', 'post_languages.sub_title as subtitle', 'post_languages.description as description',
                'post_languages.content as content', 'post_languages.slug as slug','page_categories.id as category_id','page_categories.name as category_name',
                'page_categories.name_en as category_name_en','page_categories.slug as category_slug')
            ->join('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->join('page_categories', function($join)
            {
                $join->on('posts.page_type', '=', 'page_categories.id');
            })
            ->where('post_languages.lang_code', $lang)
            ->where('posts.id', $id)
            ->where('posts.status', config('constants.STATUS_ACTIVE'))
            ->orderBy('posts.order','asc')
            ->get()->first();
    }

    public function getFirstPostByCategory($pageType, $lang) {
        return $this->model
            ->select('post_languages.slug as slug')
            ->join('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->join('page_categories', function($join)
            {
                $join->on('posts.page_type', '=', 'page_categories.id');
            })
            ->where('post_languages.lang_code', $lang)
            ->where('posts.page_type', $pageType)
            ->where('posts.status', config('constants.STATUS_ACTIVE'))
            ->orderBy('posts.order','asc')
            ->limit(1)
            ->get()->first();
    }

    public function getPostByCategoryWithLang($cond, $lang){
        return $this->with(['category', 'getName' => function ($q) use ($lang){
            return $q->where('lang_code' , $lang);
        }])->getPostCategories($cond, $lang);
    }

    public function getPostByCategorySlug($slug, $lang){
        $postCats = $this->model->with('category')->whereHas('category', function($q) use ($slug){
            $q->where('slug', $slug);
        })
            ->select('posts.*', 'post_languages.name as name', 'post_languages.sub_title', 'post_languages.description',
                'post_languages.content as content', 'post_languages.slug')
            ->join('post_languages', function($join)
            {
                $join->on('post_languages.post_id', '=', 'posts.id');
            })
            ->where('post_languages.lang_code', $lang)
            ->where(array(
            'posts.type' => 0,
            'posts.status' => 1,
            'posts.is_home' => 1
        ), $lang)->orderBy('posts.order','asc')->get();
        foreach ($postCats as $k => $v){
            $postCats[$k]->child = $this->getPostWithLang(array(
                'posts.type' => $v->id,
                'posts.status' => 1,
            ), $lang);
        }
        return $postCats;
    }
}
