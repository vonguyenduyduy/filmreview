<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Banners;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class BannersRepository extends BaseRepository
{
    public function model()
    {
        return Banners::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
