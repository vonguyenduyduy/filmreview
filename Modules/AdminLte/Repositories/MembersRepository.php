<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Members;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class MembersRepository extends BaseRepository
{
    public function model()
    {
        return Members::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
