<?php

namespace Modules\AdminLte\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\AdminLte\Repositories\PageCategoriesRepository;

class AdminLteServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */

    protected $pageCategoriesRepository;
    public function boot(PageCategoriesRepository $pageCategoriesRepository)
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();

        view()->share('pageCategories',$pageCategoriesRepository->findWhere(array(
            'active' => 1
        ), ['id', 'name_en as name']));


//        dd($pageCategoriesRepository->all());
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('adminlte.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'adminlte'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/adminlte');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/adminlte';
        }, \Config::get('view.paths')), [$sourcePath]), 'adminlte');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/adminlte');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'adminlte');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'adminlte');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
