@extends('frontend::main.index')
@section('content')
    <link rel='stylesheet' id='elementor-post-8-css'  href='/wp-content/plugins/css/post-8.css' type='text/css' media='all' />

    <div class="elementor elementor-8">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section data-id="dmqfnvk"
                         class="elementor-element elementor-element-dmqfnvk elementor-section-boxed elementor-section-height-full elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                         data-element_type="section">
                    <div class="elementor-background-overlay"></div>

                    @include('frontend::home.module.slide')

                </section>
                <section data-id="orzgbei"
                         class="elementor-element elementor-element-orzgbei elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                         data-element_type="section">

                    @include('frontend::home.module.about_us')

                </section>
                <section data-id="gsulzki"
                         class="elementor-element elementor-element-gsulzki elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}"
                         data-element_type="section">

                    @include('frontend::home.module.couter_day')

                </section>
                <section data-id="icfhdpu"
                         class="elementor-element elementor-element-icfhdpu elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                         data-element_type="section">

                    @include('frontend::home.module.portfolio')

                </section>
                <section data-id="r9x9k7r"
                         class="elementor-element elementor-element-r9x9k7r elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-tilt&quot;,&quot;shape_divider_bottom&quot;:&quot;tilt&quot;}"
                         data-element_type="section">

                    @include('frontend::home.module.middle_text')

                </section>
                <section data-id="bnjsyof"
                         class="elementor-element elementor-element-bnjsyof elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                         data-element_type="section">

                    @include('frontend::home.module.latest_video')

                </section>
                <section data-id="bnjsyof"
                         class="elementor-element elementor-element-bnjsyof elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                         data-element_type="section">

                    @include('frontend::home.module.latest_post')

                </section>
                <section data-id="khalzzr"
                         class="elementor-element elementor-element-khalzzr elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                         data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                         data-element_type="section">
                    @include('frontend::home.module.get_in_touch')

                </section>
                {{--<section data-id="fc0d7f1"--}}
                         {{--class="elementor-element elementor-element-fc0d7f1 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"--}}
                         {{--data-element_type="section">--}}

                    {{--@include('frontend::home.module.footer_images')--}}

                {{--</section>--}}
            </div>
        </div>
    </div>
@endsection