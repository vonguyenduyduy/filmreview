<div class="elementor-background-overlay"></div>
<div class="elementor-shape elementor-shape-bottom" data-negative="false">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
        <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"/>
        <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"/>
        <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"/>
    </svg>		</div>
<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="dehrloj" class="elementor-element elementor-element-dehrloj elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="wittpf0" class="elementor-element elementor-element-wittpf0 the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-center clearfix">
                                <h2 class="the-title">
                                    {{isset($options['list_services_text'])? $options['list_services_text']: 'The Culture We Always Keep'}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    <section data-id="umowyyq" class="elementor-element elementor-element-umowyyq elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeInUp elementor-invisible elementor-section elementor-inner-section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">

                                @foreach($services as $k => $v)
                                <div data-id="itusrkg" class="elementor-element elementor-element-itusrkg elementor-column elementor-col-25 elementor-inner-column" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="tunqpck" class="elementor-element elementor-element-tunqpck elementor-widget elementor-widget-heading" data-element_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <a href="{{route('service', [$v->slug, $v->id])}}"><h2 class="elementor-heading-title elementor-size-default">0{{$k + 1}}</h2></a>
                                                </div>
                                            </div>
                                            <div data-id="pi77tre" class="elementor-element elementor-element-pi77tre the-title-left the-title-center elementor-widget elementor-widget-nikah-head-title" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-left clearfix">
                                                        <a href="{{route('service', [$v->slug, $v->id])}}"><h2 class="the-title">{{$v->name}}</h2></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>