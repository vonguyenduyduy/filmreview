<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="2bb594d" class="elementor-element elementor-element-2bb594d elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="da9fc24" class="elementor-element elementor-element-da9fc24 elementor-widget elementor-widget-nikah-gallery" data-element_type="nikah-gallery.default">
                        <div class="elementor-widget-container">
                            <div class="elementor-image-gallery custom-gallery clearfix">

                                <div id="lightgallery" class="gallery gallery-columns-10">
                                    @foreach($newsOnFooter as $v)
                                    <div data-src="{{$v->images}}" class="mobile-column-5 tablet-column-5 gallery-item" data-sub-html="">
                                        <div class="item-wrap">
                                            <figure class="imghvr-zoom-in">
                                                <img src="{{$v->images}}" alt="{{$v->images}}">
                                                <figcaption>
                                                    <div class="caption-inside">

                                                        <div class="gallery-icon ih-fade ih-delay-xs">
                                                            <i class="fa fa-search"></i>
                                                        </div>
                                                        <!-- if use title -->
                                                        <h3 class="gallery-caption ih-fade ih-delay-sm">
                                                        </h3>
                                                        <!-- if use title -->

                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <script type="text/javascript">
                                    (function($) {
                                        'use strict';

                                        $(document).ready(function() {
                                            $("#lightgallery").lightGallery({
                                                mode: 'lg-slide',
                                                download: true,
                                            });
                                        });

                                    })( jQuery );
                                </script>
                            </div>		</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
