<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="klxsdgu" class="elementor-element elementor-element-klxsdgu elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="1evp12m" class="elementor-element elementor-element-1evp12m elementor-widget elementor-widget-nikah-slider" data-element_type="nikah-slider.default">
                        <div class="elementor-widget-container">


                            <link href="http://fonts.googleapis.com/css?family=Raleway:400%2C500|Roboto:500|Open+Sans:300" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                            <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background-color:#ffffff;padding:0px;">
                                <!-- START REVOLUTION SLIDER 5.3.1.5 fullscreen mode -->
                                <div id="rev_slider_1_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.1.5">
                                    <ul>
                                        <!-- SLIDE  -->
                                        @foreach($banners as $k => $v)
                                            @if($k%2 == 0)
                                                <li data-index="rs-{{$k + 1}}" data-transition="slidingoverlaydown" data-slotamount="default" data-hideafterloop="0"
                                                    data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"
                                                    data-thumb="{{$v->images}}"  data-rotate="0"  data-saveperformance="off"
                                                    data-title="Excellence" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                                                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="{{$v->images}}"  alt="" title="slider1"  width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->

                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                                         id="slide-1-layer-3"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                                         data-width="full"
                                                         data-height="full"
                                                         data-whitespace="nowrap"

                                                         data-type="shape"
                                                         data-basealign="slide"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.30);"> </div>

                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption   tp-resizeme"
                                                         id="slide-1-layer-1"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['top','top','top','top']" data-voffset="['178','151','218','186']"
                                                         data-fontsize="['200','160','130','90']"
                                                         data-lineheight="['180','150','130','90']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"

                                                         data-type="text"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['center','center','center','center']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 6; white-space: nowrap; font-size: 200px; line-height: 180px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa;">{{$v->name}}</div>

                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption   tp-resizeme"
                                                         id="slide-1-layer-2"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['top','top','top','top']" data-voffset="['581','494','523','394']"
                                                         data-fontsize="['20','20','20','16']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"

                                                         data-type="text"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">{!! $v->description !!} </div>


                                                    <!-- LAYER NR. 5 -->
                                                    <div class="tp-caption rev-scroll-btn "
                                                         id="slide-1-layer-10"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                                                         data-width="35"
                                                         data-height="55"
                                                         data-whitespace="nowrap"

                                                         data-type="button"
                                                         data-responsive_offset="on"
                                                         data-responsive="off"
                                                         data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
							<span>
							</span>
                                                    </div>
                                                </li>
                                            @else
                                                <li data-index="rs-{{$k + 1}}" data-transition="slidingoverlayleft" data-slotamount="default" data-hideafterloop="0"
                                                    data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"
                                                    data-thumb="{{$v->images}}"  data-rotate="0"  data-saveperformance="off"
                                                    data-title="Ceremonies" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                                                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="{{$v->images}}"  alt="" title="slider2s"  width="1950" height="1300" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->

                                                    <!-- LAYER NR. 6 -->
                                                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                                         id="slide-2-layer-3"
                                                         data-x="['center','center','center','center']" data-hoffset="['1','1','1','1']"
                                                         data-y="['middle','middle','middle','middle']" data-voffset="['-2','-2','-2','-2']"
                                                         data-width="full"
                                                         data-height="full"
                                                         data-whitespace="nowrap"

                                                         data-type="shape"
                                                         data-basealign="slide"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.30);"> </div>

                                                    <!-- LAYER NR. 7 -->
                                                    <div class="tp-caption   tp-resizeme"
                                                         id="slide-2-layer-1"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['middle','middle','middle','middle']" data-voffset="['-10','-40','-47','-37']"
                                                         data-fontsize="['240','220','160','90']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"

                                                         data-type="text"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['center','center','center','center']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 6; white-space: nowrap; font-size: 240px; line-height: 150px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa;">{{$v->name}} </div>

                                                    <!-- LAYER NR. 8 -->
                                                    <div class="tp-caption   tp-resizeme"
                                                         id="slide-2-layer-2"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['middle','middle','middle','middle']" data-voffset="['92','81','43','21']"
                                                         data-fontsize="['20','20','20','16']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"

                                                         data-type="text"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">{!! $v->description !!} </div>


                                                    <!-- LAYER NR. 10 -->
                                                    <div class="tp-caption rev-scroll-btn "
                                                         id="slide-2-layer-10"
                                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']"
                                                         data-width="35"
                                                         data-height="55"
                                                         data-whitespace="nowrap"

                                                         data-type="button"
                                                         data-responsive_offset="on"
                                                         data-responsive="off"
                                                         data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
							<span>
							</span>
                                                    </div>

                                                    <!-- LAYER NR. 11 -->
                                                    <div class="tp-caption   tp-resizeme"
                                                         id="slide-2-layer-12"
                                                         data-x="['left','left','left','left']" data-hoffset="['246','165','143','100']"
                                                         data-y="['top','top','top','top']" data-voffset="['311','250','356','270']"
                                                         data-fontsize="['30','30','24','18']"
                                                         data-width="none"
                                                         data-height="none"
                                                         data-whitespace="nowrap"

                                                         data-type="text"
                                                         data-responsive_offset="on"

                                                         data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                                         data-paddingtop="[0,0,0,0]"
                                                         data-paddingright="[0,0,0,0]"
                                                         data-paddingbottom="[0,0,0,0]"
                                                         data-paddingleft="[0,0,0,0]"

                                                         style="z-index: 10; white-space: nowrap; font-size: 30px; line-height: 22px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">

                                                    </div>
                                                </li>
                                            @endif
                                        @endforeach

                                    </ul>
                                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                                        if(htmlDiv) {
                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                        }else{
                                            var htmlDiv = document.createElement("div");
                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                        }
                                    </script>
                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
                                <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                                    if(htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    }else{
                                        var htmlDiv = document.createElement("div");
                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                                <script type="text/javascript">
                                    /******************************************
                                     -	PREPARE PLACEHOLDER FOR SLIDER	-
                                     ******************************************/

                                    var setREVStartSize=function(){
                                        try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                                            e.c = jQuery('#rev_slider_1_1');
                                            e.responsiveLevels = [1240,1024,778,480];
                                            e.gridwidth = [1240,1024,778,480];
                                            e.gridheight = [868,768,960,720];

                                            e.sliderLayout = "fullscreen";
                                            e.fullScreenAutoWidth='off';
                                            e.fullScreenAlignForce='off';
                                            e.fullScreenOffsetContainer= '';
                                            e.fullScreenOffset='';
                                            if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})

                                        }catch(d){console.log("Failure at Presize of Slider:"+d)}
                                    };

                                    setREVStartSize();

                                    var tpj=jQuery;

                                    var revapi1;
                                    tpj(document).ready(function() {
                                        if(tpj("#rev_slider_1_1").revolution == undefined){
                                            revslider_showDoubleJqueryError("#rev_slider_1_1");
                                        }else{
                                            revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                                sliderType:"standard",
                                                jsFileLocation:"//nikah1.themesawesome.com/wp-content/plugins/revslider/public/assets/js/",
                                                sliderLayout:"fullscreen",
                                                dottedOverlay:"none",
                                                delay:3000,
                                                navigation: {
                                                    keyboardNavigation:"off",
                                                    keyboard_direction: "horizontal",
                                                    mouseScrollNavigation:"off",
                                                    mouseScrollReverse:"default",
                                                    onHoverStop:"off",
                                                    arrows: {
                                                        style:"hermes",
                                                        enable:true,
                                                        hide_onmobile:true,
                                                        hide_under:768,
                                                        hide_onleave:false,
                                                        {{--tmp:'<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div>	<div class="tp-arr-titleholder">{{title}}</div>	</div>',--}}
                                                        left: {
                                                            h_align:"left",
                                                            v_align:"center",
                                                            h_offset:0,
                                                            v_offset:0
                                                        },
                                                        right: {
                                                            h_align:"right",
                                                            v_align:"center",
                                                            h_offset:0,
                                                            v_offset:0
                                                        }
                                                    }
                                                },
                                                responsiveLevels:[1240,1024,778,480],
                                                visibilityLevels:[1240,1024,778,480],
                                                gridwidth:[1240,1024,778,480],
                                                gridheight:[868,768,960,720],
                                                lazyType:"none",
                                                shadow:0,
                                                spinner:"spinner4",
                                                stopLoop:"off",
                                                stopAfterLoops:-1,
                                                stopAtSlide:-1,
                                                shuffle:"off",
                                                autoHeight:"off",
                                                fullScreenAutoWidth:"off",
                                                fullScreenAlignForce:"off",
                                                fullScreenOffsetContainer: "",
                                                fullScreenOffset: "",
                                                disableProgressBar:"on",
                                                hideThumbsOnMobile:"off",
                                                hideSliderAtLimit:0,
                                                hideCaptionAtLimit:0,
                                                hideAllCaptionAtLilmit:0,
                                                debugMode:false,
                                                fallbacks: {
                                                    simplifyAll:"off",
                                                    nextSlideOnWindowFocus:"off",
                                                    disableFocusListener:false,
                                                }
                                            });
                                        }
                                    });	/*ready*/
                                </script>
                                <script>
                                    var htmlDivCss = '	#rev_slider_1_1_wrapper .tp-loader.spinner4 div { background-color: #FFFFFF !important; } ';
                                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                    if(htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    }
                                    else{
                                        var htmlDiv = document.createElement('div');
                                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                                <script>
                                    var htmlDivCss = unescape(".hermes.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A30px%3B%0A%09height%3A110px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hermes.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A15px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20110px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%0A.hermes%20.tp-arr-allwrapper%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%09width%3A180px%3B%0A%20%20%20%20height%3A140px%3B%0A%20%20%20%20top%3A0px%3B%0A%20%20%20%20left%3A0px%3B%0A%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20-webkit-transform%200.3s%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%200.3s%3B%0A%20%20-webkit-perspective%3A%201000px%3B%0A%20%20perspective%3A%201000px%3B%0A%20%20%20%20%7D%0A.hermes.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20right%3A0px%3Bleft%3Aauto%3B%0A%20%20%20%20%20%20%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20visibility%3Avisible%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A.hermes%20.tp-arr-imgholder%20%7B%0A%20%20width%3A180px%3Bposition%3Aabsolute%3B%0A%20%20left%3A0px%3Btop%3A0px%3Bheight%3A110px%3B%0A%20%20transform%3Atranslatex%28-180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-180px%29%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20transition-delay%3A0.3s%3B%0A%7D%0A.hermes.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20transform%3Atranslatex%28180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28180px%29%3B%0A%20%20%20%20%20%20%7D%0A%20%20%0A.hermes.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%20%20%20%20%20%20%20%20%20%20%20%20%0A%7D%0A.hermes%20.tp-arr-titleholder%20%7B%0A%20%20top%3A110px%3B%0A%20%20width%3A180px%3B%0A%20%20text-align%3Aleft%3B%20%0A%20%20display%3Ablock%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20line-height%3A30px%3B%20background%3A%23000%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20font-weight%3A600%3B%20position%3Aabsolute%3B%0A%20%20font-size%3A12px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20letter-spacing%3A1px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%200%3B%0A%20%20transform-origin%3A%2050%25%200%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%0A%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20%20-webkit-transition-delay%3A%200.6s%3B%0A%20%20transition-delay%3A%200.6s%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%3B%0A%20%20transform%3A%20rotatex%280deg%29%3B%0A%7D%0A%0A");
                                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                    if(htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    }
                                    else{
                                        var htmlDiv = document.createElement('div');
                                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                            </div><!-- END REVOLUTION SLIDER -->		</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
