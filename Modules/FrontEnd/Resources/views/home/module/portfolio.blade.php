<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="fwbyeex" class="elementor-element elementor-element-fwbyeex elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="i9xbf17" class="elementor-element elementor-element-i9xbf17 the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-center clearfix">
                                <h2 class="the-title">
                                    {{isset($options['project_menu'])? $options['project_menu']: @trans('front.project')}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div data-id="uaqvmof" class="elementor-element elementor-element-uaqvmof the-title-center the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-center clearfix">
                                <h2 class="the-title">
                                    {{isset($options['project_bottom'])? $options['project_bottom']: 'Our Projects'}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div data-id="rdnoioe" class="elementor-element elementor-element-rdnoioe extra-padding-use mix-filter-use elementor-widget elementor-widget-nikah-portfolio-block" data-element_type="nikah-portfolio-block.default">
                        <div class="elementor-widget-container">

                            <div id="some-1256736513" class="portfolio-grid-block porf-hidetitle-st clearfix">

                                <div class="filter-wraper">
                                    <div id="mobile-filter-id" class="mobile-filter container clearfix">
                                        <button id="filter-icon">
                                            <span class="bar bar-1"></span>
                                            <span class="bar bar-2"></span>
                                            <span class="bar bar-3"></span>
                                            <span class="bar bar-4"></span>
                                        </button>
                                    </div>
                                    <ul id="portfolio-filter" class="filters container style-1 clearfix">
                                        <li class="activeFilter">
                                            <a data-filter="*" href="#" class="filter-btn">@lang('front.all')</a>
                                        </li>
                                        @foreach($projects as $v)
                                            <li><a data-filter=".{{$v->slug}}" href="#"  class="filter-btn">{{$v->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- #portfolio-filter end -->

                                <div class="portfolio-block-wrap grid-template column-4 clearfix">

                                    @foreach($albums as $v)

                                    <div class="portfo-block-item grid-item mobile-column-1 tablet-column-2 {{$v->project_slug}}">
                                        <div class="item-wrap">
                                            <a href="{{route('project', [$v->slug, $v->id])}}">
                                                <h3 class="portfolio-loop-title-mobile">{{$v->name}}</h3>

                                                <figure class="imghvr-image-rotate-left">
                                                    <img src="{{$v->images}}" alt="Sarah &#038; Adam" width="800" height="600">
                                                    <figcaption >
                                                        <div class="caption-inside">

                                                            <!-- if use title -->
                                                            <h3 class="portfolio-loop-title ih-fade-down ih-delay-sm">{{$v->name}}</h3>
                                                            <!-- if use title -->

                                                            <!-- if use category -->
                                                            <h5 class="portfolio-category ih-fade-up ih-delay-sm">{{$v->project_name}}</h5>
                                                            <!-- if use category -->

                                                        </div>
                                                    </figcaption>
                                                </figure>

                                            </a>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>



                            </div>
                            <script type="text/javascript">
                                (function($) {
                                    'use strict';

                                    $(document).ready(function(){

                                        $('#some-1256736513 figcaption .caption-inside').css('transform', 'translate(-50%, -50%)');

                                        var $grid = $('.grid-template').imagesLoaded( function() {
                                            // init Masonry after all images have loaded
                                            $grid.isotope({
                                                transitionDuration: '0.65s',
                                                initLayout: true,
                                                columnWidth: '.grid-item',
                                                itemSelector: '.grid-item',
                                                fitWidth: true,
                                                stagger: 30,
                                            });
                                        });



                                        $('#portfolio-filter a').click(function(){
                                            $('#portfolio-filter li').removeClass('activeFilter');
                                            $(this).parent('li').addClass('activeFilter');
                                            var selector = $(this).attr('data-filter');
                                            $grid.isotope({ filter: selector });
                                            return false;
                                        });
                                    });

                                })( jQuery );
                            </script><!-- Portfolio Script End -->


                        </div>
                    </div>
                    <div data-id="yvivjfy" class="elementor-element elementor-element-yvivjfy elementor-align-center elementor-mobile-align-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="button.default">
                        <div class="elementor-widget-container">
                            <div class="elementor-button-wrapper">
                                <a href="{{route('project_all')}}" class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">@lang('front.view_all')</span>
		</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
