<!doctype html public '-//w3c//dtd xhtml 1.0 transitional//en' 'http://www.w3.org/tr/xhtml1/dtd/xhtml1-transitional.dtd'><HTML>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='vi' lang='vi'>
<head>
    @include('frontend::main.head')
</head>
<body id='pagetop'>
<div class='bcontents00'>
    <div class='bcontents0'>
        <div id='header'>
            <a class='logo' href='{{url('/')}}' onfocus='blur();' title='Việt Nam CLEAR Vẹo cột sống Điều trị bones+beyond TRANG CHỦ'>
                <img src='{{ asset('_images/header_logo.png') }}' border='0' alt='Việt Nam CLEAR Vẹo cột sống Điều trị bones+beyond TRANG CHỦ' />
            </a>
            <div class='headertxt'>@lang("frontend::main.header_line_1")<br />
                @lang("frontend::main.header_line_2")
            </div>
            <a style="text-decoration: none" class='phone' href='#' onfocus='blur();' title='GỌI CHO CHÚNG TÔI! VIỆT NAM {{getValueConfig('phone')}}' >
                <span><i style="font-size: 1.5em" class="fas fa-phone"></i></span>
                <span style="color: #000; font-weight: bold; font-size: 2em;"> {{getValueConfig('phone')}}</span>
                <div style="background-color: #6ba221; padding: 2px 10px; font-weight: bold; color: #fff;font-size: 1.4em;">@lang('frontend::main.call_us_now')</div>
            </a>

        </div>
        <div class='mainmenu'>
            @include('frontend::main.menu')
        </div>
        <div class='bcontents'>
            @yield('content')
            <div class='bcontents1'>
                @include('frontend::main.sidebar')
            </div>
            <br clear='all' />
        </div>
        <br />
        <br />
        <div class='clear'></div>
        <a class='pagetop' href='#' onclick='pageScroll("pagetop");'>@lang('frontend::main.pagetop')</a><div class='clear'></div>
        <div id='footer'>
            @include('frontend::main.footer')

        </div>
    </div>
</div>
<!-- Load javascript session -->
<script type='text/javascript' src='/js/default.htm.js?v={{config('app.version')}}'></script>
<script type='text/javascript' src='/js/frontend.js?v={{config('app.version')}}'></script>

</body>
</html>