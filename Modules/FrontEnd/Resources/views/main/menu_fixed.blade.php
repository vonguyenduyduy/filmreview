<div id="sticky-wrap-head" class="sticky-header-wrap header_fixed_noscroll clearfix">
    <!-- Header
    ============================================= -->
    <header id="header" class="header-style-1-wrap inner-head-wrap  animated  clearfix">

        <div class="container clearfix">

            <div class="header-clear  clearfix">
                <div class="fl header1-2 horizontal header_left_float clearfix">
                    <!-- Logo
============================================= -->
                    <div class="logo head-item">

                        <div class="logo-image">
                            <a href="/">
                                <img src="{{$logo->images}}" alt="logo"/>
                            </a>
                        </div>
                    </div>
                    <!-- end logo -->
                </div>

                <div class="fr header1-2 vertical header_right_float clearfix">
                    <!-- Mobile menu toggle button (hamburger/x icon) -->
                    <input id="main-menu-state" type="checkbox"/>
                    <label class="main-menu-btn sub-menu-triger" for="main-menu-state">
                        <span class="main-menu-btn-icon"></span>
                    </label>

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu" class="menu main-menu head-item">
                        <ul id="menu-menu" class="sm sm-clean menu--ferdinand">
                            <li id="menu-item-41"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-41 @if(url()->current() == route('home')) current-menu-item @endif">
                                <a class="menu__link"
                                   href="/">{{isset($options['home_menu'])? $options['home_menu']: @trans('front.home')}}</a>
                            </li>
                            <li id="menu-item-48"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48 @if(url()->current() == route('about_us')) current-menu-item @endif">
                                <a class="menu__link"
                                   href="{{route('about_us')}}">{{isset($options['about_us_menu'])? $options['about_us_menu']: @trans('front.about_us')}}</a>
                            </li>
                            <li id="menu-item-56"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56 @if($menuActive == 'project') current-menu-item @endif">
                                <a class="menu__link"
                                   href="{{route('project_all')}}">{{isset($options['project_menu'])? $options['project_menu']: @trans('front.project')}}</a>
                            </li>
                            <li id="menu-item-87"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87 @if($menuActive == 'video') current-menu-item @endif">
                                <a class="menu__link"
                                   href="{{route('video_all')}}">{{isset($options['video_menu'])? $options['video_menu']: @trans('front.video')}}</a>
                            </li>
                            <li id="menu-item-87"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87 @if($menuActive == 'news') current-menu-item @endif">
                                <a class="menu__link"
                                   href="{{route('news_all')}}">{{isset($options['news_menu'])? $options['news_menu']: @trans('front.news')}}</a>
                            </li>

                            <li id="menu-item-40"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40 @if(url()->current() == route('contact')) current-menu-item @endif">
                                <a class="menu__link"
                                   href="{{route('contact')}}">{{isset($options['contact_menu'])? $options['contact_menu']: @trans('front.contact')}}</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- end primary menu -->
                    <div class="search-wrap head-item">
                        <button id="btn-search"><i class="icon-simple-line-icons-143"></i></button>
                    </div>
                </div>
            </div>

        </div>

    </header>

</div>
<div class="sticky-header-gap header_fixed_noscroll clearfix"></div>
<div class="search">
    <button id="btn-search-close" class="btn--search-close"><i class="icon-themify-1"></i></button>

    <form method="get" class="searchform search__form" action="{{route('search')}}" role="search">
        <input type="search" class="field search__input" name="key" value="" id="s" placeholder="Search"/>
        <button type="submit" class="submit search-button" value=""><i class="icon-simple-line-icons-143"></i></button>
    </form>

    <span class="search__info">Hit enter to search or ESC to close</span>

    <div class="search__related">
        <div class="search__suggestion">
        </div>
    </div>
</div>
<!-- HEADER END -->