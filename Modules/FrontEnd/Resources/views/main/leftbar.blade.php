<div class="col l3 hide-on-med-and-down">

    <div class="category-product">
        {{--<div class="category-pal">--}}
            {{--<span>{{trans('front.category_list')}}</span>--}}
        {{--</div>--}}
        <ul class="lb-level1">

            <li>
                <ul class="collapsible collapsible-accordion glass-human">
                    <li ><a  class="collapsible-header active" ><b>{{trans('front.category_list')}}<i class="mdi-action-input right"></i></b></a>
                        <div class="collapsible-body">
                            <ul>
                                <li class="@if(url()->current() == route('product_all')) side-active @endif"><a href="{{route('product_all')}}"><i class="material-icons dp48 left">border_inner</i>Tất cả sản phẩm</a></li>
                                @foreach ($categories as $v)
                                    <li class="@if(url()->current() == route('front.category.level1', [$v->slug, $v->id])) side-active @endif"><a href="{{route('front.category.level1', [$v->slug, $v->id])}}"><i class="material-icons dp48 left">blur_on</i>{{$v->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="category-product_new">
        <div class="category-pal">
            <span>{{trans('front.feature_product')}}</span>
        </div>
        <div class="slide-new-sp">
            <ul id="product-new">

                @foreach ($featureProducts as  $v)

                <li>
                    <div class="row">
                        <a href="{{route('product.detail', [$v->category->slug, $v->slug, $v->id])}}">
                            <div class="col m5 s5"><img class="card z-depth-2" src="{{$v->images}}" width="80" height="80"></div>
                            <div class="col m7 s7">
                                <p>
                                    <h6>{{$v->name}}</h6>
                                </p>
                            </div>
                        </a>
                    </div>


                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var slider_new = $('#product-new').bxSlider({
                mode:'vertical',
                controls:false,
                // slideWidth: 250,
                minSlides: 4,
                moveSlides: 1,
                pager: false,
                slideMargin: 22,
                auto:true,
            });
        })
    </script>
</div>