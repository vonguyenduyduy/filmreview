<ul class='layer1'>
    @foreach($pageMenu as $key => $menuItem)
        @if($menuItem->slug == 'home')
            <li><a class='layer1_0' href='/{{$menuItem->slug}}'><img class='layer1img' src='{{ asset('_images/layer1img_'.$menuItem->slug.'.jpg') }}' border='0' alt='' />{{$menuItem->name}}</a></li>
        @else
            <li>
                <a href='javascript:void(0)' class='layer1 {{$menuActive == $menuItem->slug? 'layer1_1':'layer1_0'}}' onclick='rcSlideBox("block{{$key}}");return false;'>
                <img class='layer1img' src='{{ asset('_images/layer1img_'.$menuItem->slug.'.jpg') }}' border='0' alt='' />{{$menuItem->name}}</a>
                <ul id='block{{$key}}' class='layer2 {{$menuActive == $menuItem->slug? 'v1':'v0'}}'>
                    @foreach($menuItem->posts as $post)
                        @if(isset($post->name) && !empty($post->name))
                        <li>
                            @if(!isset($post->hasChildren))
                                @if(isset($post->slug) && $post->slug == $postActive)
                                    <span class='layer2 menuactive'>{{$post->name}} @if(isset($post->is_new))<img src='{{ asset('_images/new.gif') }}' border='0' alt='' />@endif</span>
                                @elseif(isset($post->slug))
                                    <a class='layer2 menuinactive' href='{{route('pageView', [$menuItem->slug, isset($post->slug) ? $post->slug : '', $post->id])}}' border='0' alt=''>{{$post->name}} @if(isset($post->is_new))<img src='{{ asset('_images/new.gif') }}' border='0' alt='' />@endif</a>
                                @endif
                            @else
                                <span class='layer2'>{{$post->name}}</span>
                                <ul class='layer3'>
                                    @foreach($subPosts[$post->id] as $subPost)
                                        @if(isset($subPost->name) && !empty($subPost->name))
                                            @if($subPost->slug == $postActive)
                                                <span class='layer3 menuactive'>{{$subPost->name}} @if(isset($subPost->is_new)))<img src='{{ asset('_images/new.gif') }}' border='0' alt='' />@endif</span>
                                            @else
                                                <li><a class='layer3 menuinactive' href='{{route('pageView', [$menuItem->slug, isset($subPost->slug) ? $subPost->slug : '', $subPost->id])}}' target='_self'>{{$subPost->name}} @if(isset($subPost->is_new))<img src='{{ asset('_images/new.gif') }}' border='0' alt='' />@endif</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                        @endif
                    @endforeach
                </ul>
            </li>
        @endif
    @endforeach
</ul>