@extends('frontend::main.index')
@section('content')
    <link rel='stylesheet' id='elementor-post-8-css'  href='{{asset('/wp-content/plugins/css/post-42.css')}}' type='text/css' media='all' />

    <div id="content" class="content-wrapper clearfix">

        <div class="page-content clearfix">
            <div class="elementor elementor-42">
                <div class="elementor-inner">
                    <div class="elementor-section-wrap">
                        <section data-id="tovxiij" class="elementor-element elementor-element-tovxiij elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}" data-element_type="section">
                            <div class="elementor-background-overlay"></div>
                            <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
                                    <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                    <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                    <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                </svg>		</div>
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="zqknlbu" class="elementor-element elementor-element-zqknlbu elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="t9fouh9" class="elementor-element elementor-element-t9fouh9 the-title-center elementor-widget elementor-widget-nikah-head-title" data-element_type="nikah-head-title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="head-title head-title-2 text-center clearfix">
                                                            <h2 class="the-title">{{$detail->name}}</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section data-id="ivodgtu" class="elementor-element elementor-element-ivodgtu elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="pshcyzn" class="elementor-element elementor-element-pshcyzn elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="3mzoktm" class="elementor-element elementor-element-3mzoktm elementor-widget elementor-widget-image" data-element_type="image.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-image">
                                                            <img width="1024" height="756" src="{{$detail->top_image}}" class="attachment-large size-large" alt=""
                                                                 srcset="{{$detail->top_image}} 1024w, {{$detail->top_image}} 300w, {{$detail->top_image}} 768w, {{$detail->top_image}} 1064w"
                                                                 sizes="(max-width: 1024px) 100vw, 1024px">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-id="nqnfukw" class="elementor-element elementor-element-nqnfukw elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="vvuoc5q" class="elementor-element elementor-element-vvuoc5q the-title-left animated animated-slow elementor-widget elementor-widget-nikah-head-title fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-head-title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="head-title head-title-2 text-left clearfix">
                                                            {{--<h2 class="the-title">--}}
                                                                {{--What We do--}}
                                                            {{--</h2>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-id="5j0i7it" class="elementor-element elementor-element-5j0i7it animated animated-slow elementor-widget elementor-widget-nikah-text fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-text.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="nikah-text clearfix">
                                                            {!! $detail->top_text !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section data-id="pz6k0hq" class="elementor-element elementor-element-pz6k0hq elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-tilt&quot;}" data-element_type="section">
                            <div class="elementor-shape elementor-shape-top" data-negative="false">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
                                    <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                    <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                    <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                </svg>		</div>
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="0zgcswv" class="elementor-element elementor-element-0zgcswv elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="1ssvfzc" class="elementor-element elementor-element-1ssvfzc the-title-left animated animated-slow elementor-widget elementor-widget-nikah-head-title fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-head-title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="head-title head-title-2 text-left clearfix">
                                                            {{--<h2 class="the-title">--}}
                                                                {{--Planning a Destination Wedding?--}}
                                                            {{--</h2>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-id="cjr04bo" class="elementor-element elementor-element-cjr04bo animated animated-slow elementor-widget elementor-widget-nikah-text fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-text.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="nikah-text clearfix">
                                                            {!! $detail->middle_text !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-id="pshcyzn" class="elementor-element elementor-element-pshcyzn elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="3mzoktm" class="elementor-element elementor-element-3mzoktm elementor-widget elementor-widget-image" data-element_type="image.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-image">
                                                            <img width="1024" height="756" src="{{$detail->middle_image}}" class="attachment-large size-large" alt=""
                                                                 srcset="{{$detail->middle_image}} 1024w, {{$detail->middle_image}} 300w, {{$detail->middle_image}} 768w, {{$detail->middle_image}} 1064w"
                                                                 sizes="(max-width: 1024px) 100vw, 1024px">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        {{--<section data-id="nlurwco" class="elementor-element elementor-element-nlurwco elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">--}}
                            {{--<div class="elementor-container elementor-column-gap-no">--}}
                                {{--<div class="elementor-row">--}}
                                    {{--<div data-id="cnplapg" class="elementor-element elementor-element-cnplapg elementor-column elementor-col-50 elementor-top-column" data-element_type="column">--}}
                                        {{--<div class="elementor-column-wrap elementor-element-populated">--}}
                                            {{--<div class="elementor-widget-wrap">--}}
                                                {{--<div data-id="9czrz4u" class="elementor-element elementor-element-9czrz4u the-title-left animated animated-slow elementor-widget elementor-widget-nikah-head-title fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-head-title.default">--}}
                                                    {{--<div class="elementor-widget-container">--}}
                                                        {{--<div class="head-title head-title-2 text-left clearfix">--}}
                                                            {{--<h2 class="the-title">--}}
                                                                {{--Awesome Team	</h2>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div data-id="ybx0e24" class="elementor-element elementor-element-ybx0e24 animated animated-slow elementor-widget elementor-widget-nikah-text fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-text.default">--}}
                                                    {{--<div class="elementor-widget-container">--}}
                                                        {{--<div class="nikah-text clearfix">--}}
                                                            {{--<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p></div>		</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div data-id="wikbgwy" class="elementor-element elementor-element-wikbgwy elementor-column elementor-col-50 elementor-top-column" data-element_type="column">--}}
                                        {{--<div class="elementor-column-wrap elementor-element-populated">--}}
                                            {{--<div class="elementor-widget-wrap">--}}
                                                {{--<div data-id="jfyeyv3" class="elementor-element elementor-element-jfyeyv3 extra-padding-use animated animated-slow elementor-widget elementor-widget-nikah-team-block fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-team-block.default">--}}
                                                    {{--<div class="elementor-widget-container">--}}
                                                        {{--<div class="team-block-section team-grid2-style clearfix">--}}
                                                            {{--<div class="team-block-wrap team-grid-2-wrap">--}}
                                                                {{--@foreach($members as $k => $v)--}}
                                                                    {{--@if ($k > 3)--}}
                                                                        {{--@break--}}
                                                                    {{--@endif--}}
                                                                {{--<div class="team-block column column-3 mobile-column-1 tablet-column-3">--}}
                                                                    {{--<div class="team-inner-block clearfix">--}}
                                                                        {{--<img src="{{$v->images}}" alt="About">--}}

                                                                        {{--<div class="team-details clearfix">--}}
                                                                            {{--<div class="inner-team-details">--}}
                                                                                {{--<h3 class="team-name">{{$v->name}}</h3>--}}
                                                                                {{--<h5 class="team-job">{!! $v->description !!}</h5>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}

                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--@endforeach--}}


                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                        {{--<section data-id="csv1whl" class="elementor-element elementor-element-csv1whl elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">--}}
                            {{--<div class="elementor-container elementor-column-gap-no">--}}
                                {{--<div class="elementor-row">--}}
                                    {{--<div data-id="ws956cc" class="elementor-element elementor-element-ws956cc elementor-column elementor-col-100 elementor-top-column" data-element_type="column">--}}
                                        {{--<div class="elementor-column-wrap elementor-element-populated">--}}
                                            {{--<div class="elementor-widget-wrap">--}}
                                                {{--<div data-id="ycld74x" class="elementor-element elementor-element-ycld74x extra-padding-use animated animated-slow elementor-widget elementor-widget-nikah-team-block fadeIn" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="nikah-team-block.default">--}}
                                                    {{--<div class="elementor-widget-container">--}}
                                                        {{--<div class="team-block-section team-grid2-style clearfix">--}}
                                                            {{--<div class="team-block-wrap team-grid-2-wrap">--}}
                                                                {{--@foreach($members as $k => $v)--}}
                                                                    {{--@if ($k > 3)--}}
                                                                    {{--<div class="team-block column column-4 mobile-column-1 tablet-column-3">--}}
                                                                    {{--<div class="team-inner-block clearfix">--}}
                                                                        {{--<img src="http://nikah1.themesawesome.com/wp-content/uploads/sites/96/2017/11/h1-team-member-6.jpg" alt="About">--}}

                                                                        {{--<div class="team-details clearfix">--}}
                                                                            {{--<div class="inner-team-details">--}}
                                                                                {{--<h3 class="team-name">{{$v->name}}</h3>--}}
                                                                                {{--<h5 class="team-job">{!! $v->description !!}</h5>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}

                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                    {{--@endif--}}

                                                                {{--@endforeach--}}

                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                        <section data-id="pazqesw" class="elementor-element elementor-element-pazqesw elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="elsblvs" class="elementor-element elementor-element-elsblvs elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="pf4mrx0" class="elementor-element elementor-element-pf4mrx0 the-title-center elementor-widget elementor-widget-nikah-head-title" data-element_type="nikah-head-title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="head-title head-title-2 text-center clearfix">
                                                            <h2 class="the-title">
                                                                {{isset($options['slogan'])? $options['slogan']: 'Write your special wishes. We love to hear from all of you...	'}}
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-id="euoxeim" class="elementor-element elementor-element-euoxeim elementor-align-center elementor-widget elementor-widget-button" data-element_type="button.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-button-wrapper">
                                                            <a href="{{route('contact')}}" class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">@lang('front.contact')</span>
		</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div>
@endsection