@extends('frontend::main.news')
@section('content')
    <section id="content" class="search-page blog-content-wrap clearfix">

        <div class="page-title">
            <div class="container">
                <h3><span>Search Results</span>
                    project</h3>
            </div>
        </div><!-- .page-header -->

        <!-- BLOG START
        ============================================= -->
        <div class="blog search-result clearfix">
            <div class="container">
                <div class="row">

                    <!-- BLOG LOOP START
                    ============================================= -->
                    <div class="column column-2of3 masonry-post post-masonry-style masonry-style-1 clearfix">
                        <div class="blog-section main-blog-loop infinite-container content-section masonry-style-loop grid" style="position: relative; height: 236px;">
                            @if(count($projects))
                                @foreach($projects as $v)
                                    <article id="post-52" class="post-52 page type-page status-publish hentry blog-item masonry-blog column column-2" style="position: absolute; left: 0px; top: 0px;">

                                        <div class="loop-content">

                                            <div class="info">

                                                <div class="top-info">
                                                    <span class="category"></span>
                                                </div>

                                                <h4 class="title"><a href="{{route('project_all')}}">{{$v->name}}</a></h4>


                                                <div class="post-excerpt post-text">
                                                    <p>GET IN TOUCH</p>
                                                </div>

                                                <div class="more-button clearfix">
                                                    <a href="{{route('project')}}" title="Project" class="more">
                                                        VIEW
                                                    </a>
                                                </div>

                                            </div>
                                        </div><!-- post-content -->

                                    </article>
                                @endforeach
                            @endif

                                @if(count($albums))
                                    @foreach($albums as $v)
                                        <article id="post-52" class="post-52 page type-page status-publish hentry blog-item masonry-blog column column-2" style="position: absolute; left: 0px; top: 0px;">

                                            <div class="loop-content">

                                                <div class="info">

                                                    <div class="top-info">
                                                        <span class="category"></span>
                                                    </div>

                                                    <h4 class="title"><a href="{{route('project', [$v->slug, $v->id])}}">{{$v->name}}</a></h4>


                                                    <div class="post-excerpt post-text">
                                                        <p>GET IN TOUCH</p>
                                                    </div>

                                                    <div class="more-button clearfix">
                                                        <a href="{{route('project', [$v->slug, $v->id])}}" title="Project" class="more">
                                                            VIEW
                                                        </a>
                                                    </div>

                                                </div>
                                            </div><!-- post-content -->

                                        </article>
                                    @endforeach
                                @endif
                                @if(count($categoryNews))
                                    @foreach($categoryNews as $v)
                                        <article id="post-52" class="post-52 page type-page status-publish hentry blog-item masonry-blog column column-2" style="position: absolute; left: 0px; top: 0px;">

                                            <div class="loop-content">

                                                <div class="info">

                                                    <div class="top-info">
                                                        <span class="category"></span>
                                                    </div>

                                                    <h4 class="title"><a href="{{route('news_category', [$v->slug, $v->id])}}">{{$v->name}}</a></h4>


                                                    <div class="post-excerpt post-text">
                                                        <p>GET IN TOUCH</p>
                                                    </div>

                                                    <div class="more-button clearfix">
                                                        <a href="{{route('news_category', [$v->slug, $v->id])}}" title="Project" class="more">
                                                            VIEW
                                                        </a>
                                                    </div>

                                                </div>
                                            </div><!-- post-content -->

                                        </article>
                                    @endforeach
                                @endif
                                @if(count($news))
                                    @foreach($news as $v)
                                        <article id="post-52" class="post-52 page type-page status-publish hentry blog-item masonry-blog column column-2" style="position: absolute; left: 0px; top: 0px;">

                                            <div class="loop-content">

                                                <div class="info">

                                                    <div class="top-info">
                                                        <span class="category"></span>
                                                    </div>

                                                    <h4 class="title"><a href="{{route('news', [$v->slug, $v->id])}}">{{$v->name}}</a></h4>


                                                    <div class="post-excerpt post-text">
                                                        <p>GET IN TOUCH</p>
                                                    </div>

                                                    <div class="more-button clearfix">
                                                        <a href="{{route('news', [$v->slug, $v->id])}}" title="Project" class="more">
                                                            VIEW
                                                        </a>
                                                    </div>

                                                </div>
                                            </div><!-- post-content -->

                                        </article>
                                    @endforeach
                                @endif

                        </div>

                        <div class="blog-standard pagination clearfix">
                        </div>
                    </div>
                    <!-- BLOG LOOP END -->
                    @include('frontend::news.module.sidebar')
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- search-result -->

    </section>
@endsection