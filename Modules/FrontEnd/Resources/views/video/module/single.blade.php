<section id="content" class="single-post-wrap clearfix">

    <!-- BLOG START
    ============================================= -->
    <div class="blog right-sidebar clearfix">
        <div class="container clearfix">
            <div class="row clearfix">

                <!-- BLOG LOOP START
                ============================================= -->
                <div class="column column-2of3 clearfix">
                    <div class="blog-single content-section">

                        <article id="post-78" class="blog-item hentry post-78 post type-post status-publish format-standard has-post-thumbnail category-pre-wedding tag-blog tag-post tag-standard ">

                            <div class="post-content-wrap">
                                <div class="post-content">
                                    <div class="post-thumb">
                                        <iframe width="100%" height="328" id="ytvideo" frameborder="0"
                                                allowfullscreen src="http://www.youtube.com/embed/{{$detail->ytb_url}}?autoplay=0"></iframe>
                                    </div><!-- thumbnail-->


                                    <div class="content-inner-wrapper clearfix">
                                        <div class="meta-wrapper clearfix">

                                            <h1 class="post-title entry-title"><a href="">{{$detail->name}}</a></h1>

                                            <div class="post-meta clearfix">
                                                <div class="meta-info">
                                                    <span class="date">
								                    <span>Posted On</span>
								                    <a href="#">{{$detail->updated_at}}</a>
							                        </span>
                                                </div>
                                            </div>

                                            <div class="separator-line"><span></span></div>

                                        </div>

                                        <div class="post-text entry-content">
                                            <div>
                                                {!! $detail->content !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </article><!-- #post-78 -->



                    </div>
                </div>

                <!-- BLOG LOOP END -->

                <!-- SIDEBAR START
                ============================================= -->
                @include('frontend::news.module.sidebar')
                <!-- SIDEBAR END -->

            </div>
        </div>
    </div>
    <!-- BLOOG END -->

</section>