<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 15/01/2018
 * Time: 15:30
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'Trang chủ',
    'scoliosis_treatment' => 'Liệu pháp trị chứng vẹo cột sống',
    'chiropractic_treatment' => 'Nắn khớp xương',
    'about_bones_beyond' => 'Về chúng tôi',
    'access_&_contact' => 'Liên hệ',
    'header_line_1' => 'Việt Nam CLEAR Vẹo cột sống Điều trị & Chiropractic',
    'header_line_2' => 'Hoa Kỳ chuyên nghiệp được cấp phép Chiropractic và CLEAR Điều trị Chứng vẹo cột sống "bones+beyond"',
    'menu_text' => 'Dành cho Bệnh nhân Vẹo cột sống Việt Nam / Liệu pháp chỉnh hình Chuyên nghiệp được Cấp phép Hoa Kỳ / Được cấp phép bởi CLEAR / Liệu pháp Điều trị Vẹo cột sống Chuyên sâu được Cấp ',
    'call_us_text_main' => 'Hãy gọi chúng tôi để được giải đáp các thắc mắc liên quan đến chứng vẹo cột sống, điều trị chỉnh hình, hoặc bất cứ các triệu chứng cơ thể nào. Kết hợp với khả năng tự phục hồi tự nhiên của cơ thể và tâm trí bạn, Bác sĩ Kieu sẽ giúp bạn đạt được mục tiêu của mình... không đau đớn... tốt cho sức khỏe... và một cách tự nhiên!',
    'call_us_now' => 'Gọi Cho Chúng Tôi! Việt Nam',
    'news' => 'Tin Tức',
    'about_header' => 'Chúng tôi là đội ngũ chuyên nghiệp',
    'about_sub_header' => 'Được cấp phép tại Hoa Kỳ và chứng ninghận bởi CLEAR',
    'click_to_see' => 'Nhấp để xem những đoạn phim về liệu pháp của chúng tôi!',
    'scoliosis_intensive_care' => 'Vẹo Cột Sống Điều trị Chuyên sâu',
    'scoliosis_intensive_care_text' => 'Phương án được khuyến nghị cho 1-2 tuần điều trị vẹo cột sống, thường cung cấp kết quả tuyệt vời trong thời gian ngắn nhất.',
    'scoliosis_expanded_care' => 'Điều trị Vẹo Cột sống Mở rộng',
    'scoliosis_expanded_care_text' => 'Để những bệnh nhân sinh sống gần với trung tâm của chúng tôi có thể nhận điều trị thường xuyên mà không chịu gián đoạn hoặc chỉ gián đoạn tối thiểu từ các sinh hoạt thông thường (trường học, công việc,…v.v…).',
    'new' => 'new',
    'more' => 'xem thêm',
    'main_clear_scoliosis_treatment' => 'Thấy là Tin - Những kết quả đã được chứng mình của chúng tôi trong Điều trị chứng Vẹo cột sống',
    'main_clear_scoliosis_treatment_text' => 'Thấy là Tin - Những kết quả đã được chứng mình của chúng tôi trong Điều trị chứng Vẹo cột sống',
    'main_clear_scoliosis_treatment_case_1' => 'Trước khi đến Bones & Beyond, người này đã trải qua 3 năm điều trị y tế đầy căng thẳng và phục hồi với kết quả không hiệu quả và đã được thông báo rằng có thể sẽ cần đến phẫu thuật nếu độ vẹo cột sống trở nên tệ hơn. Với 2 tuần điều trị chuyên sâu (IC), những phần vẹo lớn ở dưới cổ và trên lưng đã được giảm đáng kể.',
    'main_clear_scoliosis_treatment_case_2' => 'Người này đã được yêu cầu phải mang niềng tại bệnh viện từ 23 đến 24 giờ một ngày nhưng cảm thấy mình không thể bởi vì làm vậy quá đau đớn. Do cột sống chưa trở nên đủ cứng (có thể gây ra bởi mang niềng một thời gian dài), chúng tôi đã có thể giảm độ cong vẹo từ 44 xuống còn 18 độ.',
    'main_about_clear' => 'Liệu pháp điều trị Vẹo cột sống chuyên sâu của chúng tôi (được phát triển, chứng nhận và hỗ trợ bởi viện CLEAR) là phương pháp giảm vẹo cột sống tiên tiến nhất trên thế giới. Phương pháp giảm vẹo cột sống của CLEAR là một hướng tiếp cận mang tính cách mạng đã được chứng minh có khả năng không chỉ ổn định mà còn giảm đáng kể vùng cong vẹo cột sống mà không cần dùng niềng hoặc phẫu thuật.',
    'important' => "quan trọng",
    'clear_certificate' => 'Được cấp phép IC (Điều trị chuyên sâu) bởi CLEAR thì có ý nghĩa gì?',
    'pagetop' => 'đầu trang',
    'chiro_title_top' => 'bones+beyond professional chiropractic care',
    'chiro_title_bottom' => 'experience profressional licensed adjustment'
];
