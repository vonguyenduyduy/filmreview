<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 15/01/2018
 * Time: 15:30
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'Home',
    'scoliosis_treatment' => 'Scoliosis Treatment',
    'chiropractic_treatment' => 'Chiropractic Treatment',
    'about_bones_beyond' => 'About bones+beyond',
    'access_&_contact' => 'Access & Contact',
    'header_line_1' => 'Việt Nam CLEAR Scoliosis Treatment & Chiropractic',
    'header_line_2' => 'U.S. Professional Licensed Chiropractic and CLEAR Scoliosis Treatment "bones+beyond"',
    'menu_text' => 'For Việt Nam Scoliosis Patients / U.S. Professional Licensed Chiropractic / CLEAR Certified / CLEAR Intensive Care Certified Scoliosis Treatment',
    'call_us_text_main' => 'Call us for any questions related to scoliosis treatment, chiropractic, or any physical symptoms. Working in harmony with your body and mind\'s natural healing capacities, Dr. Kieu will help you achieve your goals . . . pain free . . . health-wise . . . and naturally!',
    'call_us_now' => 'Call Us Now! Viet Nam',
    'news' => 'News',
    'about_header' => 'We\'re professional',
    'about_sub_header' => 'U.S. licensed and CLEAR Certified',
    'click_to_see' => 'Click to view our videos about our treatment!',
    'scoliosis_intensive_care' => 'Scoliosis Treatment Plan (Intensive Care)',
    'scoliosis_intensive_care_text' => 'The recommended plan for 1-2 weeks of scoliosis treatment, generally providing excellent results in the least amount of time.',
    'scoliosis_expanded_care' => 'Scoliosis Treatment Plan (Expanded Care)',
    'scoliosis_expanded_care_text' => 'For patients living close enough to our clinic to allow frequent scoliosis treatments with no or minimal interruption of regular routine (school, work, etc.).',
    'new' => 'new',
    'more' => 'more',
    'main_clear_scoliosis_treatment' => 'Seeing Is Believing—Our Proven Results in Scoliosis Treatment',
    'main_clear_scoliosis_treatment_text' => 'Since the commencement of our CLEAR Certified IC Scoliosis treatment in 2011, we have achieved impressive results in correcting the curvature of the spine.',
    'main_clear_scoliosis_treatment_case_1' => 'Prior to coming to Bones and Beyond, this person had already undergone 3 years of stressful medical care and rehab with ineffective results and had been told that surgery might be necessary if the curvature continued to worsen. With 2 weeks of our IC scoliosis treatment, major scoliosis curvatures in both the lower neck and upper back were substantially reduced.',
    'main_clear_scoliosis_treatment_case_2' => 'This person was told at the hospital to wear a brace for 23 to 24 hours a day but had found that she couldn’t because doing so was too painful. Since the spine had not yet become rigid (which wearing a brace for a long period can cause the spine to become), we were able to reduce the curvature from 44 to 18 degrees.',
    'main_about_clear' => 'Our Intensive Care (IC) Scoliosis Treatment, developed, certified, and supported by CLEAR Institute, is the most advanced scoliosis reduction method in the world. The CLEAR scoliosis reduction method is a revolutionary approach that has been proven capable of not only stabilizing but also substantially reducing scoliosis curvature without the need for a brace or surgery.',
    'important' => "important",
    'clear_certificate' => 'What does it mean to be CLEAR IC (Intensive Care) Certified?',
    'pagetop' => 'pagetop',
    'chiro_title_top' => 'bones+beyond professional chiropractic care',
    'chiro_title_bottom' => 'experience profressional licensed adjustment'

];