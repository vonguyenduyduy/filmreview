<?php

namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AdminLte\Repositories\AboutUsRepository;
use Modules\AdminLte\Repositories\AlbumsRepository;
use Modules\AdminLte\Repositories\BannersRepository;
use Modules\AdminLte\Repositories\MembersRepository;
use Modules\AdminLte\Repositories\NewsCategoriesRepository;
use Modules\AdminLte\Repositories\NewsRepository;
use Modules\AdminLte\Repositories\ProjectsRepository;
use Modules\AdminLte\Repositories\SystemFeaturesRepository;
use Modules\AdminLte\Repositories\VideosRepository;
use Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class FrontEndController extends BaseController
{

    protected $bannersRepository;
    protected $albumsRepository;
    protected $projectsRepository;
    protected $newsRepository;
    protected $newsCategoriesRepository;
    protected $aboutUsRepository;
    protected $membersRepository;
    protected $systemFeaturesRepository;
    protected $videosRepository;

    protected $lang;
    const ON_FOOTER = 'show_on_footer';
    const IS_LATEST = 'is_latest';
    const IS_POPULAR = 'is_popular';
    const ON_HOME = 'is_home';
    const LOGO_KEY = 'LOGO';
    const ICON_KEY = 'ICON';

    function __construct(BannersRepository $bannersRepository, AlbumsRepository $albumsRepository,
                         ProjectsRepository $projectsRepository, NewsRepository $newsRepository,
                         NewsCategoriesRepository $newsCategoriesRepository, AboutUsRepository $aboutUsRepository,
                         MembersRepository $membersRepository, SystemFeaturesRepository $systemFeaturesRepository,
                         VideosRepository $videosRepository){

        parent::init();
        $this->bannersRepository = $bannersRepository;
        $this->albumsRepository = $albumsRepository;
        $this->projectsRepository = $projectsRepository;
        $this->newsRepository = $newsRepository;
        $this->newsCategoriesRepository = $newsCategoriesRepository;
        $this->aboutUsRepository = $aboutUsRepository;
        $this->membersRepository = $membersRepository;
        $this->systemFeaturesRepository = $systemFeaturesRepository;
        $this->videosRepository = $videosRepository;

        $this->data['logo'] = $this->systemFeaturesRepository->findWhere(array('config_key' => self::LOGO_KEY))->first();
        $this->data['icon'] = $this->systemFeaturesRepository->findWhere(array('config_key' => self::ICON_KEY))->first();

        $this->lang = LaravelLocalization::getCurrentLocale();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $this->data['menuActive'] = 'home';
        $this->data['banners'] = $this->bannersRepository->findWhere(array('status' => self::ACTIVE));
        $this->data['projects'] = $this->reproduceProject();
        $this->data['albums'] = $this->albumsRepository->getRandomAlbums();
        $this->data['newsOnFooter'] = $this->newsRepository->getNewsByAtribute(self::ON_FOOTER, false, 10);
        $this->data['news'] = $this->newsRepository->with('category')->getNewsByAtribute(self::ON_HOME, true, 4);
        $this->data['aboutUs'] = $this->aboutUsRepository->firstOrNew();
        $this->data['videos'] = $this->videosRepository->findWhere(array());
        $this->data['services'] = $this->systemFeaturesRepository->findWhereIn('config_key',array('SERVICE1', 'SERVICE2', 'SERVICE3', 'SERVICE4'));

        return view('frontend::home.index', $this->data);
    }
    protected function reproduceProject(){
        $projects = $this->projectsRepository->with(['albums' => function($q){
            $q->where('status', self::ACTIVE);
        }])->orderBy('order', 'asc')->findWhere(array('status' => self::ACTIVE));

        return $projects;
    }

    public function projectAll()
    {
        $this->data['menuActive'] = 'project';

        $this->data['projects'] = $this->reproduceProject();
        $this->data['albums'] = $this->albumsRepository->getRandomAlbums();
        $this->data['subView'] = 'frontend::project.module.multiple';
        return view('frontend::project.index', $this->data);
    }
    public function project($slug, $id)
    {
        $this->data['menuActive'] = 'project';

        $this->data['album'] = $this->albumsRepository->with(['sub_images','project'])->findWhere(array('id' => $id))->first();
        $this->data['menuActive'] = 'project';
        $this->data['subView'] = 'frontend::project.module.single';
        return view('frontend::project.index', $this->data);
    }

    public function aboutUs()
    {
        $this->data['members'] = $this->membersRepository->findWhere(array(
            'status' => self::ACTIVE
        ));

        $this->data['detail'] = $this->aboutUsRepository->firstOrNew();
        $this->data['subView'] = 'frontend::static.about_us';
        return view('frontend::project.index', $this->data);
    }
    public function service($slug,$id)
    {
        $this->data['service'] = $this->systemFeaturesRepository->find($id);

        $this->data['subView'] = 'frontend::static.service';
        return view('frontend::project.index', $this->data);
    }
    public function contact()
    {
        $this->data['subView'] = 'frontend::static.contact';
        return view('frontend::project.index', $this->data);
    }

    public function newsAll()
    {
        $this->data['menuActive'] = 'news';

        $this->data['categoryTitle'] = getValueConfig('news_all');
        $this->data['news'] = $this->newsRepository->with('category')->findWhere(array(
            'status' => self::ACTIVE
        ));
        $this->data['subView'] = 'frontend::news.module.all';
        return view('frontend::news.index', $this->data);
    }

    public function newsCategory($slug, $categoryId)
    {
        $this->data['menuActive'] = 'news';

        $this->data['categoryTitle'] = $this->newsCategoriesRepository->find($categoryId)->first()->name;

        $this->data['news'] = $this->newsRepository->with('category')->findWhere(array(
            'status' => self::ACTIVE,
            'category_id' => $categoryId
        ));
        $this->data['subView'] = 'frontend::news.module.all';
        return view('frontend::news.index', $this->data);
    }
    public function news($slug, $id)
    {
        $this->data['menuActive'] = 'news';

        $this->data['categories'] = $this->newsCategoriesRepository->findWhere(array('status' => self::ACTIVE));

        $this->data['detail'] = $this->newsRepository->with('category')->find($id);

        $this->data['latest'] = $this->newsRepository->getNewsByAtribute(self::IS_LATEST,false, 4);
        $this->data['mostly'] = $this->newsRepository->getNewsByAtribute(self::IS_POPULAR,false,  4);
//        dd($this->data['mostly']);
        $this->data['recently'] = $this->newsRepository->getWithLimit(false, 4);

        $this->data['subView'] = 'frontend::news.module.single';
        return view('frontend::news.index', $this->data);
    }

    public function search(Request $request)
    {
        $params = $request->all();
        $this->data['menuActive'] = 'news';
        $this->data['result'] = $this->summarySearch($params['key']);
        $key = $params['key'];
        $this->data['projects']  = $this->projectsRepository->findWhere(array(
            ['name', 'like', "%".$key."%"]
        ));
        $this->data['albums']  = $this->albumsRepository->findWhere(array(
            ['name', 'like', "%".$key."%"]
        ));
        $this->data['categoryNews']  = $this->newsCategoriesRepository->findWhere(array(
            ['name', 'like', "%".$key."%"]
        ));
        $this->data['news']  = $this->newsRepository->findWhere(array(
            ['name', 'like', "%".$key."%"]
        ));

        $this->data['categories'] = $this->newsCategoriesRepository->findWhere(array('status' => self::ACTIVE));

        $this->data['latest'] = $this->newsRepository->with('category')->getNewsByAtribute(self::IS_LATEST,false, 4);
        $this->data['mostly'] = $this->newsRepository->with('category')->getNewsByAtribute(self::IS_POPULAR,false,  4);
        $this->data['subView'] = 'frontend::news.module.single';
        return view('frontend::news.index', $this->data);
    }

    public function videoAll()
    {
        $this->data['menuActive'] = 'video';

        $this->data['categoryTitle'] = getValueConfig('video_all');
        $this->data['videos'] = $this->videosRepository->findWhere(array(
            'status' => self::ACTIVE
        ));
        $this->data['subView'] = 'frontend::video.module.all';
        return view('frontend::video.index', $this->data);
    }
    public function videoDetail($slug, $id)
    {
        $this->data['menuActive'] = 'video';

        $this->data['categories'] = $this->newsCategoriesRepository->findWhere(array('status' => self::ACTIVE));

        $this->data['detail'] = $this->videosRepository->find($id);

        $this->data['latest'] = $this->newsRepository->getNewsByAtribute(self::IS_LATEST,false, 4);
        $this->data['mostly'] = $this->newsRepository->getNewsByAtribute(self::IS_POPULAR,false,  4);
        $this->data['recently'] = $this->newsRepository->getWithLimit(false, 4);

        $this->data['subView'] = 'frontend::video.module.single';
        return view('frontend::video.index', $this->data);
    }
    protected function summarySearch($key){
        $result = array();

        return $result;
    }


}
