/**
 * Created by ducanh on 15/01/2018.
 */
function changeLocale(locale) {
    pageURL = document.URL;
    var res = pageURL.split("/");
    newURL = '';

    for (i = 0; i<res.length;i++ ) {
        tmp = "";
        if(i > 0)
            tmp = "/";
        if(i == 3)
            res[i] = locale;

        newURL += tmp + res[i];
    }

    window.location = newURL;
}