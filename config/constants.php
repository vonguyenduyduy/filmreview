<?php
return array(

    'languages' => array(
        'en' => 'English',
        'vi' => 'Viet',
    ),
    'lang_default' => 'en',
    'IS_MENU' => 1,
    'STATUS_ACTIVE' => 1,
    'SCOLIOSIS_TREATMENT' => 2,
    'ABOUT_US' => 4,
    'CONTACTS' => 5,
    'NEWS' => 6,
    'IS_HOT' => 'is_hot',
    'IS_NEW' => 'is_new',
    'HOME_TYPE_1' => 'home_type_1',
    'HOME_TYPE_2' => 'home_type_2',
    'HOME_TYPE_3' => 'home_type_3',
    'HOME_TYPE_4' => 'home_type_4',
    'POST_SELECT_BOX' => array(
        1 => 'Post categories',
        2 => 'Only posts',
    )
);